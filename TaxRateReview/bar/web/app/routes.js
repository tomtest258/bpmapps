/**
 * Created by tpalmer on 10/13/2016.
 */


(function () {
    'use strict';


    angular
        .module('BPM')
        .config(function ($routeProvider) {
        $routeProvider
        // when: takes 2 arguments
        //	path - string describing what is covered by this instruction
        //	route - special object describing what should be done when route is accessed.
        //		** controller - controller for the content of the view
        //		** templateUrl - relative location of a view
        //		** redirectTo - path where use should be redirected.
            .when('/home', {
                controller: 'HomeCtrl',
                templateUrl: 'app/home.html'
            })
            // otherwise: takes just one object which is a route as defined above
            .otherwise({
            	controller: 'HomeCtrl',
                templateUrl: 'app/home.html'
            });

    });

})();
