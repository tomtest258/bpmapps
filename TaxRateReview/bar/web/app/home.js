
(function () {
    "use strict";

    angular
        .module('BPM')
        .controller('HomeCtrl', HomeCtrl)
    ;

    HomeCtrl.$inject = ['$scope','ApplicationState','BPM','$timeout','$interval','$http','BPMAdmin'];
    function HomeCtrl($scope,ApplicationState,BPM,$timeout,$interval,$http,BPMAdmin) {
    	$scope.connection = {};
    	$scope.plans = {"Secure REST Action Test":0,"Income Tax Review":0};
        $scope.sameTenant = true;
        $scope.rulesTenant = "";
    	$scope.error = "";
    	$scope.info = {"summary":"","details":""};
    	$scope.intervalPromise = null;
    	$scope.currentProcessId = null;
    	
        // Functions
    	$scope.test = test;
    	$scope.openProcessInstance = openProcessInstance;
    	$scope.openProcessDiagram = openProcessDiagram;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
    	
    	function openProcessDiagram(planName) {
    		document.location.href =$scope.connection.aaServerUrl+"/#/definitionGraph/" + $scope.connection.tenant + "/a/" + $scope.connection.application + "/pd/" + $scope.plans[planName];
    	}
    	
    	function openProcessInstance(piId) {
    		document.location.href =$scope.connection.aaServerUrl+"/index.html#/instanceDetail/" + $scope.connection.tenant + "/a/" + $scope.connection.application + "/pi/" + piId;
    	}
    	
    	function test() {
    		var tms = getCurrentTimeString();
    		$scope.error = "";
    		$scope.currentProcessId = null
    		
    		if($scope.loginUser != "ibpmuser@example.com") {
    			$scope.error = "Please login in as ibpmuser@example.com to run the demo";
    			return;
    		}
    		
    		$scope.info = {"summary":"","details":""};
    		
    		BPMAdmin
            .getTenants()
            .success(
            	function (data, status) {
            		var tenants = data.tenants;
            		var found = false;
            		if($scope.sameTenant) {
            			found = true;
            		} else {
            			$scope.rulesTenant = $scope.rulesTenant.trim(); 
                		for(var i=0; i< tenants.length; i++) {
                			if(tenants[i].name == $scope.rulesTenant) {
                				found = true;
                				break;
                			}
                		}
            		}
            		if(!found){
            			console.log("Could not find the rules tenant '" + $scope.rulesTenant + "'");
                    	$scope.error = $scope.error + "\n" + "Could not find the rules tenant '" + $scope.rulesTenant + "'";
            		} else {
            			prepareConnectorsAndRunTest();
            		}
                }
            )
            .error(
                function (data, status) {
                	console.log("Could not get tenants",data);
                	$scope.error = $scope.error + "\n" + "Could not get tenants";
                }
            );
    		
    	}
    	
    	function prepareConnectorsAndRunTest() {
            if(!$scope.sameTenant && !$scope.rulesTenant) {
            	$scope.error = "Please specify the tenant where application 'TaxRateRules' is hosted";
    			return;
            }
            
            $scope.info.summary = "Test is 0% Complete...";
    		$scope.info.details = getCurrentTimeString() + ": Test is in progress...";
    		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Please wait for the completion before leave this page."

            BPMAdmin
            .getConnectorsConfiguration($scope.connection.tenant, $scope.connection.application)
            .success( function(data) {
                var connectorsConfig = data;
                BPMAdmin
                .encrypt("Fujitsu1")
                .success( function(data) {
                    var encrytedPassword = data.ciphertext;
                    for (var p in connectorsConfig) {
                    	if(p == "Tax_Rule_List_Service" || p == "Tax_Rate_Service" ) {
                    		if($scope.sameTenant) {
                    			connectorsConfig[p]["authentication"] = "internal";
                        		if(connectorsConfig[p].hasOwnProperty("user")) {
                        			delete connectorsConfig[p]["user"];
                        		}
                        		if(connectorsConfig[p].hasOwnProperty("password")) {
                        			delete connectorsConfig[p]["password"];
                        		}
                    		} else {
                    			connectorsConfig[p]["authentication"] = "basic";
                            	connectorsConfig[p]["user"] = "ibpmuser@example.com";
                            	connectorsConfig[p]["password"] = encrytedPassword;
                    		}
                    	}
                        var curl = connectorsConfig[p]["url"];
                        var index1 = curl.indexOf("/t=");
                        var substring1 = curl.substring(0,index1+3);
                        var substring2 = curl.substring(index1+3);
                        var index2 = substring2.indexOf("/");
                        var newcurl = "";
                        if(p == "Tax_Rule_List_Service" || p == "Tax_Rate_Service" ) {
                        	if($scope.sameTenant) {
                            	newcurl = substring1 + $scope.connection.tenant + substring2.substring(index2);
                            } else {
                            	newcurl = substring1 + $scope.rulesTenant + substring2.substring(index2);
                            }
                        } else {
                        	newcurl = substring1 + $scope.connection.tenant + substring2.substring(index2);
                        }
                        connectorsConfig[p]["url"] = newcurl;
                    }
                    BPMAdmin
                    .setConnectorsConfiguration($scope.connection.tenant, $scope.connection.application,connectorsConfig)
                    .success( function(data) {
                    	$scope.info.summary = "Test is 10% Complete...";
                		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Finished preparing connectors.json";
                		testRestAction("Secure REST Action Test");
                    })
                    .error( function(data, status, headers, config) {
                        console.log("updateConnectorsConfiguration error:",data,status);
                        $scope.$emit("ShowError", data);
                    });
                })
                .error( function(data, status, headers, config) {
                	$scope.error = $scope.error + "\n" + "Failed to encrypt user password to access application 'TaxRateRules' - " + data;
                });
            })
            .error( function(data, status, headers, config) {
                console.log("Could not get connectors configuration - ",data);
                $scope.error = $scope.error + "\n" + "Could not get connectors configuration";
            });
    	}
    	
    	function testBusinessRules(planName) {
    		var uda = {};
    		BPM
            .createInstance($scope.plans[planName],uda,null)
            .success(
            	function (data, status) {
            		$scope.info.summary = "Test is 60% Complete...";
            		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Created a 'Income Tax Review' process - " + data.idInst;
            		$scope.currentProcessId = data.idInst;
            		$scope.intervalPromise = $interval(workonBusinessRulesWorkItems, 5000);
                }
            )
            .error(
                function (data, status) {
                	console.log("Could not create 'Income Tax Review' process",data);
                	$scope.error = $scope.error + "\n" + "Could not create 'Income Tax Review' processes";
                }
            );
    	}
    	
    	function checkRestActionProcessStatus() {
    		BPM
            .getProcessInstance($scope.currentProcessId)
            .success(
            	function (data, status) {
            		if(data.pi.hasOwnProperty("error") && data.pi.error) {
            			console.log("Failed to validate secure rest actions: " + data.pi.error);
                    	$scope.error = $scope.error + "\n" + "Failed to validate secure rest actions";
                    	$interval.cancel($scope.intervalPromise);
            		}
                }
            )
            .error(
                function (data, status) {
                	;
                }
            );
    	}
    	
    	function checkBusinessRulesProcessStatus() {
    		BPM
            .getProcessInstance($scope.currentProcessId)
            .success(
            	function (data, status) {
            		if(data.pi.hasOwnProperty("error") && data.pi.error) {
            			console.log("Failed to validate business rules: " + data.pi.error);
                    	$scope.error = $scope.error + "\n" + "Failed to validate business rules";
                    	$interval.cancel($scope.intervalPromise);
            		}
                }
            )
            .error(
                function (data, status) {
                	;
                }
            );
    	}
    	
    	function workonBusinessRulesWorkItems() {
    		if($scope.info.summary != "Test is 100% Complete!"){
    			checkBusinessRulesProcessStatus();
    		}
    		
    		BPM
    		.getWorkItemList("MyActiveWorkItems", null)
    		.success(
	            function (data, status) {
	            	var wiList = data.wiList;
	            	if(wiList == null || wiList.length == 0) {
	            		return;
	            	}
	            	for(var i=0; i < wiList.length; i++) {
	            		var wi = wiList[i];
	            		var choices = wi.choices;
	            		var udas = wi.uda;
	            		if(wi.instanceId == $scope.currentProcessId) {
	            			$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Continued working on assigned work item '" + wi.name + "' for 'Income Tax Review' process";
	            			if(wi.name == "Send Tax Due Notice" || wi.name == "Issue a Refund") {
	            				if(udas["FederalTaxRate"] != 0.29) {
	            					console.log("Federate Tax Rate returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "Federate Tax Rate returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				if(udas["TaxDue"] != 14500.00) {
	            					console.log("Tax Due returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "Tax Due returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				if(udas["taxFormula"] != 4900.00) {
	            					console.log("Tax formula returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "Tax formula returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				$scope.info.summary = "Test is 100% Complete!";
			            		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Business Rules Test was completed!";
	                            if(!$scope.error) {
	                            	$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": All tests were completed succesfully!";
	                            }
			            		$interval.cancel($scope.intervalPromise);
	            			}
	            			makeChoice({"wi":{"id":wi.id}},choices[0]);
	            		}
	            	}
	            }
	         )
	         .error(
	            function (data, status) {
	            	console.log("Could not retrieve the assigned workitems",data);
	                $scope.error = $scope.error + "\n" + "Could not retrieve the assigned workitems";
	            }
	         );
    	}
    	
    	
    	function testRestAction(planName) {
    		var uda = {};
    		uda["Content"] = "test 123";
    		BPM
            .createInstance($scope.plans[planName],uda,null)
            .success(
            	function (data, status) {
            		$scope.info.summary = "Test is 20% Complete...";
            		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Created a 'Secure REST Action Test' process - " + data.idInst;
            		$scope.currentProcessId = data.idInst;
            		$scope.intervalPromise = $interval(workonRestActionWorkItems, 5000);
                }
            )
            .error(
                function (data, status) {
                	console.log("Could not create 'Secure REST Action Test' process",data);
                	$scope.error = $scope.error + "\n" + "Could not create 'Secure REST Action Test' processes";
                }
            );
    	}
    	
    	function workonRestActionWorkItems() {
    		checkRestActionProcessStatus();
    		
    		BPM
    		.getWorkItemList("MyActiveWorkItems", null)
    		.success(
	            function (data, status) {
	            	var wiList = data.wiList;
	            	if(wiList == null || wiList.length == 0) {
	            		return;
	            	}
	            	for(var i=0; i < wiList.length; i++) {
	            		var wi = wiList[i];
	            		var choices = wi.choices;
	            		var udas = wi.uda;
	            		if(wi.instanceId == $scope.currentProcessId) {
	            			$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Continued working on assigned work item '" + wi.name + "' for 'Secure REST Action Test' process";
	            			if(wi.name == "Review Result") {
	            				if(udas["PUT Script Result"] != "Success") {
	            					console.log("REST PUT Action returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "REST PUT Action returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				if(udas["GET Script Result"] != "Success") {
	            					console.log("REST GET Action returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "REST GET Action returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				if(udas["POST Script Result"] != "Success") {
	            					console.log("REST POST Action returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "REST POST Action returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				if(udas["DELETE Script Result"] != "Success") {
	            					console.log("REST DELETE Action returns wrong result on process " + $scope.currentProcessId);
	                            	$scope.error = $scope.error + "\n" + "REST DELETE Action returns wrong result on process " + $scope.currentProcessId;
	            				}
	            				$scope.info.summary = "Test is 50% Complete!";
			            		$scope.info.details = $scope.info.details + "\n" + getCurrentTimeString() + ": Rest Action Test was completed!";
			            		$interval.cancel($scope.intervalPromise);
	            				testBusinessRules("Income Tax Review");
	            			}
	            			makeChoice({"wi":{"id":wi.id}},choices[0]);
	            		}
	            	}
	            }
	         )
	         .error(
	            function (data, status) {
	            	console.log("Could not retrieve the assigned workitems",data);
	                $scope.error = $scope.error + "\n" + "Could not retrieve the assigned workitems";
	            }
	         );
    	}
    	
    	
    	function makeChoice(workItem, choiceName) {
    		BPM
            .makeChoice(workItem, choiceName)
            .success(
            	function (data, status) {
            		;
                }
            )
            .error(
                function (data, status) {
                	;
                }
            );
    	}
    	
    	function getPlanId(planName) {
    		BPM
            .getPublishedPdListByName(planName)
            .success(
            	function (data, status) {
            		var pdList = data.pdList;
            		if(pdList == null || pdList.length <= 0) {
            			$scope.error = $scope.error + "\n" + "Cound not find the published process definition named " + planName;
            			return;
            		}
            		$scope.plans[planName] = pdList[0].id;
            	}
            )
            .error(
                function (data, status) {
                	console.log("Could not find the publised process definition named " + planName, data);
                    $scope.error = $scope.error + "\n" + "Could not find the publised process definition named " + planName;
                }
            );
    	}
    	
    	function loginCallback(loginInfo) {
            // update the display of who is logged in according to login info
            ApplicationState.connectionModel.loginInfo = loginInfo;
            if (ApplicationState.connectionModel.loginInfo.verified) {
            	BPM.initialize($scope.connection.aaServerUrl, $scope.connection.tenant, $scope.connection.application);
            	BPMAdmin.initialize($scope.connection.aaServerUrl);
            	$scope.loginUser = ApplicationState.connectionModel.loginInfo.userId;
            	$scope.$apply();
            	getPlanId("Secure REST Action Test");
            	getPlanId("Income Tax Review");
            }
        }
    	
        initialize();
        function initialize() {
        	$scope.connection = getServerConnection();
        	
        	// ApplicationState is where we store all of our application's state.
            $scope.appState = ApplicationState;
            var loginConfig = {
                    "providerUrl": "",
                    "serverUrl": $scope.connection.aaServerUrl
            };

            // This is the call that actually initiates the login.
            SLAP.initLogin(loginConfig, {}, loginCallback);
        }
    }

})();

