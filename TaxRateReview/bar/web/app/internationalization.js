/**
 * Created by tpalmer on 10/15/2016.
 */


(function () {
    "use strict";

    ///////////////////////////////////
    //     i18n
    //     Internationalization
    ///////////////////////////////////

    // bpm_angular uses angular-translate so we can also leverage it for our application

    var bpmAngularUiModule= angular.module('BPM');

    bpmAngularUiModule.config(['$translateProvider', function($translateProvider) {
        var app_fr=
        {
            "cas" : {
            	"CodeEvenementAffaire": "Type d’événement",
            	"CodeEtatCodes": "État",
                "Agent": "Agent assigné"
            },
        	"casEtat" :{
                "AQualifier" : "À qualifier",
                "EnQualification" : "En qualification",
                "EnEnregistrement" : "En enregistrement",
                "EnRejet" : "En rejet",
                "Qualifie" :"Qualifié",
                "Rejete" :"Rejeté"
            },
            "casEvenementAffaire" :{
                "ReceptionDocumentNonStructure" : "Document non structuré",
                "RetourCourrierDNS" : "Retour de courrier"
            },
            "casTache" :{
            	"Qualifier GroupeDocument":"Qualifier un groupe de document",
            	"Traiter rejet":"Traiter un reject"
            },
            "casIndicateurTraitementRejet" :{
                "true" : "",
                "false" : "Rejet annulé"
            },
            "piTable" :{
                "NumeroGroupeDocument": "Numéro du cas",
                "NumeroCas": "Numéro du cas",
                "DateReception": "Date de réception",
                "CodeEvenementAffaire": "Type d’événement",
                "EtatCode": "État",
                "CodeAgent": "Agent assigné",
                "NombreDocument": "Nombre de document",
                "IndicateurTraitementRejet": "Rejet",
                "state": "Status du processus"
            },
            "taskName" :{
                "Qualifier GroupeDocument" : "Qualifier un groupe de document",
                "Traiter rejet" : "Traiter un rejet"
            },
            "wiTable" :{
                "NumeroGroupeDocument": "Numéro du cas",
                "NumeroCas": "Numéro du cas",
                "DateReception": "Date de réception",
                "CodeEvenementAffaire": "Type d’événement",
                "EtatCode": "État",
                "CodeAgent": "Agent assigné",
                "NombreDocument": "Nombre de document",
                "IndicateurTraitementRejet": "Rejet",
                "nameText": "Tâche"
            },
            "navBar" : {
                "About"     : "À propos",
                "New_Case"  : "Nouveau cas",
                "Cases"     : "Cas",
                "Tasks"     : "Tâches",
                "bo"        : "Objets d’affaires",
                "support"   : "Support"
            },
            "home" :{
                "cegc" : "Page de démarrage DNS",
                "cuiIntro" : "Application pour document non structuré.",
                "cuiDetail" : "Lors de la mise en œuvre d’un processus, les utilisateurs veulent une interface utilisateur qui est spécifique à la solution. Cette application est destinée à être utilisée comme base pour les interfaces utilisateurs requis par les applications à développer avec Fujitsu Interstage BPM.",
                "processText" : "Vous pouvez voir la liste et le détail des processus.",
                "taskText" : "Chaque utilisateur dispose d'une liste de mots personnelle où ils peuvent gérer leurs tâches.",
                "boText" : "L'objet d'affaire est l'objet métier de la solution."
            },
            "field" : {
            	"identifiant": "Identifiant"
            },
            "text": {
            	"listeCas": "Liste de cas",
            	"listeTaches": "Liste des tâches",
            	"listeTachesEnQualification":"Liste des tâches",
            	"rechercherListe":"Rechercher dans la liste",
            	"obtenirNouveauCas": "Obtenir un nouveau cas",
            	"obtenirNouvelleTache": "Obtenir une nouvelle tâche",
            	"reassignment": "Réassignation",
            	"selectAgent": "Sélectionner agent",
            	"selectTypeDeTachet": "Sélectionner la tâche",
            	"reassign": "Réassigner",
            	"message": "Message",
            	"ok": "D'accord",
            	"taskReassignmentSucceeded": "La réassignation de la tâche s'est effectuée avec succès",
            	"cleReperage":"Clé de repérage",
            	"codeSysteme":"Système",
            	"codeReperage":"Type de repérage",
            	"valeurReperage1":"Repérage 1",
            	"valeurReperage2":"Repérage 2",
            	"valeurReperage3":"Repérage 3",
            	"historique":"Historique",
            	"etat":"Etat",
            	"date":"Date",
            	"agentAssignee":"Agent assigné"
            },
            "option" : {
            	"tous": "Tous"
            },
            "error" : {
            	"value_valide_required_error": "Une valeur valide est requis"
            },
            "boList" :{
                "boText" : "Cette page affichera la liste des objets d’affaires de l'application. L'objet d’affaires (OA) est l'objet que la solution gère. Par exemple, au déploiement d’un système, l’OA pourrait être une demande de déploiement et par conséquent cette page afficherait une liste des demandes de déploiement."
            },
            "support" : {
                "supportText" : "Cette page affichera des informations pour trouver l'aide et le soutien à l'application."
            },
            "processInstanceLauncherTitle" : "Case Launcher"
        };

        var app_en=
        {
        	"piTable" :{
                "NumeroGroupeDocument": "Case group number",
                "NumeroCas": "Case group number",
                "DateReception": "Receipt date",
                "CodeEvenementAffaire": "Event type",
                "EtatCode": "State",
                "CodeAgent": "Agent",
                "NombreDocument": "Document nuumber",
                "IndicateurTraitementRejet": "Rejection"
            },
            "wiTable" :{
                "NumeroGroupeDocument": "Case group number",
                "NumeroCas": "Case group number",
                "DateReception": "Receipt date",
                "CodeEvenementAffaire": "Event type",
                "EtatCode": "State",
                "CodeAgent": "Agent",
                "NombreDocument": "Document nuumber",
                "IndicateurTraitementRejet": "Rejection",
                "nameText": "Task"
            },
            "navBar" : {
                "About"     : "About",
                "New_Case"  : "New Case",
                "Cases"     : "Cases",
                "Tasks"     : "Tasks",
                "bo"        : "Business Objects",
                "support"   : "Support"
            },
            "home" :{
                "cegc" : "DNS Start Page",
                "cuiIntro" : "This application is for non-structured document handling.",
                "cuiDetail" : "When implementing a process driven solution users want a user interface that is specific to the solution. This application is meant to be used as the basis for process driven solutions that leverage Fujitsu's Interstage BPM.",
                "processText" : "You can see the list of processes and view details about them.",
                "taskText" : "Each user has a personal work list where they can manage their tasks.",
                "boText" : "The Managed Object is the Business Object of the solution."
            },
            "boList" :{
                "boText" : "This page will display the list of Business Objects of the application. The Business Object (BO) is the object that the solution is managing. For example, in a deployment system the BO would be a Deployment Request and hence this page would display a list of Deployment Requests."
            },
            "support" : {
                "supportText" : "This page will display information about where to go for Help and Support for the application."
            },
            "processInstanceLauncherTitle" : "Case Launcher"
        };




        $translateProvider
            .translations('en', app_en)
            .translations('fr', app_fr)
            .preferredLanguage('en')
            .fallbackLanguage('en')
            .useSanitizeValueStrategy('escape')
        ;
    }]);


})();
