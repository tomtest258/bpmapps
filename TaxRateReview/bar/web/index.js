/**
 * Main file
 */


(function () {
    'use strict';

    // This BPM GUI uses functions from:
    //  bpm_angular: Angular wrapper methods for Interstage Agile Adapter
    //  bpm_angular_ui: Directive library with reusable BPM UI components
    //  Bootstrap library for AngularJS. This makes our User Interface look much nicer.
    var BPMModule = angular.module('BPM', ['ngRoute', 'bpm-angular', 'bpm-angular-ui', 'ui.bootstrap', 'pascalprecht.translate', 'ngMessages', 'ui.mask']);

    angular
        .module('BPM')
        .factory('ApplicationState', ApplicationState)
        .factory('ConfigFactory', ConfigFactory)
        .controller('BPMController', BPMController)
    ;


    // All of the state of the application will be stored here on this object.
    function ApplicationState() {
        return {
            // Anything to do with the user connecting to the Server goes here:
            "connectionModel": {
                // loggedIn keeps track of if we are already logged in.
                // We use this to make sure we only initializae the form once.
                "loggedIn": false,
                // loginInfo will store the user data such as the userId.
                "loginInfo": {}
            },
            // Anything to do with Process Instances goes here:
            "piModel": {
                "processInstances": [],
                "processInstanceFilter": "",
                "selectedProcessInstance": null
            },
            // Anything to do with Work Items goes here:
            "wiModel": {
                "workItems": [],
                "workItemFilter": "",
                "selectedWorkItem": null
            },
            // Anything to do with Process Definitions goes here:
            "pdModel": {
                "processDefinitions": [],
                "processDefinitionFilter": "",
                "selectedProcessDefinition": null
            },
            "selectedTab": "home",
            "selectedLanguage": "fr"
        };
    }

    // ConfigFactory checks for a global variable named: configSettings
    // Typically, this is set in the file: configSettings.js
    // If found, it uses the configuration information from object.
    function ConfigFactory() {
        return {
            get: function () {
                var myConfigSettings = {};

                // check if configSetting object exists, if so we use that.
                if (typeof configSettings === "undefined") {
                    myConfigSettings = {
                        "aaServerUrl": "./"
                    };
                }
                else {
                    myConfigSettings = configSettings;
                }

                // If the server is blank we set it to "./" so it uses the local server
                if (!myConfigSettings.aaServerUrl) {
                    myConfigSettings.aaServerUrl = "./";
                }

                // bpm-angular takes care of checking for a / at the end of the URL but we need to check for the auth server.
                if (myConfigSettings.aaServerUrl) {
                    myConfigSettings.aaServerUrl = myConfigSettings.aaServerUrl.endsWith("/") ? myConfigSettings.aaServerUrl : myConfigSettings.aaServerUrl + "/";
                }

                console.log("myConfigSettings", myConfigSettings);
                return myConfigSettings;
            }
        };
    }

    // This is the Angular Controller for the main page of the Forms Tutorial.
    BPMController.$inject = ['$scope', 'BPM', 'ApplicationState', 'ConfigFactory'];
    function BPMController($scope, BPM, ApplicationState, ConfigFactory) {

        // This is called by slap.js after we are logged in.
        // This function is passed into SLAP.initLogin(loginConfig , {}, loginCallback);
        // We wait until verified is true, then we know we are connected.
        function loginCallback(loginInfo) {
            // update the display of who is logged in according to login info
            ApplicationState.connectionModel.loginInfo = loginInfo;

            if (ApplicationState.connectionModel.loginInfo.verified) {
                // We only want to call initForm once.
                // So check if we are already logged in.
                if (!ApplicationState.connectionModel.loggedIn) {
                    ApplicationState.connectionModel.loggedIn = true;
                    initForm();
                }
            }
        }

        // In this method we do all the code to initialize our form.
        // This is called by loginCallback() once we are
        // confirmed to be logged in to BPM through Agile Adapter.
        function initForm() {
            // We are logged now so:
            // Get the BPM Connection Object and initialize the WorkItemFactory

            BPM.initialize($scope.connectionModel.configSettings.aaServerUrl, $scope.connectionModel.configSettings.tenant, $scope.connectionModel.configSettings.application);

            // Now we change the filter so the <bpm-work-item-fetcher>
            // will update the Work List from Agile Adapter.
            ApplicationState.wiModel.workItemFilter = "MyCurrentWorkItems";
            ApplicationState.piModel.processInstanceFilter = "AllProcesses";
            ApplicationState.pdModel.processDefinitionFilter = "AllPlans";

            // This is purely for AngularJS.
            // We need to tell Angular that the scope has been updated.
            $scope.$apply();
        }


        initialize();
        function initialize() {
            // ApplicationState is where we store all of our application's state.
            $scope.appState = ApplicationState;
            $scope.connectionModel = ApplicationState.connectionModel;
            $scope.connectionModel.configSettings = ConfigFactory.get();

            //Any error that happens in the page will send this event.
            $scope.$on('bpmErrorEvent',
                function (event, data) {
                    console.log('BPMController: Error from server:', data);
                    var messageError = (data) ? (data.error) : null;
                    ApplicationState.connectionModel.message = { type: "danger", text: "There was an error.", error: messageError };
                    console.log('BPMController: message:', ApplicationState.connectionModel.message);
                }
            );

            console.log("$scope.connectionModel.configSettings", $scope.connectionModel.configSettings);
            // providerUrl is no longer used.
            var loginConfig = {
                "providerUrl": "",
                "serverUrl": $scope.connectionModel.configSettings.aaServerUrl
            };

            // This is the call that actually initiates the login.
            SLAP.initLogin(loginConfig, {}, loginCallback);
        }
    }

})();

/////////////////////////////////////////
// Polyfills for Microsoft IE
/////////////////////////////////////////

// Array.find()
console.log("Check for Array.prototype.find.");
if (!Array.prototype.find) {
    console.log("Array.prototype.find is undefined. Using polyfill.");
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            'use strict';
            if (this == null) {
                throw new TypeError('Array.prototype.find called on null or undefined');
            }
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;

            for (var i = 0; i < length; i++) {
                value = list[i];
                if (predicate.call(thisArg, value, i, list)) {
                    return value;
                }
            }
            return undefined;
        }
    });
}




