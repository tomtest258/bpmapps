
(function () {
    "use strict";

    angular
        .module('BPM')
        .controller('HomeCtrl', HomeCtrl)
    ;

    HomeCtrl.$inject = ['$scope','ApplicationState','BPM','$timeout','$interval','$http','BPMAdmin','httpFactory'];
    function HomeCtrl($scope,ApplicationState,BPM,$timeout,$interval,$http,BPMAdmin,httpFactory) {
    	$scope.connection = {};
        $scope.rulesTenant = "";
    	$scope.results = [];
		$scope.processing = false;
    	$scope.totalTests = 0;
    	$scope.totalSucceededTests = 0;
    	$scope.totalFailedTests = 0;
    	
        // Functions
    	$scope.test = test;
    	$scope.testTCK = testTCK
    	$scope.statusColor = statusColor;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
    	
    	function statusColor(result) {
            if (result.passed) {
                return {"color":"green"};
            } else {
                return {"color":"red"};
            }
        }
    	
    	function testTCK() {
    		var rulesRootURL = $scope.connection.aaServerUrl+"/api/t="+$scope.connection.tenant+"/dms/r=1/apps/" + $scope.connection.application + "/Rules";
    		if($scope.processing == true) {
 			   return;
 			}
 			$scope.error = "";
     		$scope.results = [];
     		$scope.totalSucceededTests = 0;
     		$scope.totalTests = 0;
         	$scope.totalFailedTests = 0;
 			$scope.processing = true;
 			$scope.numberOfProcessedSuites = 0;
    		httpFactory.getRequest(rulesRootURL)
            .success( function(data) {
            	$scope.ruleTestSuites = [];
            	for(var i=0; i < data.contents.length; i++) {
                	if(data.contents[i].isFolder && (data.contents[i].name.charAt(0)=="0" || data.contents[i].name.charAt(0)=="1")) {
                		$scope.ruleTestSuites.push({"name":data.contents[i].name});
                	}
            	}
            	for(var k=0; k < $scope.ruleTestSuites.length; k++) {
            		handleTestTCKTestSuite(rulesRootURL,$scope.ruleTestSuites[k]);
            	}
            })
            .error( function(data, status, headers, config) {
                $scope.error = "Failed to get Rules root folder " + rulesRootURL +", error is " + data;
				$scope.processing = false;
            });
    	}
    	
    	function handleTestTCKTestSuite(rulesRootURL, testSuite) {
    		httpFactory.getRequest(rulesRootURL + "/"+testSuite.name)
    		.success( function(data,status, headers,config) {
    			for(var i=0; i < data.contents.length; i++) {
    				if(!data.contents[i].isFolder && endsWith(data.contents[i].name,".xml")) {
        				//////////////////////////////////////////////
    					testSuite.testFile = data.contents[i].name.substring(0,data.contents[i].name.indexOf(".xml"));
    					var ruleTestFileURL = rulesRootURL + "/"+testSuite.name+"/"+data.contents[i].name;	
    					httpFactory.getRequest(ruleTestFileURL)
                		.success( function(data) {
                			var XML = new DOMParser().parseFromString(data, "text/xml");
                			testSuite.test = removeNameSpacePrefixFromObj(JSON.parse(xml2json(XML)));
                			var dmnFileUrl = rulesRootURL + "/"+testSuite.name + "/" + testSuite.test.testCases.modelName;
                			testSuite = convertTestSpecForTestTCK(testSuite);
                			console.log("Test suite >> " + JSON.stringify(testSuite));
                			for(var p=0; p < testSuite.newTestCases.length; p++) {
                				$scope.totalTests = $scope.totalTests + numAttrs(testSuite.newTestCases[p].expectedOutcome.results);
                			}
                			$scope.numberOfProcessedSuites = $scope.numberOfProcessedSuites + 1;
                			if($scope.numberOfProcessedSuites == $scope.ruleTestSuites.length) {
                				startTestTCKTestSuites($scope.ruleTestSuites);
                			}
                		})
                		.error( function(data, status, headers, config) {
                			$scope.error = "Failed to get rule test file " + ruleTestFileURL +", error is " + data;
            				$scope.processing = false;
                		});
    					//////////////////////////////////////////////
        			}
    			}
    		})
    		.error( function(data, status, headers, config) {
    			$scope.error = "Failed to get rule folder " + rulesRootURL + "/"+$scope.ruleTestSuites[k].name +", error is " + data;
				$scope.processing = false;
    		});
    	}
    	
    	function removeNameSpacePrefixFromObj(obj) {
    		var newObj = {};
    		for(var key in obj) {
    			if(obj.hasOwnProperty(key)) {
    				var newKey = key;
    				if(key.indexOf(":")>=0) {
    					newKey = key.substring(key.indexOf(":")+1);
    				}
    				if(obj[key] == null) {
    					newObj[newKey] = null;
    				} else if(typeof obj[key] === 'object' && obj[key].constructor === Array) {
    					newObj[newKey] = removeNameSpacePrefixFromArray(obj[key]);
    				} else if(typeof obj[key] === 'object' && obj[key].constructor === Object) {
    					newObj[newKey] = removeNameSpacePrefixFromObj(obj[key]);
    				} else {
    					newObj[newKey] = obj[key];
    				}
    			}
    		}
    		return newObj;
    	}
    	
    	function removeNameSpacePrefixFromArray(arr) {
    		var newArr = [];
    		for(var i=0; i < arr.length; i++) {
    			if(arr[i] == null) {
    				newArr[i] = null;
    			} else if(typeof arr[i] === 'object' && arr[i].constructor === Array) {
    				newArr[i] = removeNameSpacePrefixFromArray(arr[i]);
    			} else if(typeof arr[i] === 'object' && arr[i].constructor === Object) {
    				newArr[i] = removeNameSpacePrefixFromObj(arr[i]);
				} else {
					newArr[i] = arr[i];
				}
    		}
    		return newArr;
    	}
    	
    	
    	function numAttrs(obj) {
    	   var count = 0;
    	   for(var key in obj) {
    		  if(obj.hasOwnProperty(key)) {
    		    ++count;
    		  }
    	   }
    	   return count;
    	}
    	
    	function startTestTCKTestSuites(testSuites) {
    		for(var i=0; i < testSuites.length; i++) {
    			var testSuite = testSuites[i];
    			testSuite.report = [];
    			for(var k=0; k < testSuite.newTestCases.length; k++) {
    				for(var p in testSuite.newTestCases[k].expectedOutcome.results) {
    					var newTestCase = JSON.parse(JSON.stringify(testSuite.newTestCases[k]));
    					testSuite.report.push(newTestCase);
    					newTestCase.input.decisionCriteria={};
						var thisRulePath = newTestCase.input.rules[0];
						newTestCase.input.decisionCriteria[thisRulePath] = {};
    					if(newTestCase.decisions.hasOwnProperty(p)) {
    						if(newTestCase["decisionService"] != "") {
    							newTestCase.input.decisionCriteria[thisRulePath]["decisionName"] = newTestCase["decisionService"];
    							newTestCase.input.decisionCriteria[thisRulePath]["isDecisionService"] = true;
    						} else {
    							newTestCase.input.decisionCriteria[thisRulePath]["decisionName"] = newTestCase.decisions[p];
    							newTestCase.input.decisionCriteria[thisRulePath]["isDecisionService"] = false;
    						}
    						if(newTestCase.expectedOutcome.results[p]["ignoreError"] == true) {
    							newTestCase.input.decisionCriteria[thisRulePath]["ignoreError"] = true;
    						}
    					}
    					runSingleTestTCK(newTestCase,p);
    				}
    			}
    		}
    	}
    	
    	function runSingleTestTCK(testCase,resultNode) {
        	var testResult = {};
        	testResult["showTest"] = true;
        	testResult["showInput"] = false;
        	testResult["showExpected"] = false;
        	testResult["showActual"] = false;
        	testResult["input"] = testCase.input;
        	testResult["error"] = null;
        	testResult["test"] = testCase.input.rules[0];
        	var evaluateRulesURL = $scope.connection.aaServerUrl+"/api/t="+$scope.connection.tenant+"/a=" + $scope.connection.application+"/EvaluateRules";
        	httpFactory.postJsonRequest(testCase.input,evaluateRulesURL)
            .success( function(data) {
            	var outcome = data;
            	var newExpectedOutcome = {"results":{}};
            	var newActualOutcome = {"results":{}};
            	newExpectedOutcome.results[resultNode] = testCase.expectedOutcome.results[resultNode].value;
            	newActualOutcome.results[resultNode] = outcome.results[resultNode];
            	try {
            		handleTCKTestResult(newExpectedOutcome,newActualOutcome);
            	} catch(e) {
            		console.log("Failed to handle TCK rules result.", "Expected="+JSON.stringify(newExpectedOutcome),
            				" Actual="+JSON.stringify(newActualOutcome), " Error=" + e, e.stack);
            		$scope.totalFailedTests = $scope.totalFailedTests + 1;
                	testCase.expectedOutcome.status[resultNode] = "FAIL";
                	testResult["error"] = ""+e;
                	$scope.results.push(testResult);
    				$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
    				if(!$scope.processing) {
    					generateReportCSV($scope.ruleTestSuites);
    				}
    				return;
            	}
            	testResult["expected"] = newExpectedOutcome;
            	testResult["actual"] = newActualOutcome;
            	testResult["passed"] = isEqual(testResult["expected"], testResult["actual"]);
            	if(!testResult["passed"]) {
            		$scope.totalFailedTests = $scope.totalFailedTests + 1;
            		testCase.expectedOutcome.status[resultNode] = "FAIL";
            	} else {
            		$scope.totalSucceededTests = $scope.totalSucceededTests + 1;
            		testCase.expectedOutcome.status[resultNode] = "PASS";
            	}
            	$scope.results.push(testResult);
				$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
				if(!$scope.processing) {
					generateReportCSV($scope.ruleTestSuites);
				}
            })
            .error( function(data, status, headers, config) {
            	$scope.totalFailedTests = $scope.totalFailedTests + 1;
            	testCase.expectedOutcome.status[resultNode] = "FAIL";
            	testResult["error"] = "Failed to invoke rules service " + evaluateRulesURL +", error is " + JSON.stringify(data);
            	$scope.results.push(testResult);
				$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
				if(!$scope.processing) {
					generateReportCSV($scope.ruleTestSuites);
				}
            });
        }
    	
    	function generateReportCSV(ruleTestSuites) {
    		var report = [];
    		for(var p=0; p < ruleTestSuites.length; p++) {
    			var testSuite = ruleTestSuites[p];
    			var rulePath = null;
    			var testFile = testSuite.testFile;
    			for(var k=0; k < testSuite.report.length; k++) {
    				var testCase = testSuite.report[k];
    				var reportItem = [];
    				var success = true;
    				var errorResultNode = "";
    				if(testCase.compliance == 2) {
        				rulePath = "compliance-level-2" + "/" + testSuite.name;
        			} else if(testCase.compliance == 3) {
        				rulePath = "compliance-level-3" + "/" + testSuite.name;
        			} else {
        				rulePath = "compliance-level-3" + "/" + testSuite.name;
        			}
    				reportItem.push(rulePath);
    				reportItem.push(testFile);
    				reportItem.push(pad3(""+testCase.id));
    				for(var m in testCase.expectedOutcome.status) {
    					if(testCase.expectedOutcome.hasOwnProperty("status") && 
    					   testCase.expectedOutcome.status.hasOwnProperty(m) &&
    					   testCase.expectedOutcome.status[m] == 'FAIL') {
    						success = false;
    						errorResultNode = errorResultNode + m + ",";
    					}
    				}
    				if(success == true) {
    					reportItem.push("SUCCESS");
    					reportItem.push("");
    				} else {
    					reportItem.push("FAILURE");
    					reportItem.push(errorResultNode);
    				}
    				var newReportItem = findReportItem(report, reportItem);
    				if(newReportItem == null) {
    					report.push(reportItem);
    				} else {
    					if(newReportItem[3] == "SUCCESS" && reportItem[3] == "FAILURE") {
    						newReportItem[3] = "FAILURE";
    						newReportItem[4] = reportItem[4];
    					} else if(newReportItem[3] == "FAILURE") {
    						newReportItem[4] = newReportItem[4] + reportItem[4];
    					}
    				}
    			}
    		}
    		report.sort(function(a, b){
    		    var nameA=a[0].toLowerCase(), nameB=b[0].toLowerCase()
    		    if (nameA < nameB) {
    		        return -1 
    		    } else if (nameA > nameB) {
    		        return 1
    		    } else {
    		        return 0
    		    }
    		})
    		console.log("The test report >> " + report);
    		var BB = new Blob(
                 [reportToCSV(report)]
                 , {type: "application/json;charset=utf-8"}
            );
            saveAs(
                 BB, "tck_results.csv"
            );
    	}
    	
    	function findReportItem(report, item) {
    		for(var p=0; p < report.length; p++) {
    			if(report[p][0] == item[0] && report[p][1] == item[1] && report[p][2] == item[2]) {
    				return report[p];
    			}
    		}
    		return null;
    	}
    	
    	function pad3(str) {
    		if(str.length == 1) {
    			return '00' + str;
    		} else if(str.length == 2) {
    			return '0'+str;
    		} else {
    			return str;
    		}
    	}
    	
    	function reportToCSV(report) {
    		var csv = "";
    		for(var i=0; i < report.length; i++) {
    			csv = csv + "\"" + report[i][0].replace(new RegExp('"', 'g'), '""') + "\"" + ",";
    			csv = csv + "\"" + report[i][1].replace(new RegExp('"', 'g'), '""') + "\"" + ",";
    			csv = csv + "\"" + report[i][2].replace(new RegExp('"', 'g'), '""') + "\"" + ",";
    			csv = csv + "\"" + report[i][3].replace(new RegExp('"', 'g'), '""') + "\"" + ",";
    			csv = csv + "\"" + report[i][4].replace(new RegExp('"', 'g'), '""') + "\"";
    			if(i != (report.length - 1)) {
    				csv = csv + "\n";
    			}
    		}
    		return csv;
    	}
    	
    	function handleArrayResult(expectedArr, actualArr) {
    		for(var k=0; k < actualArr.length; k++) {
    			var actualItem = actualArr[k];
    			var expectedItem = expectedArr[k];
    			if(actualItem == null) {
    				;
    			} else if(Array.isArray(actualItem)) {
    				handleArrayResult(expectedItem,actualItem);
    			} else if(isObject(actualItem)) {
    				handleObjectResult(expectedItem,actualItem);
    			} else {
    				handlePrimitiveResult(expectedArr, actualArr, k);
    			}
    		}
    	}
    	
    	function handleObjectResult(expectedObj, actualObj) {
    		for(var p in actualObj) {
    			var actualItem = actualObj[p];
    			var expectedItem = expectedObj[p];
    			if(actualItem == null) {
    				;
    			} else if(Array.isArray(actualItem)) {
    				handleArrayResult(expectedItem,actualItem);
    			} else if(isObject(actualItem)) {
    				handleObjectResult(expectedItem,actualItem);
    			} else {
    				handlePrimitiveResult(expectedObj, actualObj, p);
    			}
    		}
    	}
    	
    	function convertEpochMilliToDateTimeString(milli) {
    		var dt = moment(milli);
    		if(dt.isValid()) {
    			return dt.format();
    		} else {
    			return null;
    		}
    	}
    	
    	
    	function convertEpochMilliToDateTimeWithZoneString(milli, zoneOffset) {
    	    var dt = moment(milli);
    	    if(dt.isValid()) {
    	    	return dt.zone(zoneOffset).format("YYYY-MM-DDThh:mm:ss.SSSZ");
    	    } else {
    	    	return null;
    	    }
    	}
    	
    	function handleDateTimeResult(expected, actual, k) {
    		var resstr = "" + actual[k];
    		var actualDate = (new Date(parseFloat(resstr)));
			var negativedatetimefmt = /^-\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[.]?\d*[+-]\d{2}:\d{2}$/;
			
			if(!isString(expected[k])) {
				actual[k] = convertEpochMilliToDateTimeString(actualDate.getTime());
				expected[k] = convertEpochMilliToDateTimeString(parseFloat(expected[k]));
			} else if(expected[k].match(negativedatetimefmt)) {
				actual[k] = convertEpochMilliToDateTimeWithZoneString(actualDate.getTime(),expected[k].substring(expected[k].length-6));
			}
    	}
    	
    	function handlePrimitiveResult(expected, actual, k) {
    		var res = actual[k];
    		if(res == null) {
    			return;
    		} else if(isString(res)) {
    			if(res.trim().length == 0 && expected[k] == null) {
    				expected[k] = res;
    			} else {
					if(res.indexOf("duration(") == 0) {
						res = res.replace(/\s/g,'');
						var durationRegex = /duration\(\"(.*)\"\)/;
						var durationMatches = res.match(durationRegex);
						if(durationMatches.length == 2) {
							if(expected[k] == durationMatches[1]) {
								expected[k] = "duration(\"" + expected[k] + "\")"
								actual[k] = res;
							} else if(durationMappings.hasOwnProperty(expected[k])) {
								if(durationMappings[expected[k]] == durationMatches[1]) {
									expected[k] = "duration(\"" + durationMappings[expected[k]] + "\")"
									actual[k] = res;
								}
							}
						}
					} else if(durationMappings.hasOwnProperty(expected[k])) {
						expected[k] = durationMappings[expected[k]];
					}
				}
    		} else if(typeof(res) === "boolean") {
    			;
    		} else if(!isNaN(res)) {
    			var resstr = ""+res;
    			if(resstr.indexOf(".") > 0 && (resstr.length - resstr.indexOf(".")) >= 8) {
    				if(Math.abs(parseFloat(resstr) - parseFloat(""+expected[k])) < 0.00000001) {
    					actual[k] = parseFloat(resstr).toFixed(8);
    					expected[k] = actual[k];
    				} else {
    					actual[k] = parseFloat(resstr).toFixed(8);
        				expected[k] = parseFloat(""+expected[k]).toFixed(8);
    				}
    			} else {
    				if((parseFloat(resstr) >= 943250400000 && parseFloat(resstr) < 1630070585990) ||
    						(parseFloat(resstr) < -1630070585990)) {
    					handleDateTimeResult(expected, actual, k);
    				} else {
    					actual[k] = parseFloat(resstr);
        				expected[k] = parseFloat(expected[k]);
    				}
    			}
    		}
    	}
    	
    	function handleTCKTestResult(expectedObj, actualObj) {
    		for(var p in actualObj.results) {
        		var actualItem = actualObj.results[p];
        		var expectedItem = expectedObj.results[p];
        		if(actualItem == null) {
        			;
        		} else if(Array.isArray(actualItem)) {
    				handleArrayResult(expectedItem,actualItem);
    			} else if(isObject(actualItem)) {
    				handleObjectResult(expectedItem, actualItem);
    			} else {
    				handlePrimitiveResult(expectedObj.results, actualObj.results, p);
    			}
    		}
    	}
    	
    	function isObject(value) {
    		return value && typeof value === 'object' && value.constructor === Object;
    	}
    	
    	function isString(value) {
    		return typeof value === 'string' || value instanceof String;
    	}
    	
    	function convertTestSpecForTestTCK(testSuite) {
    		var testCases=[];
    		var modelName;
    		var compliance = 0;
    		testSuite.newTestCases = [];
    		if(testSuite.test.testCases.hasOwnProperty("testCase")) {
    			testCases.push(testSuite.test.testCases);
    			modelName = testSuite.test.testCases.modelName;
    			compliance = getCompliance(testSuite.test.testCases.labels);
    		} else {
    			for(var i=0; i < testSuite.test.testCases.length; i++) {
    				if(testSuite.test.testCases[i].hasOwnProperty("testCase")) {
    					testCases.push(testSuite.test.testCases[i]);
    				} else if(testSuite.test.testCases[i].hasOwnProperty("modelName")) {
    					modelName = testSuite.test.testCases[i].modelName;
    				} else if(testSuite.test.testCases[i].hasOwnProperty("labels")) {
    					compliance = getCompliance(testSuite.test.testCases[i].labels);
    				}
    			}
    		}
    		for(var k=0; k < testCases.length; k++) {
    			var testCase = testCases[k];
    			var newTestCase = {"id":testCase["testCase@id"],"compliance":"","decisionService":"","decisions":{},"input":{},"expectedOutcome":{"results":{},"status":{}}};
    			var inputNodes = [];
    			var resultNodes = [];
    			
    			newTestCase.compliance = compliance;
    			
    			if(testCase.hasOwnProperty("testCase@type") && testCase["testCase@type"] == "decisionService") {
    				newTestCase["decisionService"] = testCase["testCase@invocableName"].trim();
    			}
    			
    			if(testCase.testCase.hasOwnProperty("inputNode")) {
    				inputNodes.push(testCase.testCase);
    			} else {
    				for(var p=0; p < testCase.testCase.length; p++) {
    					var item = testCase.testCase[p];
    					if(item.hasOwnProperty("inputNode")) {
    						inputNodes.push(item);
    					}
    				}
    			}
    			if(testCase.testCase.hasOwnProperty("resultNode")) {
    				resultNodes.push(testCase.testCase);
    			} else {
    				for(var p=0; p < testCase.testCase.length; p++) {
    					var item = testCase.testCase[p];
    					if(item.hasOwnProperty("resultNode")) {
    						resultNodes.push(item);
    					}
    				}
    			}
    			newTestCase.input.rules = [testSuite.name+"/"+modelName];
    			newTestCase.input.conditionValues = {};
    			for(var x=0; x<inputNodes.length; x++) {
    				var item = inputNodes[x];
    				newTestCase.input.conditionValues[item["inputNode@name"]] = getTestInputDataValue(item);
    			}
    			for(var x=0; x<resultNodes.length; x++) {
    				var item = resultNodes[x];
    				if(item["resultNode@type"] == "decision" || newTestCase["decisionService"] != "") {
    					newTestCase.decisions[item["resultNode@name"]]=item["resultNode@name"];
    				}
    				newTestCase.expectedOutcome.results[item["resultNode@name"]] = {
    						"value":getTestOutputDataValue(item),
    				        "ignoreError":item["resultNode@errorResult"] == "true"
    				}
    			}
    			testSuite.newTestCases.push(newTestCase);
    		}
    		
    		return testSuite;
    	}
    	
    	function getCompliance(labels) {
    		var compliance = 0;
    		for(var m=0; m < labels.length; m++) {
				var label = labels[m];
				if(label.label.toUpperCase() == "COMPLIANCE LEVEL 2") {
					compliance = 2;
					break;
				} else if(label.label.toUpperCase() == "COMPLIANCE LEVEL 3") {
					compliance = 3;
					break;
				} 
			}
    		return compliance;
    	}
    	
    	function getListOfValues(items) {
    		var itemList = [];
    		var newItems = [];
    		if(items == null) {
    			return [];
    		} 
    		if(Array.isArray(items)) {
    			newItems = items;
    		} else {
    			newItems.push(items);
    		}
    		for(var i=0; i < newItems.length; i++) {
    			var item = newItems[i];
    			if(item.item.hasOwnProperty("value")) {
    				itemList.push(getValueFromString(item.item));
    			} else if(item.item.hasOwnProperty("list")) {
    				itemList.push(getListOfValues(item.item.list))
    			} else {
    				itemList.push(getComponentValues(item.item))
    			}
    		}
    		return itemList;
    	}
    	
    	function getComponentValues(items) {
    		var component = {};
    		var newItems = [];
    		if(Array.isArray(items)) {
    			newItems = items;
    		} else {
    			newItems.push(items);
    		}
    		for(var p=0; p < newItems.length; p++) {
    			var item = newItems[p];
    			if(item["component"].hasOwnProperty("value")) {
    				component[item["component@name"]] =  getValueFromString(item["component"])
    			} else if(item["component"].hasOwnProperty("list")) {
    				component[item["component@name"]] = getListOfValues(item["component"].list);
    			} else {
    				component[item["component@name"]] = getComponentValues(item["component"]);
    			}
    		}
    		return component;
    	}
    	
    	function getTestInputDataValue(item) {
    		if(item["inputNode"].hasOwnProperty("value")) {
    			return getValueFromString(item["inputNode"]);
    		} else if(item["inputNode"].hasOwnProperty("list")) {
    			return getListOfValues(item["inputNode"]["list"]);
    		} else {
    			return getComponentValues(item["inputNode"]);
    		}
    	}
    	
    	function getTestOutputDataValue(item) {
    		if(item["resultNode"]["expected"].hasOwnProperty("value")) {
    			return getValueFromString(item["resultNode"]["expected"]);
    		} else if(item["resultNode"]["expected"].hasOwnProperty("list")) {
    			return getListOfValues(item["resultNode"]["expected"]["list"]);
    		} else {
    			return getComponentValues(item["resultNode"]["expected"]);
    		}
    	}
    	
    	function stringToEpochMilli(datestr) {
    		var dt = moment(datestr);
    		if(dt.isValid()) {
    			return dt.valueOf();
    		} else {
    			return Date.parse(datestr);
    		}
    	}
    	
    	function getValueFromString(value) {
    		var val = null;
    		var type = null;
    		var str = value["value"];
    		if(value.hasOwnProperty("type")) {
    			type = value["type"];
    		}
    		if(str == null) {
    			val = null;
    		} else if(str == "true") {
    			val = true;
    		} else if(str == "false") {
    			val = false;
    		} else if(type != null && type == "xsd:decimal") {
    			val = parseFloat(str);
    		} else if(isString(str)) {
    			if(durationMappings.hasOwnProperty(str)) {
					val = str;
    			} else if(type != null) {
    				if(type == "xsd:dateTime" || type == "xs:dateTime") {
    					var re = /^[-]?\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[.]?\d*[+-]\d{2}:\d{2}$/;
    					var re2 = /^[-]?\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[.]?\d*Z$/;
    					if(str.match(re) || str.match(re2)) {
    						val = stringToEpochMilli(str);
    					} else {
    						val = str;
    					}
    					if(isNaN(val)) {
    						val = str;
    					}
    				} else {
    					val = str;
    				}
    			} else {
    				val = str;
    			}
    		} else if(!isNaN(str)) {
    			val = parseFloat(str);
    		} else {
    			val = str;
    		}
    		return val;
    	}
    	
    	function test() {
    		var getURL = $scope.connection.aaServerUrl+"/api/t="+$scope.connection.tenant+"/dms/r=1/apps/" + $scope.connection.application;
    		if($scope.processing == true) {
			   return;
			}
			$scope.error = "";
    		$scope.results = [];
    		$scope.totalSucceededTests = 0;
    		$scope.totalTests = 0;
        	$scope.totalFailedTests = 0;
			$scope.processing = true;
    		httpFactory.getRequest(getURL)
            .success( function(data) {
                var appFiles = [];
                var i=0;
                for(i=0; i < data.contents.length; i++) {
                	if(!data.contents[i].isFolder && endsWith(data.contents[i].name,"_rules_test.json")) {
                		appFiles.push(data.contents[i].name);
                    }
                }
                $scope.totalTests = appFiles.length;
                for(i=0; i < appFiles.length; i++) {
                	runSingleTest(getURL,appFiles[i]);
                }
            })
            .error( function(data, status, headers, config) {
                $scope.error = "Failed to get files under application root with url " + getURL +", error is " + data;
				$scope.processing = false;
            });
    	}
            
        function runSingleTest(dmsRootURL, testFile) {
        	var testFileURL = dmsRootURL + "/" + testFile;
        	var testResult = {};
        	testResult["showTest"] = true;
        	testResult["showInput"] = false;
        	testResult["showExpected"] = false;
        	testResult["showActual"] = false;
        	testResult["test"] = testFile;
        	
        	httpFactory.getRequest(testFileURL)
            .success( function(data) {
            	var content = data;
            	testResult["input"] = content.input;
            	testResult["expected"] = content.expectedOutcome;
            	testResult["error"] = null;
            	var evaluateRulesURL = $scope.connection.aaServerUrl+"/api/t="+$scope.connection.tenant+"/a=" + $scope.connection.application+"/EvaluateRules";
            	httpFactory.postJsonRequest(content.input,evaluateRulesURL)
                .success( function(data) {
                	var outcome = data;
                	testResult["actual"] = outcome;
                	testResult["passed"] = isEqual(testResult["expected"], outcome);
                	if(!testResult["passed"]) {
                		$scope.totalFailedTests = $scope.totalFailedTests + 1;
                	} else {
                		$scope.totalSucceededTests = $scope.totalSucceededTests + 1;
                	}
                	$scope.results.push(testResult);
					$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
                })
                .error( function(data, status, headers, config) {
                	$scope.totalFailedTests = $scope.totalFailedTests + 1;
                	testResult["error"] = "Failed to invoke rules service " + evaluateRulesURL +", error is " + JSON.stringify(data);
                	$scope.results.push(testResult);
					$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
                });
            })
            .error( function(data, status, headers, config) {
            	$scope.totalFailedTests = $scope.totalFailedTests + 1;
            	testResult["error"] = "Failed to get test file with url " + testFileURL +", error is " + data;
            	$scope.results.push(testResult);
				$scope.processing = !($scope.totalFailedTests+$scope.totalSucceededTests==$scope.totalTests);
            });
        }
        
        function isEqual(value, other) {
        	
        	// Get the value type
        	var type = Object.prototype.toString.call(value);

        	// If the two objects are not the same type, return false
        	if (type !== Object.prototype.toString.call(other)) return false;

        	// If items are not an object or array, return false
        	if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

        	// Compare the length of the length of the two items
        	var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
        	var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
        	if (valueLen !== otherLen) return false;

        	// Compare two items
        	var compare = function (item1, item2) {

        		// Get the object type
        		var itemType = Object.prototype.toString.call(item1);

        		// If an object or array, compare recursively
        		if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
        			if (!isEqual(item1, item2)) return false;
        		}

        		// Otherwise, do a simple comparison
        		else {

        			// If the two items are not the same type, return false
        			if (itemType !== Object.prototype.toString.call(item2)) return false;

        			// Else if it's a function, convert to a string and compare
        			// Otherwise, just compare
        			if (itemType === '[object Function]') {
        				if (item1.toString() !== item2.toString()) return false;
        			} else {
        				if (item1 !== item2) return false;
        			}

        		}
        	};

        	// Compare properties
        	if (type === '[object Array]') {
        		for (var i = 0; i < valueLen; i++) {
        			if (compare(value[i], other[i]) === false) return false;
        		}
        	} else {
        		for (var key in value) {
        			if (value.hasOwnProperty(key)) {
        				if (compare(value[key], other[key]) === false) return false;
        			}
        		}
        	}

        	// If nothing failed, return true
        	return true;

        };
    	
        function endsWith(str,suffix) {
            return str.substring(str.length - suffix.length, str.length ) === suffix;
        }
    	
    	function loginCallback(loginInfo) {
            // update the display of who is logged in according to login info
            ApplicationState.connectionModel.loginInfo = loginInfo;
            if (ApplicationState.connectionModel.loginInfo.verified) {
            	BPM.initialize($scope.connection.aaServerUrl, $scope.connection.tenant, $scope.connection.application);
            	BPMAdmin.initialize($scope.connection.aaServerUrl);
            	$scope.loginUser = ApplicationState.connectionModel.loginInfo.userId;
            	$scope.$apply();
            }
        }
    	
        initialize();
        function initialize() {
        	$scope.connection = getServerConnection();
        	
        	// ApplicationState is where we store all of our application's state.
            $scope.appState = ApplicationState;
            var loginConfig = {
                    "providerUrl": "",
                    "serverUrl": $scope.connection.aaServerUrl
            };

            // This is the call that actually initiates the login.
            SLAP.initLogin(loginConfig, {}, loginCallback);
        }
    }

})();
