    function toTwoDigits(digit) {
        if(digit < 10) {
    		return "0" + digit;
    	} else {
    		return digit;
    	}
    }
    	
    function getCurrentTimeString() {
    	var now = new Date();
    	return toTwoDigits(now.getHours()) + ":" + toTwoDigits(now.getMinutes()) + ":" + toTwoDigits(now.getSeconds());
    }
    	
    function getRandomInteger(min, max) {
    	return Math.floor(Math.random() * (max - min)) + min;
    }    

    function CSV2JSON(csv) {
        var array = CSVToArray(csv);
        var objArray = [];
        for (var i = 1; i < array.length; i++) {
            var newRow = {};
            for (var k = 0; k < array[0].length && k < array[i].length; k++) {
                var value = array[i][k];
                if (value) {
                   var key = array[0][k];
                   newRow[key] = value;
                }
            }
            objArray[i - 1] = newRow;
        }
        var json = JSON.stringify(objArray);
        var str = json.replace(/},/g, "},\r\n");
        return str;
    }
    
    function CSVToArray(strData, strDelimiter) {
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");
        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp((
        // Delimiters.
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
        // Quoted fields.
        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
        // Standard fields.
        "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData = [[]];
        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches = null;
        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        while (arrMatches = objPattern.exec(strData)) {
            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[1];
            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push([]);
            }
            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[2]) {
                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                var strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"), "\"");
            } else {
                // We found a non-quoted value.
                var strMatchedValue = arrMatches[3];
            }
            // Now that we have our value string, let's add
            // it to the data array.
            arrData[arrData.length - 1].push(strMatchedValue);
        }
        // Return the parsed data.
        return (arrData);
    }
    
    //////////////////////////////////////////////////////
    //The following method is used for predictive analysis
    //////////////////////////////////////////////////////
    
    function getActivityNameById(nodes, id) {
		for(var k=0; k < nodes.length; k++) {
			var node = nodes[k];
			if(node.id == id) {
				return node.name;
			}
		}
		return null;
	}
	
	function findTargetActivities(nodes, nodeName) {
		var targetActivities = [""];
		var selectedNode = null;
		for(var k=0; k < nodes.length; k++) {
			var node = nodes[k];
			if(node.name == nodeName) {
				selectedNode = node;
				break;
			}
		}
		if(selectedNode) {
			var outgoingArrows = selectedNode.arrowsOut;
			for(var k=0; k < outgoingArrows.length; k++) {
				var targetAct = getActivityNameById(nodes, outgoingArrows[k].targetNode);
				if(targetActivities.indexOf(targetAct) < 0) {
					targetActivities.push(targetAct);
				}
			}
		}
		return targetActivities;
	}
    
	function parse_url(url) {
	    var match = url.match(/^(http|https|ftp)?(?:[\:\/]*)([a-z0-9\.-]*)(?:\:([0-9]+))?(\/[^?#]*)?(?:\?([^#]*))?(?:#(.*))?$/i);
	    var ret   = new Object();
	    ret['protocol'] = '';
	    ret['host']     = match[2];
	    ret['port']     = '';
	    ret['path']     = '';
	    ret['query']    = '';
	    ret['fragment'] = '';
	    if(match[1]){
	       ret['protocol'] = match[1];
	    }
	    if(match[3]){
	       ret['port']     = match[3];
	    }
	    if(match[4]){
	       ret['path']     = match[4];
	    }
	    if(match[5]){
	       ret['query']    = match[5];
	    }
	    if(match[6]){
	       ret['fragment'] = match[6];
	    }

	    return ret;

	}
	
	function getServerConnection() {
		var connection = {};
		var pathArray = parse_url(window.location.href)["path"].split("/");
    	for (var i = 0; i < pathArray.length; i++)
    	{
    		if(i == 1) {
    			connection.aaServerUrl = "/" + pathArray[i] + "/";
    		} else {
    			if(pathArray[i].indexOf("t=") == 0) {
    				connection.tenant = pathArray[i].substring(2);
    			} else if(pathArray[i].indexOf("a=") == 0) {
    				connection.application = pathArray[i].substring(2);
    			} 
    		}
    	}
    	return connection;
	}
    
    
