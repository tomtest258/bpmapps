/**
 * BPM Angular User Interface
 * Copyright Fujitsu 2016
 * 
 * This file contains the UI directives for Interstage BPM.
 *      General Directives (Priority, check box selector, Applications)
 *      Process Instance List Directives
 *      Process Instance Directives
 *      Work Item List Directives
 *      Work Item Directives
 *      Process Definition List Directives
 *      Process Definition Directives
 *      UDA Directives
 *      Comment Directives
 *      User and Group Directives
 *      Time Directives
 */

(function () {
    "use strict";

    var bpmAngularUiModule= angular.module('bpm-angular-ui', []);

    ///////////////////////////////////
    // Dependencies
    ///////////////////////////////////
    try {
        angular.module('bpm-angular'); // this will throw if the js file has not been loaded
        bpmAngularUiModule.requires.push('bpm-angular');
    } catch (error) {
        console.log('ERROR: bpm-angular is not loaded.');
    }
    try {
        angular.module('pascalprecht.translate'); // this will throw if the js file has not been loaded
        bpmAngularUiModule.requires.push('pascalprecht.translate');
    } catch (error) {
        console.log('ERROR: translation module (pascalprecht.translate) is not loaded');
    }
    try {
        angular.module('ui.bootstrap'); // this will throw if the js file has not been loaded
        bpmAngularUiModule.requires.push('ui.bootstrap');
    } catch (error) {
        console.log('ERROR: Angular Bootstrap (ui.bootstrap) is not loaded');
    }



    ///////////////////////////////////
    // General Directives
    ///////////////////////////////////
    bpmAngularUiModule
        .factory('TableColumnFactory', TableColumnFactory)
        .controller('bpmItemListCtrl', bpmItemListCtrl)
        .directive('bpmItemTable', bpmItemTable)
        .directive('bpmInfoWell', bpmInfoWell)
        .directive('bpmPriorityText', bpmPriorityText)
        .directive('bpmPrioritySelect', bpmPrioritySelect)
        .controller('bpmSelectAllCheckboxCtrl', bpmSelectAllCheckboxCtrl)
        .directive('bpmSelectAllCheckbox', bpmSelectAllCheckbox)
        .directive('bpmSelectCheckbox', bpmSelectCheckbox)
        .directive('bpmApplicationState', bpmApplicationState)
    ;

    // TableColumnFactory takes an object that defines the columns for a BPM table.
    // You should pass an array of columns
    // For example the default Work Item list looks like:
    // [["id","state"],"name","creationTime","priority"]
    // This provides 4 columns, the first column has 2 values: id and state.
    // You can provide more information for each value like this:
    //  {"name":"udaName", "uda":"true", "display":"STRING","class":"h4 btn-link"}
    // All fields except name are optional

    function TableColumnFactory() {
        var factory = {
            processColumns: processColumns
        };

        var fields={};
        fields.pi= ["closedTime","creationTime","description","idDef","idInst","initiator","name","owners","parentProcess","priority",
            "sequence","state","title","type"];
        fields.wi=["application","assignee","choices","creationTime","forms","id","instanceDescription",
            "instanceId","instanceName","instanceOwners", "instanceTitle", "name","nodeInstanceId","planDescription",
            "planIdentifier","planName", "planTitle","priority","state","subProcessId" ];
        fields.pd=["application","desc","id","name","owner","priority","state","version"];

        ////////////
        // IMPLEMENTATION
        return factory;

        function isInFields(itemType,fieldName) {
            return (fields[itemType].indexOf(fieldName)>=0);
        }

        // parseColumnInfo takes a column and returns it as a well formatted array of fields that are displayed in that column
        function parseColumnInfo(column,itemType){
            // check if column is an array, if so, we use the first property as the heading text
            // if it is not an array we only need to parse one object.
            var columnArray= column;
            if (!Array.isArray(column)){
                columnArray= [];
                columnArray.push(column);
            }

            var formattedColumn= [];
            columnArray.forEach(function(columnValue) {
                // This should be either a string or an object.
                var formattedValue= columnValue;
                if (typeof columnValue === 'string'){
                    formattedValue= {};
                    formattedValue.name= columnValue;
                }

                // Check to see if it is a UDA or a field
                if (typeof formattedValue.uda == "undefined"){
                    formattedValue.uda= !isInFields(itemType,formattedValue.name);
                }

                if (typeof formattedValue.display == "undefined"){
                    formattedValue.display= "DEFAULT";
                }

                if (typeof formattedValue.translateTable == "undefined"){
                    formattedValue.translateTable= null;
                }
                // we don't need to do anything for formattedValue.class

                formattedColumn.push(formattedValue);
            });
            return formattedColumn;
        }

        // This parses through all the columns processing them
        function processColumns(itemType, columns) {
            var columnArray= [];
            columns.forEach(function(column) {
                var columnInfo = parseColumnInfo(column,itemType);
                columnArray.push(columnInfo);
            });

            return columnArray;
        }
    }


    ///////////////////////////////////
    // bpmItemListCtrl
    ///////////////////////////////////
    bpmItemListCtrl.$inject = ['$scope','BPM','TimeFactory','$timeout','TableColumnFactory'];
    function bpmItemListCtrl($scope,BPM,TimeFactory,$timeout,TableColumnFactory) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // functions
        vm.itemClicked= itemClicked;
        vm.checkClicked= checkClicked;
        vm.sortColumn= sortColumn;
        vm.isColumnSorted= isColumnSorted;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        // This receives the item that was clicked and which field was clicked
        // It ignores all clicks except for fields that are clickable.
        function itemClicked(item, field){
            if (field && field.clickable){
                vm.clickedItem= item;
                vm.clickedField=field;
                if (vm.bpmOnClicked){
                    $timeout(function(){
                        vm.bpmOnClicked();
                    });
                }
            }
        }

        function checkClicked(item) {
            item.isChecked = !item.isChecked;
            vm.selectAllCheckboxApi.updateSelected();
        }
        
        function sortColumn(column) {
        	vm.sortBy=(column.uda==true ? "uda." + column.name : "" + column.name);
        	vm.sortOrder= !vm.sortOrder;
        }
        
        function isColumnSorted(column) {
        	var fieldName = (column.uda==true ? "uda." + column.name : "" + column.name);
        	return (vm.sortBy == fieldName);
        }

        initialize();
        function initialize() {
            vm.sortBy= "name";
            vm.clickedItem= null;

            // Set the header table to use based on the type of item
            // itemType should be: pi, wi, or pd
            var headerTableSet={
                "pi": "piTable",
                "wi": "wiTable",
                "pd": "pdTable"
            };
            // Set vm.headerTable to the correct of
            vm.headerTable= headerTableSet[vm.itemType];

            // Watch the columns so we can set a default set of columns to be displayed.
            var defaultColumns={
                "wi":[[{"name":"id","clickable":true},"state"],[{"name":"name","clickable":true}],"creationTime","priority"],
                "pi":[["idInst","state"],{"name":"name","clickable":true},"creationTime","priority"],
                "pd":["id",{"name":"name","clickable":true},"version","state","owner"]
            };
            $scope.$watch('vm.columns',
                function() {
                    if (!vm.columns || vm.columns==null || vm.columns==''){
                        vm.columns=defaultColumns[vm.itemType];
                    }
                    vm.columnsArray= TableColumnFactory.processColumns(vm.itemType,vm.columns);
                }
            );

            // When the list of items changes the currently selected item is set to null
            $scope.$watch('vm.items',
                function() {
                    vm.clickedItem= null;
                }
            );
        }
    }

    ///////////////////////////////////
    // bpmItemTable
    // bpm-item-table
    // DIRECTIVE
    ///////////////////////////////////
    // Directive to display a List of BPM objects
    function bpmItemTable() {
        var directive = {
            restrict: 'E',
            template: '<table class="table table-striped table-condensed">' +
            '<tr>' +
            '<th ng-hide="vm.hideCheckbox"><bpm-select-all-checkbox item-array="vm.items" selected-items="vm.selectedItems" api="vm.selectAllCheckboxApi"></bpm-select-all-checkbox> </th>' +
            '<th style="cursor:default" ng-repeat="header in vm.columnsArray track by $index" ng-click="vm.sortColumn(header[0])"> ' +
            '<a><span>{{vm.headerTable+"."+header[0].name | translate}}</span></a>' +
            '<span ng-show="vm.isColumnSorted(header[0])" ng-class="{glyphicon:true,' + "'glyphicon-sort-by-attributes'" + ':!vm.sortOrder,' + "'glyphicon-sort-by-attributes-alt'" + ':vm.sortOrder}" aria-hidden="true"></span></th>' +
            '</tr>' +
            '<tr ng-repeat="item in vm.items | orderBy:vm.sortBy:vm.sortOrder | filter:vm.filterText">' +
            '<td ng-hide="vm.hideCheckbox">' +
            '<bpm-select-checkbox item="item" select-all-api="vm.selectAllCheckboxApi"></bpm-select-checkbox> ' +
            '</td>' +
            '<td ng-repeat="column in vm.columnsArray track by $index">' +
            '<span ng-repeat="value in column track by $index">' +
            '<div ng-switch on="vm.itemType">' +
            '<div ng-switch-when="wi">' +
            '<bpm-work-item-list-table-value wi="item" field="value" bpm-on-clicked="vm.itemClicked(item,value)"></bpm-work-item-list-table-value></div>' +
            '<div ng-switch-when="pi">' +
            '<bpm-process-instance-list-table-value pi="item" field="value" bpm-on-clicked="vm.itemClicked(item,value)"></bpm-process-instance-list-table-value></div>' +
            '<div ng-switch-when="pd">' +
            '<bpm-process-definition-list-table-value pd="item" field="value" bpm-on-clicked="vm.itemClicked(item,value)"></bpm-process-definition-list-table-value></div>' +
            '</div></span>' +
            '</td>' +
            '</tr></table>',
            controller: 'bpmItemListCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "items": "=",
                "itemType": "@", // should be wi, pi, or pd
                "columns" : "<?",
                "sortBy" : "=?",
                "sortOrder" : "=?",
                "filterText" : "@?",
                "hideCheckbox" : "<?",
                "clickedItem": "=?",
                "clickedField": "=?",
                "selectedItems": "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmInfoWell
    // bpm-info-well
    // Show the name and description with an info icon.
    ///////////////////////////////////
    function bpmInfoWell() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-class="bpmClass"><span style="cursor: default" ng-click="bpmOnClicked && bpmOnClicked()">{{name}}</span>' +
            '<small style="color: blue; cursor: pointer" ng-show="description" ng-click="descIsOpen = !descIsOpen" class="glyphicon glyphicon-info-sign" aria-hidden="true"></small></span>' +
            '<span uib-collapse="!descIsOpen" ng-init="descIsOpen=false">' +
            '<span class="well"><span>{{description}}</span></span></span>',
            scope: {
                name: '@',
                description : "@",
                bpmClass : "@?",
                bpmOnClicked: '&?'
            }
        };
        return directive;
    }



    function bpmPriorityText() {
        var directive = {
            restrict: 'E',
            template: '<span translate-namespace="priority" translate>{{' + "'.'" + '+asText(priority)}}</span>',
            scope: {
                "priority": "@"
            },
            controller: function($scope) {
                $scope.asText = function(priority) {
                    if (priority < 0)  return "undefined";
                    if (priority < 6) return "low";
                    if (priority < 12) return "medium";

                    return "high";
                }
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmPrioritySelect
    // DIRECTIVE
    // bpm-priority-select
    //      Select box with priorities
    ///////////////////////////////////
    function bpmPrioritySelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedPriority" name="prioritySelect" id="prioritySelectId" class="form-control">' +
            '<option value="0">{{"priority.low" | translate}}</option>' +
            '<option value="8">{{"priority.medium" | translate}}</option>' +
            '<option value="16">{{"priority.high" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedPriority": "="
            }
        };
    }


    /////////////////////////////////////////////////////
    // bpmSelectAllCheckboxCtrl
    // CONTROLLER for the bpmSelectAllCheckbox directive
    /////////////////////////////////////////////////////
    bpmSelectAllCheckboxCtrl.$inject = ['$scope'];
    function bpmSelectAllCheckboxCtrl($scope){
        var vm = this;

        function setAllSelected(){
            if (vm.itemArray && vm.selectedItems){
                vm.allSelected= ((vm.itemArray.length>0)&&(vm.itemArray.length == vm.selectedItems.length));
            }
            else{
                vm.selectedItems = [];
                vm.allSelected= false;
            }
        }

        function itemIsChecked(item){
            return item.isChecked;
        }

        // update the selected item list based on the display. set the ALL check box accordingly
        function updateSelected() {
            if (vm.itemArray) {
                vm.selectedItems= vm.itemArray.filter(itemIsChecked);
            }
            setAllSelected();
        }

        vm.toggle= function() {
            vm.allSelected = !vm.allSelected;
            vm.itemArray.forEach(
                function(pi){
                    pi.isChecked = vm.allSelected;
                }
            );
            vm.selectedItems= vm.allSelected ? vm.itemArray : [];
        };

        initialize();
        function initialize() {
            // Expose the methods for the directive api
            vm.api = {
                "updateSelected" : updateSelected
            };

            // Watch for the items to be populated
            $scope.$watch('itemArray',updateSelected);
        }
    }

    ///////////////////////////////////
    // bpmSelectAllCheckbox
    // DIRECTIVE
    // bpm-select-all-checkbox
    //      Select all check box for lists
    ///////////////////////////////////
    function bpmSelectAllCheckbox() {
        var directive = {
            restrict: 'E',
            template: '<span style="cursor: default" ng-click="vm.toggle()" ng-class="{glyphicon:true,' + "'glyphicon-check'" + ':vm.allSelected,' + "'glyphicon-unchecked'" + ':!vm.allSelected}"></span>',
            scope: {
                "itemArray": "=",
                "selectedItems": "=",
                "api": "="
            },
            controllerAs: 'vm',
            bindToController: true,
            controller: "bpmSelectAllCheckboxCtrl"
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmSelectCheckbox
    // DIRECTIVE
    // bpm-select-checkbox
    //      Select check box for lists, this interacts with the bpm-select-all-checkbox
    ///////////////////////////////////
    function bpmSelectCheckbox() {
        var directive = {
            restrict: 'E',
            template: '<span style="cursor: default" ng-click="checkClicked()" ng-class="{glyphicon: true, ' + "'glyphicon-check'" + ': item.isChecked, ' + "'glyphicon-unchecked'" + ': !item.isChecked}"></span>',
            scope: {
                "item": "=",
                "selectAllApi": "=?"
            },
            controller : function($scope) {
                $scope.checkClicked= function(){
                    $scope.item.isChecked= !$scope.item.isChecked;
                    if ($scope.selectAllApi){
                        $scope.selectAllApi.updateSelected();
                    }
                }
            }

        };
        return directive;
    }

    ///////////////////////////////////
    // bpmApplicationState
    // bpm-application-state
    ///////////////////////////////////
    // Directive to display Applcation state with image
    function bpmApplicationState() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-style="{color: myStyle[state]}" class="{{className[state]}}" aria-hidden="true"></span> <span ng-show="state">{{"applicationState."+state | translate}}</span>',
            scope: {
                "state":"@"
            },
            controller : function($scope) {
                $scope.className = {
                    1 : 'glyphicon glyphicon-stop',
                    2  : 'glyphicon glyphicon-play'
                };

                $scope.myStyle = {
                    1 : "red",
                    2  : "green"
                };
            }
        };
        return directive;
    }



    ///////////////////////////////////
    // Process Instance List Directives
    ///////////////////////////////////

    angular
        .module('bpm-angular-ui')
        .controller('bpmProcessInstanceFeedCtrl', bpmProcessInstanceFeedCtrl)
        .directive('bpmProcessInstanceListTableValue', bpmProcessInstanceListTableValue)
        .directive('bpmProcessInstanceListTable', bpmProcessInstanceListTable)
//        .directive('bpmProcessInstanceListFeed', bpmProcessInstanceListFeed)
//        .controller('bpmProcessInstanceListItemCtrl', bpmProcessInstanceListItemCtrl)
//        .directive('bpmProcessInstanceListItem', bpmProcessInstanceListItem)
        .directive('bpmProcessInstanceListFilterSelect', bpmProcessInstanceListFilterSelect)
        .directive('bpmProcessInstanceListSortSelect', bpmProcessInstanceListSortSelect)
        .directive('bpmProcessInstanceListSortSelectReversible', bpmProcessInstanceListSortSelectReversible)
    ;


    ///////////////////////////////////
    // bpmProcessInstanceFeedCtrl
    ///////////////////////////////////
    bpmProcessInstanceFeedCtrl.$inject = ['$scope','BPM','TimeFactory','$timeout','TableColumnFactory'];
    function bpmProcessInstanceFeedCtrl($scope,BPM,TimeFactory,$timeout,TableColumnFactory) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // functions
        vm.piClicked= piClicked;
        vm.checkClicked = checkClicked;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function piClicked(pi){
            vm.clickedPi= pi;
            if (vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
        }

        // User clicked on the checkbox for a pi so update it and the list of selected pi's
        function checkClicked(pi) {
            pi.isChecked = !pi.isChecked;
            vm.selectAllCheckboxApi.updateSelected();
        }



        initialize();
        function initialize() {
            vm.clickedPi= null;

            // Watch the columns so we can set a default
            $scope.$watch('vm.columns',
                function() {
                    if (!vm.columns || vm.columns==null || vm.columns==''){
                        vm.columns=[["idInst","state"],"name","creationTime","priority"];
                    }
                    vm.columnsArray= TableColumnFactory.processPiColumns(vm.columns);
                }
            );

            // When the list of pis changes the currently selected instance is empty
            $scope.$watch('vm.piArray',
                function() {
                    vm.clickedPi= null;
                }
            );
        }
    }

    ///////////////////////////////////
    // bpmProcessInstanceListTableValue
    // DIRECTIVE
    // bpm-process-instance-list-table-value
    ///////////////////////////////////
    function bpmProcessInstanceListTableValue() {
        return {
            restrict: 'EA',
            template: '<div ng-if="!field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}">' +
            '<span ng-if="field.name==' + "'closedTime'" + '" >{{pi.closedTime | date:"y/MM/dd" }}</span>' +
            '<span ng-if="field.name==' + "'creationTime'" + '" >{{pi.creationTime | date:"y/MM/dd" }}</span>' +
            '<bpm-due-date-progress-bar ng-if="field.name==' + "'dueDate'" + '" ng-show="pi.dueDate && pi.state === 7" created-time-stamp="{{pi.creationTime}}" due-date-time-stamp="{{pi.dueDate}}"></bpm-due-date-progress-bar>' +
            '<span ng-if="field.name==' + "'idDef'" + '">{{pi.idDef}}</span>' +
            '<span ng-if="field.name==' + "'idInst'" + '">{{pi.idInst}}</span>' +
            '<span ng-if="field.name==' + "'initiator'" + '">{{pi.initiator}}</span>' +
            '<span ng-if="field.name==' + "'owners'" + '">{{pi.owners}}</span>' +
            '<span ng-if="field.name==' + "'parentProcess'" + '">{{pi.parentProcess}}</span>' +
            '<bpm-priority-text ng-if="field.name==' + "'priority'" + '" priority="{{pi.priority}}"></bpm-priority-text>' +
            '<span ng-if="field.name==' + "'sequence'" + '">{{pi.sequence}}</span>' +
            '<bpm-process-instance-state ng-if="field.name==' + "'state'" + '" state="{{pi.state}}"></bpm-process-instance-state>' +
            '<span ng-if="field.name==' + "'title'" + '">{{pi.title}}</span>' +
            '<span ng-if="field.name==' + "'type'" + '">{{pi.type}}</span>' +
            '</span>' +
            '<bpm-process-instance-name-desc bpm-on-clicked="bpmOnClicked && bpmOnClicked()" bpm-class="{{field.clickable?' +
            "'h4 btn-link'" + ':' + "'h4'" +
            '}}" ng-if="field.name==' + "'name'" + '" pi="pi"></bpm-process-instance-name-desc>' +
            '</div>' +
            '<div ng-if="field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}"><span ng-hide="field.translateTable">{{pi.uda[field.name]}}</span><span ng-show="field.translateTable">{{field.translateTable+"."+pi.uda[field.name] | translate}}</span></span>' +
            '</div>',
            scope: {
                field : "=",
                pi : "=",
                bpmOnClicked : "&?"
            }
        };
    }


    ///////////////////////////////////
    // bpmProcessInstanceListTable
    // bpm-process-instance-list-table
    // DIRECTIVE
    ///////////////////////////////////
    // Directive to display WorkItem List
    function bpmProcessInstanceListTable() {
        var directive = {
            restrict: 'E',
            template: '<bpm-item-table items="piArray" item-type="pi" columns="columns" sort-by="sortBy" sort-order="sortOrder" filter-text="{{filterText}}" hide-checkbox="hideCheckbox" clicked-item="clickedPi" clicked-field="clickedField" selected-items="selectedPiArray" bpm-on-clicked="bpmOnClicked && bpmOnClicked()"></bpm-item-table> ',
            scope: {
                "piArray": "=",
                "columns" : "<?",
                "sortBy" : "=?",
                "sortOrder" : "=?",
                "filterText" : "@?",
                "hideCheckbox" : "<?",
                "clickedPi": "=?",
                "clickedField": "=?",
                "selectedPiArray": "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessInstanceListFilterSelect
    // DIRECTIVE
    // bpm-process-instance-list-filter-select
    //      Select box with Process Instance list filters
    // events:
    ///////////////////////////////////
    function bpmProcessInstanceListFilterSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedFilter" name="piFilterSelect" id="piFilterSelectId" class="form-control">' +
            '<option value="AllActiveProcesses">{{"processInstanceFilter.AllActiveProcesses" | translate}}</option>' +
            '<option value="MyProcesses">{{"processInstanceFilter.MyProcesses" | translate}}</option>' +
            '<option value="AllProcesses">{{"processInstanceFilter.AllProcesses" | translate}}</option>' +
            '<option value="MyActiveProcesses">{{"processInstanceFilter.MyActiveProcesses" | translate}}</option>' +
            '<option value="AllInactiveProcesses">{{"processInstanceFilter.AllInactiveProcesses" | translate}}</option>' +
            '<option value="MyInactiveProcesses">{{"processInstanceFilter.MyInactiveProcesses" | translate}}</option>' +
            '<option value="AllProcessesInErrorState">{{"processInstanceFilter.AllProcessesInErrorState" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedFilter": "="
            }
        };
    }


    ///////////////////////////////////
    // bpmProcessInstanceListSortSelect
    // DIRECTIVE
    // bpm-process-instance-list-sort-select
    //      Select box with standard Process Instance sort criteria
    // events:
    ///////////////////////////////////
    function bpmProcessInstanceListSortSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedSort" name="piSortSelect" id="piSortSelectId" class="form-control">' +
            '<option value="creationTime">{{"time.creationDate" | translate}}</option>' +
            '<option value="closedTime">{{"time.closedTime" | translate}}</option>' +
            '<option value="dueDate">{{"time.dueDate" | translate}}</option>' +
            '<option value="priority">{{"priority.priority" | translate}}</option>' +
            '<option value="name">{{"name" | translate}}</option>' +
            '<option value="idInst">{{"bpm.id" | translate}}</option>' +
            '<option value="idDef">{{"bpm.plan" | translate}} {{"bpm.id" | translate}}</option>' +
            '<option value="state">{{"processInstanceState.state" | translate}}</option>' +
            '<option value="initiator">{{"bpm.initiator" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedSort": "="
            }
        };
    }

    ///////////////////////////////////
    // bpmProcessInstanceListSortSelectReversible
    // DIRECTIVE
    // bpm-process-instance-list-sort-select-reversible
    //      Select box with standard Process Instance sort criteria and a reverse button
    // events:
    ///////////////////////////////////
    function bpmProcessInstanceListSortSelectReversible() {
        return {
            restrict: 'EA',
            template:   '<div class="input-group" ng-init="sortOrder= true">' +
            '<bpm-process-instance-list-sort-select selected-sort="selectedSort"></bpm-process-instance-list-sort-select>' +
            '<span class="input-group-addon" style="cursor: pointer">' +
            '<span ng-show="sortOrder" class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '<span ng-hide="sortOrder" class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '</span></div>',
            scope: {
                "selectedSort": "=",
                "sortOrder": "="
            }
        };
    }




    ///////////////////////////////////
    // Process Instance Directives
    ///////////////////////////////////
    angular
        .module('bpm-angular-ui')
        .directive('bpmProcessInstanceUdaList', bpmProcessInstanceUdaList)
        .directive('bpmProcessInstanceHeader', bpmProcessInstanceHeader)
        .directive('bpmProcessInstanceNameDesc', bpmProcessInstanceNameDesc)
        .directive('bpmProcessInstanceCreatedBy', bpmProcessInstanceCreatedBy)
        .directive('bpmProcessInstanceState', bpmProcessInstanceState)
        .directive('bpmNodeInstanceState', bpmNodeInstanceState)
        .controller('bpmProcessInstanceActionsDropdownCtrl', bpmProcessInstanceActionsDropdownCtrl)
        .directive('bpmProcessInstanceActionsDropdown', bpmProcessInstanceActionsDropdown)
        .directive('bpmProcessHistoryEventCreate', bpmProcessHistoryEventCreate)
        .directive('bpmProcessHistoryEventMakeChoice', bpmProcessHistoryEventMakeChoice)
        .directive('bpmProcessHistoryEventUdaModified', bpmProcessHistoryEventUdaModified)
        .directive('bpmProcessHistory', bpmProcessHistory)
        .controller('bpmCurrentActivityListCtrl', bpmCurrentActivityListCtrl)
        .directive('bpmCurrentActivityList', bpmCurrentActivityList)
        .directive('bpmProcessInstanceLauncher', bpmProcessInstanceLauncher)
    ;

    ///////////////////////////////////
    // bpmProcessInstanceUdaList
    // bpm-process-instance-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Process Instance
    function bpmProcessInstanceUdaList() {
        var directive = {
            restrict: 'E',
            template: '<bpm-generic-uda-list udas="vm.processInstance.pd.dataItemRefs" uda-values="vm.processInstance.pi.uda" extended-att-array="vm.processInstance.pd.ExtendedAttributes.ExtendedAttribute">' +
            '</bpm-generic-uda-list>',
            scope: {
                "processInstance": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceHeader
    // bpm-process-instance-header
    ///////////////////////////////////
    // Directive to display a Process Instance
    // This doesn't need a controller as it is display only.
    function bpmProcessInstanceHeader() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-process-instance-name-desc pi="pi"></bpm-process-instance-name-desc>'+
            '<p><bpm-process-instance-created-by pi="pi"></bpm-process-instance-created-by></p>' +
            '<p><bpm-process-instance-state state="{{pi.state}}"></bpm-process-instance-state></p>',
            scope: {
                "pi": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceNameDesc
    // bpm-process-instance-name-desc
    // Show the process instance name and description for the pi.
    ///////////////////////////////////
    function bpmProcessInstanceNameDesc() {
        var directive = {
            restrict: 'E',
            template: '<bpm-info-well bpm-on-clicked="bpmOnClicked && bpmOnClicked()" ng-init="bpmClass=' +"'h1'" +
            '" name="{{pi.name}}" description="{{pi.description}}" bpm-class="{{bpmClass}}"></bpm-info-well>',
            scope: {
                pi : '=',
                bpmClass : "@?",
                bpmOnClicked : "&?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceCreatedBy
    // bpm-process-instance-created-by
    ///////////////////////////////////
    // Directive to display information about who created the Process Instance and when.
    // The output looks something like this:
    // Oct 6, 2016 1:17:36 PM by: admin
    function bpmProcessInstanceCreatedBy() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-show="pi">{{pi.creationTime | date:"medium"}} <span>{{"time.by"| translate}}</span>: <a>{{pi.initiator}}</a></span>',
            scope: {
                "pi": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceState
    // bpm-process-instance-state
    ///////////////////////////////////
    // Directive to display Process Instance state with image
    function bpmProcessInstanceState() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-style="{color: style[state]}" class="{{className[state]}}" aria-hidden="true"></span> <span ng-show="state">{{"processInstanceState."+state | translate}}</span>',
            scope: {
                "state":"@"
            },
            controller : function($scope) {
                // This section tells what icon to show for the state
                $scope.className = {
                    1  : "glyphicon glyphicon-play",
                    2  : "glyphicon glyphicon-ok",
                    7  : "glyphicon glyphicon-play",
                    8  : "glyphicon glyphicon-ok",
                    10 : "glyphicon glyphicon-minus-sign",
                    11 : "glyphicon glyphicon-stop",
                    12 : "glyphicon glyphicon-pause"
                };

                // This section tells what color the icon should be for the state
                $scope.style = {
                    1  : "green",
                    2  : "blue",
                    7  : "green",
                    8  : "blue",
                    10 : "red",
                    11 : "red",
                    12 : "orange"
                };
            }



        };
        return directive;
    }

    ///////////////////////////////////
    // bpmNodeInstanceState
    // bpm-node-instance-state
    ///////////////////////////////////
    // Directive to display state of Node Instance in a Process Instance with image
    function bpmNodeInstanceState() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-style="{color: style[state]}" class="{{className[state]}}" aria-hidden="true"></span> <span ng-show="state">{{"nodeInstanceState."+state | translate}}</span>',
            scope: {
                "state":"@"
            },
            controller : function($scope) {
                // This section tells what icon to show for the state
                $scope.className = {
                    0  : "glyphicon glyphicon-stop",
                    3  : "glyphicon glyphicon-play",
                    4  : "glyphicon glyphicon-pause",
                    5  : "glyphicon glyphicon-ok",
                    6 : "glyphicon glyphicon-minus-sign",
                    10 : "glyphicon glyphicon-stop",
                    11 : "glyphicon glyphicon-pause"
                };

                // This section tells what color the icon should be for the state
                $scope.style = {
                    0  : "gray",
                    3  : "green",
                    4  : "red",
                    5  : "blue",
                    6 : "red",
                    10 : "red",
                    11 : "orange"
                };
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceActionsDropdownCtrl
    ///////////////////////////////////
    bpmProcessInstanceActionsDropdownCtrl.$inject = ['$scope','BPM','$timeout'];
    function bpmProcessInstanceActionsDropdownCtrl($scope,BPM,$timeout) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER
        //The needs the full process instance object as after an action we get the full process instance back from the server.
        vm.updating=false;

        // functions
        vm.suspend=suspend;
        vm.resume=resume;
        vm.abort=abort;
        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function suspend() {
            vm.updating=true;
            BPM
                .suspend(vm.processInstance)
                .success(
                    function (data, status) {
                        vm.processInstance= data;
                        vm.updating=false;
                        if (vm.onBpmUpdate){
                            $timeout(function(){
                                vm.onBpmUpdate();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        function resume() {
            vm.updating=true;
            BPM
                .resume(vm.processInstance)
                .success(
                    function (data, status) {
                        vm.processInstance= data;
                        vm.updating=false;
                        if (vm.onBpmUpdate){
                            $timeout(function(){
                                vm.onBpmUpdate();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        function abort() {
            vm.updating=true;
            BPM
                .abort(vm.processInstance)
                .success(
                    function (data, status) {
                        vm.processInstance= data;
                        vm.updating=false;
                        if (vm.onBpmUpdate){
                            $timeout(function(){
                                vm.onBpmUpdate();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        initialize();
        function initialize() {
        }
    }

    ///////////////////////////////////
    // bpmProcessInstanceActionsDropdown
    // bpm-process-instance-actions-dropdown
    ///////////////////////////////////
    // Directive to display Process Instance Actions Drop down
    // Includes Suspend, Resume, Abort
    function bpmProcessInstanceActionsDropdown() {
        var directive = {
            restrict: 'EA',
            template: '<div class="btn-group" uib-dropdown is-open="status.isopen">' +
            '<button id="single-button" type="button" class="btn" uib-dropdown-toggle ng-disabled="disabled">Process Actions<span class="caret"></span></button>' +
            '<ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">' +
            '<li ng-click="vm.suspend()" role="menuitem"><a>Suspend  <span style="color: orange" class="glyphicon glyphicon-pause" aria-hidden="true"></span></a></li>' +
            '<li ng-click="vm.resume()" role="menuitem"><a>Resume  <span style="color: green" class="glyphicon glyphicon-play" aria-hidden="true"></span></a></li>' +
            '<li ng-click="vm.abort()" role="menuitem"><a>Abort  <span style="color: red" class="glyphicon glyphicon-stop" aria-hidden="true"></span></a></li></ul></div>',
            controller: 'bpmProcessInstanceActionsDropdownCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "processInstance": "=",
                "onBpmUpdate" : "&?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessHistoryEventCreate
    // bpm-process-history-event-create
    ///////////////////////////////////
    function bpmProcessHistoryEventCreate() {
        var directive = {
            restrict: 'EA',
            template: '<h4>Created</h4>{{historyEvent.timestamp | date:"short"}}<br>' +
            'By: {{historyEvent.responsible}}',
            scope: {
                "historyEvent" : '='
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessHistoryEventMakeChoice
    // bpm-process-history-event-make-choice
    ///////////////////////////////////
    // This make choice even needs the process instance so it can find the name of the Node Instance.
    // pi should be the pi portion of the process instance object
    function bpmProcessHistoryEventMakeChoice() {
        var directive = {
            restrict: 'EA',
            template: '<h4>Task: <span ng-repeat="nodeInstance in pi.nodes" ng-show="historyEvent.consumerId==nodeInstance.idInst">{{nodeInstance.name}}</span></h4>' +
            'Completed: {{historyEvent.timestamp | date:"short"}}<br>' +
            'By: {{historyEvent.responsible}}<br>' +
            'Choice: {{historyEvent.eventType.slice(0,historyEvent.eventType.indexOf(":"))}}',
            scope: {
                "historyEvent" : '=',
                "pi" : '='
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessHistoryEventUdaModified
    // bpm-process-history-event-uda-modified
    ///////////////////////////////////
    // Directive to display a history of activity for the process
    // history should be an array of history events retrieved using bpm-process-instance-history-fetcher.
    // process-instance should be the pi portion of the process instance object.
    function bpmProcessHistoryEventUdaModified() {
        var directive = {
            restrict: 'EA',
            template: '<h4>Attributes Modified</h4>{{historyEvent.timestamp | date:"short"}}<br><ul>' +
            '<li ng-repeat="uda in historyEvent.trackedUDAs" ng-show="uda.oldValue!=uda.newValue">' +
            '{{uda.name}}<br>From: {{uda.oldValue}}<br>' +
            '<span ng-hide="uda.type==' +"'DATE'"+'">To: {{uda.newValue}}</span>' +
            '<span ng-show="uda.type==' +"'DATE'" +'">To: {{uda.newValue | date:"short"}}</span></li></ul>',
            scope: {
                "historyEvent" : '='
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessHistory
    // bpm-process-history
    ///////////////////////////////////
    // Directive to display a history of activity for the process
    // history should be an array of history events retrieved using bpm-process-instance-history-fetcher.
    // process-instance should be the pi portion of the process instance object.
    function bpmProcessHistory() {
        var directive = {
            restrict: 'EA',
            template: '<ul class="list-group"><div ng-repeat="historyEvent in history | orderBy:'+"'-timestamp'"+'">' +
            '<li ng-show="historyEvent.eventType=='+"'__ProcessCreated'"+'" class="list-group-item">' +
            '<bpm-process-history-event-create history-event="historyEvent"></bpm-process-history-event-create></li>' +
            '<li ng-show="historyEvent.eventCode==2" class="list-group-item">' +
            '<bpm-process-history-event-make-choice pi="pi" history-event="historyEvent"></bpm-process-history-event-make-choice></li>' +
            '<li ng-show="historyEvent.eventType==' + "'__UDATracking'" +'" class="list-group-item">' +
            '<bpm-process-history-event-uda-modified history-event="historyEvent"></bpm-process-history-event-uda-modified>' +
            '</li></div></ul>',
            scope: {
                "history" : '=',
                "pi" : '='
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmCurrentActivityListCtrl
    // Controller for the bpmCurrentActivityList directive
    ///////////////////////////////////
    bpmCurrentActivityListCtrl.$inject = ['$scope','BPMService','$timeout'];
    function bpmCurrentActivityListCtrl($scope,BPMService,$timeout) {
        var vm = this;

        vm.workItemClicked = workItemClicked;

        // user clicked a workitem in the list
        function workItemClicked(wi) {
            vm.selectedWorkItem= wi;
            if (vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
        }

        initialize();
        function initialize() {
            // Watch for the processInstance to be populated
            $scope.$watch('vm.processInstance',
                function() {
                    vm.nodeInstances= null;
                    if ((vm.processInstance)&&(vm.processInstance.wiList)){
                        vm.nodeInstances= BPMService.aggregateWorkItemList(vm.processInstance.wiList);
                    }
                }
            );
        }
    }


    ///////////////////////////////////
    // bpmCurrentActivityList
    // bpm-current-activity-list
    ///////////////////////////////////
    // Directive to display a list of node instances so we can see who is responsible for the process
    // When a user clicks on a work item, selectedWorkItem is populated and bpmClicked is invoked.
    function bpmCurrentActivityList() {
        var directive = {
            restrict: 'EA',
            template: '<ul class="list-group">' +
            '<li class="list-group-item" ng-repeat="nodeInstance in vm.nodeInstances | orderBy:' +"'creationTime'" +'">' +
            '<h4>{{nodeInstance.name}}<small ng-show="nodeInstance.instanceDescription">' +
            '<span  style="color: blue;" popover-placement="bottom" uib-popover="{{nodeInstance.instanceDescription}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>' +
            '</small></h4>' +
            '<div class="row" ng-repeat="wi in nodeInstance.workItems"><a ng-click="vm.workItemClicked(wi)">' +
            '<small style="cursor: pointer;" class="col-sm-6">{{wi.assignee}}</small><small class="col-sm-6"><bpm-work-item-state state="{{wi.state}}"></bpm-work-item-state></small>' +
            '</a></div></li></ul>',
            controller: 'bpmCurrentActivityListCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "processInstance" : '=',
                "selectedWorkItem" : "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessInstanceLauncher
    // bpm-process-instance-launcher
    ///////////////////////////////////
    // Directive to display a list of process definitions that can be used to start a new process instance.
    bpmProcessInstanceLauncher.$inject = ['BPM'];
    function bpmProcessInstanceLauncher(BPM) {
        var directive = {
            restrict: 'EA',
            template: '<div class="row"><div class="col-sm-8">' +
            '<select class="form-control" ng-model="selectedPlanName" ng-change="selectProcess()">' +
            '<option ng-repeat="process in processes" value="{{process}}">{{process | translate}}</option></select></div>' +
            '<div class="col-sm-4"><select class="form-control" ng-model="selectedPlanId" ng-change="selectProcessVersion()">' +
            '<option ng-repeat="planVersion in planVersions" value="{{planVersion.id}}">{{planVersion.version}} ({{planVersion.stateLabel | translate}})</option>' +
            '</select></div></div>' +
            '<form class="form-horizontal" ng-show="selectedPlan"><bpm-uda-list bpm-object="selectedPlan"></bpm-uda-list>' +
            '<div class="form-group"><div class="col-xs-12" style="text-align:right">' +
            '<button class="btn btn-primary" ng-disabled="disableSubmit" ng-click="createProcessInstance()">{{"Submit"| translate}}</button>' +
            '</div></div></form>',
            scope: {
                processDefinitions: '=',
                onLaunched: '&'
            },
            link: function(scope, element, attributes) {
                initScope();

                function initScope() {
                    scope.selectedPlanName = null;
                    scope.selectedPlanId = null;
                    scope.udas = {};
                }

                // This runs when the user selects a name of the process definition.
                // We populate the versions.
                scope.selectProcess = function() {
                    scope.planVersions = scope.groupedPlans[scope.selectedPlanName];
                };

                scope.selectProcessVersion = function() {
                    if (!scope.selectedPlanId) {
                        return;
                    }
                    BPM
                        .getProcessDefinition(scope.selectedPlanId)
                        .success(
                            function (data, status) {
                                scope.selectedPlan = data;
                                // parse through the udas and populate the values with the defaults.
                                scope.selectedPlan.pi={};
                                scope.selectedPlan.pi.uda= scope.selectedPlan.pd.dataItemRefs.reduce(
                                    function(udas,uda){
                                        if (uda.type == "BOOLEAN"){
                                            udas[uda.identifier]= Boolean(uda.initValue);
                                        }
                                        else if((uda.type == "LONG")||(uda.type == "INTEGER")||(uda.type == "DATE")){
                                            udas[uda.identifier]= Number(uda.initValue);
                                        }
                                        else{
                                            udas[uda.identifier]= uda.initValue;
                                        }
                                        return udas;
                                    },{}
                                );
                                scope.$emit('bpmProcessDefinitionUpdatedEvent', scope.selectedPlan);
                            }
                        )
                        .error(
                            function (data, status) {
                                $scope.$emit('bpmErrorEvent', data);
                            }
                        )
                    ;
                };

                scope.createProcessInstance = function() {
                    scope.disableSubmit = true;
                    var planId = scope.selectedPlanId;
                    var udas = scope.selectedPlan.pi.uda;
                    initScope();
                    BPM.createInstance(planId, udas)
                        .success(
                            function (data, status) {
                                scope.disableSubmit = false;
                                scope.onLaunched();
                                scope.$emit('bpmProcessInstanceStartedEvent', scope.selectedPlan);
                            }
                        )
                        .error(
                            function (data, status) {
                                console.log(data);
                                scope.$emit('bpmErrorEvent', data);
                            }
                        )
                    ;
                };

                var stateLabels = {0: 'draft', 1: 'published'};

                scope.$watch('selectedPlan', function(value) {
                    if (scope.selectedPlan){
                        console.log('selectedPlan', value);
                    }
                });
                scope.$watch('processDefinitions', function() {
                    if (scope.processDefinitions){
                        // Create the label information for the plans that include name, state, and version.
                        var plans= scope.processDefinitions.map(
                            function(plan) {
                                return {
                                    "id" : plan.id,
                                    "name" : plan.name,
                                    "stateLabel" : stateLabels[plan.state],
                                    "version" : plan.version
                                }
                            }
                        );
                        // Group the plans by name
                        scope.groupedPlans = plans.reduce(
                            function(previous, plan){
                                if (!previous[plan.name]){
                                    previous[plan.name]=[];
                                }
                                previous[plan.name].push(plan);
                                return previous;
                            },{}
                        );
                        console.log("groupedPlans",scope.groupedPlans);
                        scope.processes = Object.keys(scope.groupedPlans);
                        // Set the default value of the select to be the first plan in the list.
                        scope.selectedPlanName= scope.processes[0];
                        scope.selectProcess();
                    }
                });
            }
        };
        return directive;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////



    ///////////////////////////////////
    // Work Item List Directives
    ///////////////////////////////////

    angular
        .module('bpm-angular-ui')
        .controller('bpmWorkItemFeedCtrl', bpmWorkItemFeedCtrl)
        .directive('bpmWorkItemListTableValue', bpmWorkItemListTableValue)
        .directive('bpmWorkItemListTable', bpmWorkItemListTable)
        .directive('bpmWorkItemListFeed', bpmWorkItemListFeed)
        .controller('bpmWorkItemListItemCtrl', bpmWorkItemListItemCtrl)
        .directive('bpmWorkItemListItem', bpmWorkItemListItem)
        .directive('bpmWorkItemListFilterSelect', bpmWorkItemListFilterSelect)
        .directive('bpmWorkItemListSortSelect', bpmWorkItemListSortSelect)
        .directive('bpmWorkItemListSortSelectReversible', bpmWorkItemListSortSelectReversible)
    ;

    ///////////////////////////////////
    // bpmWorkItemFeedCtrl
    ///////////////////////////////////
    bpmWorkItemFeedCtrl.$inject = ['$scope','BPM','TimeFactory','$timeout','TableColumnFactory'];
    function bpmWorkItemFeedCtrl($scope,BPM,TimeFactory,$timeout,TableColumnFactory) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // functions
        vm.wiClicked= wiClicked;
        vm.checkClicked= checkClicked;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        // This receives the wi that was clicked and which field was clicked
        // It ignores all clicks except for fields that are clickable.

        function wiClicked(wi, field){
            if (field && field.clickable){
                vm.clickedWi= wi;
                vm.clickedField=field;
                if (vm.bpmOnClicked){
                    $timeout(function(){
                        vm.bpmOnClicked();
                    });
                }
            }
        }

        function checkClicked(wi) {
            wi.isChecked = !wi.isChecked;
            vm.selectAllCheckboxApi.updateSelected();
        }

        initialize();
        function initialize() {
            vm.clickedWi= null;

            // Watch the columns so we can set a default
            $scope.$watch('vm.columns',
                function() {
                    if (!vm.columns || vm.columns==null || vm.columns==''){
                        vm.columns=[[{"name":"id","clickable":true},"state"],[{"name":"name","clickable":true}],"creationTime","priority"];
                    }
                    vm.columnsArray= TableColumnFactory.processColumns("wi",vm.columns);
                }
            );

            // When the list of workitems changes the currently selected item is set to null
            $scope.$watch('vm.wiArray',
                function() {
                    vm.clickedWi= null;
                }
            );
        }
    }


    ///////////////////////////////////
    // bpmWorkItemListTableValue
    // DIRECTIVE
    // bpm-work-item-list-table-value
    ///////////////////////////////////
    function bpmWorkItemListTableValue() {
        return {
            restrict: 'EA',
            template: '<div ng-if="!field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}">' +
            '<span ng-if="field.name==' + "'application'" + '">{{wi.application}}</span>' +
            '<span ng-if="field.name==' + "'assignee'" + '">{{wi.assignee}}</span>' +
            '<div ng-if="field.name==' + "'nameText'" + '"><span ng-hide="field.translateTable">{{wi.name}}</span><span ng-show="field.translateTable">{{field.translateTable+"."+wi.name | translate}}</span></div>' +
            '<span ng-if="field.name==' + "'choices'" + '">{{wi.choices}}</span>' +
            '<span ng-if="field.name==' + "'creationTime'" + '" >{{wi.creationTime | date:"y/MM/dd" }}</span>' +
            '<bpm-due-date-progress-bar ng-if="field.name==' + "'dueDate'" + '" ng-show="wi.dueDate && wi.state === 7" created-time-stamp="{{wi.creationTime}}" due-date-time-stamp="{{wi.dueDate}}"></bpm-due-date-progress-bar>' +
            '<span ng-if="field.name==' + "'forms'" + '">{{wi.forms}}</span>' +
            '<span ng-if="field.name==' + "'id'" + '">{{wi.id}}</span>' +
            '<span ng-if="field.name==' + "'instanceId'" + '">{{wi.instanceId}}</span>' +
            '<span ng-if="field.name==' + "'instanceOwners'" + '">{{wi.instanceOwners}}</span>' +
            '<span ng-if="field.name==' + "'instanceTitle'" + '">{{wi.instanceTitle}}</span>' +
            '<span ng-if="field.name==' + "'nodeInstanceId'" + '">{{wi.nodeInstanceId}}</span>' +
            '<bpm-info-well ng-if="field.name==' + "'planName'" + '" name="{{wi.planName}}" description="{{wi.planDescription}}"></bpm-info-well>' +
            '<bpm-priority-text ng-if="field.name==' + "'priority'" + '" priority="{{wi.priority}}"></bpm-priority-text>' +
            '<bpm-work-item-state ng-if="field.name==' + "'state'" + '" state="{{wi.state}}"></bpm-work-item-state>' +
            '<span ng-if="field.name==' + "'subProcessId'" + '">{{wi.subProcessId}}</span>' +
            '</span>' +
            '<bpm-work-item-name-desc bpm-on-clicked="bpmOnClicked && bpmOnClicked()" bpm-class="{{field.clickable?' +
            "'btn-link h4'" + ':' + "'h4'" +
            '}}" ng-if="field.name==' + "'name'" + '" wi="wi"></bpm-work-item-name-desc>' +
            '<bpm-work-item-instance-name-desc bpm-on-clicked="bpmOnClicked && bpmOnClicked()" bpm-class="{{field.clickable?' +
            "'h4 btn-link'" + ':' + "'h4'" +
            '}}" ng-if="field.name==' + "'instanceName'" + '" wi="wi"></bpm-work-item-instance-name-desc>' +
            '</div>' +
            '<div ng-if="field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}"><span ng-hide="field.translateTable">{{wi.uda[field.name]}}</span><span ng-show="field.translateTable">{{field.translateTable+"."+wi.uda[field.name] | translate}}</span></span>' +
            '</div>',
            scope: {
                field : "=",
                wi : "=",
                bpmOnClicked : "&?"
            }
        };
    }

    ///////////////////////////////////
    // bpmWorkItemListTable
    // bpm-work-item-list-table
    // DIRECTIVE
    ///////////////////////////////////
    // Directive to display WorkItem List
    function bpmWorkItemListTable() {
        var directive = {
            restrict: 'E',
            template: '<bpm-item-table items="wiArray" item-type="wi" columns="columns" sort-by="sortBy" sort-order="sortOrder" filter-text="{{filterText}}" hide-checkbox="hideCheckbox" clicked-item="clickedWi" clicked-field="clickedField" selected-items="selectedWiArray" bpm-on-clicked="bpmOnClicked && bpmOnClicked()"></bpm-item-table> ',
            scope: {
                "wiArray": "=",
                "columns" : "=?",
                "sortBy" : "=?",
                "sortOrder" : "=?",
                "filterText" : "@?",
                "hideCheckbox" : "=?",
                "clickedWi": "=?",
                "clickedField": "=?",
                "selectedWiArray": "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemListFeed
    // bpm-work-item-list-feed
    ///////////////////////////////////
    // Directive to display WorkItem List
    function bpmWorkItemListFeed() {
        var directive = {
            restrict: 'E',
            template:   '<ul class="list-group">' +
            '<li ng-repeat="wi in vm.wiArray | orderBy:vm.sortBy:vm.sortOrder" class="list-group-item">' +
            '<bpm-work-item-list-item wi="wi" bpm-on-clicked="vm.wiClicked(wi,{clickable:true,name:name})"></bpm-work-item-list-item>' +
            '</li>' +
            '</ul>',
            controller: 'bpmWorkItemFeedCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "wiArray": "=",
                "sortBy" : "@?",
                "sortOrder" : "=?",
                "clickedWi": "=",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemListItemCtrl
    ///////////////////////////////////
    bpmWorkItemListItemCtrl.$inject = ['$scope','$timeout'];
    function bpmWorkItemListItemCtrl($scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // functions
        vm.wiClicked= wiClicked;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function wiClicked()
        {
            console.log("bpmWorkItemListItemCtrl wiClicked");
            if(vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
        }
    }

    ///////////////////////////////////
    // bpmWorkItemListItem
    // bpm-work-item-list-item
    ///////////////////////////////////
    // Directive to display an item in the Work Item List
    function bpmWorkItemListItem() {
        var directive = {
            restrict: 'EA',
            template: '<h2 style="cursor: pointer"><a ng-click="vm.wiClicked()">{{vm.wi.name}}</a></h2>' +
            '<bpm-work-item-instance-name-desc wi="vm.wi"></bpm-work-item-instance-name-desc>' +
            '<bpm-time-as-words-from-now time-in-millis="{{vm.wi.creationTime}}"></bpm-time-as-words-from-now>' +
            '<bpm-due-date-progress-bar ng-show="vm.wi.dueDate" created-time-stamp="{{vm.wi.creationTime}}" due-date-time-stamp="{{vm.wi.dueDate}}"></bpm-due-date-progress-bar>' +
            '<p><bpm-work-item-state state="{{vm.wi.state}}"></bpm-work-item-state></p>',
            controller: 'bpmWorkItemListItemCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "wi": "=",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemListFilterSelect
    // DIRECTIVE
    // bpm-work-item-list-filter-select
    //      Select box with Workitem list filters
    // events:
    ///////////////////////////////////
    function bpmWorkItemListFilterSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedFilter" name="wiFilterSelect" id="wiFilterSelectId" class="form-control">' +
            '<option value="MyCurrentWorkItems">{{"workItemFilter.MyCurrentWorkItems" | translate}}</option>' +
            '<option value="MyWorkItems">{{"workItemFilter.MyWorkItems" | translate}}</option>' +
            '<option value="MyActiveWorkItems">{{"workItemFilter.MyActiveWorkItems" | translate}}</option>' +
            '<option value="MyAcceptedWorkItems">{{"workItemFilter.MyAcceptedWorkItems" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedFilter": "="
            }
        };
    }


    ///////////////////////////////////
    // bpmWorkItemListSortSelect
    // DIRECTIVE
    // bpm-work-item-list-sort-select
    //      Select box with standard Workitem sort criteria
    // events:
    ///////////////////////////////////
    function bpmWorkItemListSortSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedSort" name="wiSortSelect" id="wiSortSelectId" class="form-control">' +
            '<option value="creationTime">{{"time.creationDate" | translate}}</option>' +
            '<option value="dueDate">{{"time.dueDate" | translate}}</option>' +
            '<option value="priority">{{"priority.priority" | translate}}</option>' +
            '<option value="name">{{"name" | translate}}</option>' +
            '<option value="instanceName">{{"bpm.instanceName" | translate}}</option>' +
            '<option value="state">{{"workItemState.state" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedSort": "="
            }
        };
    }

    ///////////////////////////////////
    // bpmWorkItemListSortSelectReversible
    // DIRECTIVE
    // bpm-work-item-list-sort-select-reversible
    //      Select box with standard Work Item sort criteria and a reverse button
    // events:
    ///////////////////////////////////
    function bpmWorkItemListSortSelectReversible() {
        return {
            restrict: 'EA',
            template:   '<div class="input-group" ng-init="sortOrder= true">' +
            '<bpm-work-item-list-sort-select selected-sort="selectedSort"></bpm-work-item-list-sort-select>' +
            '<span class="input-group-addon" style="cursor: pointer">' +
            '<span ng-show="sortOrder" class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '<span ng-hide="sortOrder" class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '</span></div>',
            scope: {
                "selectedSort": "=",
                "sortOrder": "="
            }
        };
    }


    ///////////////////////////////////
    // Work Item Directives
    ///////////////////////////////////

    angular
        .module('bpm-angular-ui')
        .directive('bpmWorkItemHeader',bpmWorkItemHeader)
        .directive('bpmWorkItemUdaList',bpmWorkItemUdaList)
        .controller('bpmWorkItemChoiceCtrl', bpmWorkItemChoiceCtrl)
        .directive('bpmWorkItemChoiceButton', bpmWorkItemChoiceButton)
        .directive('bpmWorkItemChoicesButtons', bpmWorkItemChoicesButtons)
        .controller('bpmWorkItemSaveCtrl', bpmWorkItemSaveCtrl)
        .directive('bpmWorkItemSaveButton', bpmWorkItemSaveButton)
        .directive('bpmWorkItemChoicesSelect', bpmWorkItemChoicesSelect)
        .directive('bpmWorkItemNameDesc', bpmWorkItemNameDesc)
        .directive('bpmWorkItemInstanceNameDesc', bpmWorkItemInstanceNameDesc)
        .directive('bpmWorkItemState', bpmWorkItemState)
        .controller('bpmSubTaskModalCtrl', bpmSubTaskModalCtrl)
        .controller('bpmSubTaskListCtrl', bpmSubTaskListCtrl)
        .directive('bpmSubTaskList', bpmSubTaskList)
        .directive('bpmSubTaskDescriptionDropdown', bpmSubTaskDescriptionDropdown)
        .controller('bpmWorkItemActionsDropdownCtrl', bpmWorkItemActionsDropdownCtrl)
        .directive('bpmWorkItemActionsDropdown', bpmWorkItemActionsDropdown)
    ;

    ///////////////////////////////////
    // bpmWorkItemHeader
    // bpm-work-item-header
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmWorkItemHeader() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-work-item-name-desc wi="wi"></bpm-work-item-name-desc>' +
            '<p><bpm-work-item-instance-name-desc wi="wi"></bpm-work-item-instance-name-desc></p>' +
            '<p>{{wi.creationTime | date:"medium"}}</p>' +
            '<bpm-due-date-progress-bar ng-show="wi.dueDate" created-time-stamp="{{wi.creationTime}}" due-date-time-stamp="{{wi.dueDate}}"></bpm-due-date-progress-bar>' +
            '<p><bpm-work-item-state state="{{wi.state}}"></bpm-work-item-state></p>',
            scope: {
                "wi": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmWorkItemUdaList
    // bpm-work-item-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Work Item
    function bpmWorkItemUdaList() {
        var directive = {
            restrict: 'E',
            template: '<bpm-generic-uda-list udas="vm.workItem.pd.dataItemRefs" uda-values="vm.workItem.pi.uda" extended-att-array="vm.extAttArray()">' +
            '</bpm-generic-uda-list>',
            controllerAs: 'vm',
            bindToController: true,
            controller : function () {
                var vm = this;
                vm.extAttArray = extAttArray;

                function getNodeInstanceByIdInst(idInst) {
                    return vm.workItem.pi.nodes.find(
                        function(piNode){
                            return (piNode.idInst == idInst)
                        }
                    );
                }

                function getNodeDefinitionById(id) {
                    return vm.workItem.pd.nodes.find(
                        function(pdNode){
                            return (pdNode.id == id)
                        }
                    );
                }

                function extAttArray() {
                    if (vm.workItem){
                        var nodeInstance= getNodeInstanceByIdInst(vm.workItem.wi.nodeInstanceId);
                        var nodeDefinition= getNodeDefinitionById(nodeInstance.idDef);
                        return nodeDefinition.ExtendedAttributes.ExtendedAttribute;
                    }
                }
            },
            scope: {
                "workItem": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmWorkItemChoiceCtrl
    ///////////////////////////////////
    bpmWorkItemChoiceCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmWorkItemChoiceCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER

        // functions
        vm.makeChoice= makeChoice;
        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function makeChoice(){
            // This calls the client's function to tell them a Choice button has been clicked so they can hide buttons while it is processing.
            if (vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
            BPM
                .makeChoice(vm.workItem,vm.choiceName)
                .success(
                    function (data, status) {
                        vm.workItem=data;
                        if (vm.bpmOnComplete){
                            $timeout(function(){
                                vm.bpmOnComplete();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        $scope.$emit('bpmErrorEvent', data);
                        console.log("makeChoice error",data);
                    }
                )
            ;
        }

        initialize();
        function initialize() {
        }
    }

    ///////////////////////////////////
    // bpmWorkItemChoiceButton
    // <bpm-work-item-choice-button work-item="workItemObject" choice="choiceNameString"></bpm-work-item-choice>
    // You can add optional call back functions here:
    //      bpmOnClicked: the button was clicked
    //      bpmOnComplete: the make choice command completed on the server
    // This needs the entire Work Item object because it needs access to the UDA values
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmWorkItemChoiceButton() {
        var directive = {
            restrict: 'E',
            template: '<button ' +
            'class="btn btn-primary workItemChoiceButtonClass" ' +
            'ng-click="vm.makeChoice()" >{{vm.choiceName}}</button>',
            controller: 'bpmWorkItemChoiceCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                workItem: '=',
                choiceName: '@',
                bpmOnClicked: '&?',
                bpmOnComplete: '&?'
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemChoicesButtons
    // bpm-work-item-choices-buttons
    // <bpm-work-item-choices-buttons work-item="workItemObject" choice-clicked="choiceNameString" choice-made="choiceNameString"></bpm-work-item-choices-buttons>
    // choice-clicked
    // This needs the entire Work Item object because it needs access to the UDA values
    ///////////////////////////////////
    function bpmWorkItemChoicesButtons() {
        var directive;
        directive = {
            restrict: 'EA',
            template: '<ul class="list-inline" ng-init="bpmClass= ' +
            "'btn btn-primary'" +
            '"><li ng-repeat="choiceName in workItem.wi.choices">' +
            '<bpm-work-item-choice-button work-item="workItem" bpm-on-clicked="myChoiceClicked(choiceName)" bpm-on-complete="myChoiceComplete(choiceName)" choice-name="{{choiceName}}"></bpm-work-item-choice>' +
            '</li>' +
            '<li ng-show="workItem && showSave"><bpm-work-item-save-button work-item="workItem" bpm-on-clicked="myOnSaveClicked()" bpm-on-complete="myOnSaved()"></bpm-work-item-save-button></li>' +
            '</ul>',
            controller: ['$scope','$timeout', function ($scope,$timeout) {
                $scope.myChoiceClicked= function (choice) {
                    $scope.selectedChoice= choice;
                    if ($scope.bpmOnClicked){
                        $timeout(function(){
                            $scope.bpmOnClicked();
                        });
                    }
                };
                $scope.myChoiceComplete= function (choice) {
                    $scope.selectedChoice= choice;
                    if ($scope.bpmOnComplete){
                        $timeout(function(){
                            $scope.bpmOnComplete();
                        });
                    }
                };
                $scope.myOnSaveClicked= function () {
                    if ($scope.bpmOnSaveClicked){
                        $timeout(function(){
                            $scope.bpmOnSaveClicked();
                        });
                    }
                };
                $scope.myOnSaved= function () {
                    if ($scope.bpmOnSaved){
                        $timeout(function(){
                            $scope.bpmOnSaved();
                        });
                    }
                };
            }],
            scope: {
                workItem : '=',
                showSave : '@?',
                bpmOnClicked : '&?',
                bpmOnComplete : '&?',
                bpmOnSaveClicked : '&?',
                bpmOnSaved : '&?',
                selectedChoice : '=?'
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmWorkItemSaveCtrl
    ///////////////////////////////////
    bpmWorkItemSaveCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmWorkItemSaveCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER

        // functions
        vm.save= save;
        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function save(){
            // This calls the client's function to tell them the save button has been clicked so they can hide buttons while it is processing.
            if (vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
            BPM
                .updateWorkItem(vm.workItem)
                .success(
                    function (data, status) {
                        vm.workItem=data;
                        if (vm.bpmOnComplete){
                            $timeout(function(){
                                vm.bpmOnComplete();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        $scope.$emit('bpmErrorEvent', data);
                        console.log("Work Item Save error",data);
                    }
                )
            ;
        }

        initialize();
        function initialize() {
        }
    }

    ///////////////////////////////////
    // bpmWorkItemSaveButton
    // bpm-work-item-save-button
    // This needs the entire Work Item object because it needs access to the UDA values
    ///////////////////////////////////
    function bpmWorkItemSaveButton() {
        var directive = {
            restrict: 'EA',
            template: '<button class="btn btn-primary workItemSaveButtonClass" ng-click="vm.save()" ><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>',
            controller: 'bpmWorkItemSaveCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                workItem: '=',
                bpmOnClicked: '&?',
                bpmOnComplete: '&?'
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemChoicesSelect
    // bpm-work-item-choices-select
    // Show the work item choices as a select box
    // Work Item choice made:   $scope.$emit('bpmWorkItemMadeChoiceEvent', workItem, choice);
    //      var largeWI.wi= workListWI;
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmWorkItemChoicesSelect() {
        var directive = {
            restrict: 'EA',
            template: '<select class="form-control" ng-model="vm.selectedChoice" ' +
            'ng-options="choice for choice in vm.workItem.wi.choices"></select>',
            controller: 'bpmWorkItemChoicesCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                workItem: '='
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemNameDesc
    // bpm-work-item-name-desc
    // Show the work item name and description if it has one.
    ///////////////////////////////////
    function bpmWorkItemNameDesc() {
        var directive = {
            restrict: 'E',
            template: '<bpm-info-well bpm-on-clicked="bpmOnClicked && bpmOnClicked()" ng-init="bpmClass=' +
            "'h1'" +
            '" name="{{wi.name}}" description="{{wi.instanceDescription}}" bpm-class="{{bpmClass}}"></bpm-info-well>',
            scope: {
                wi: '=',
                bpmClass : "@?",
                bpmOnClicked: '&?'
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmWorkItemInstanceNameDesc
    // bpm-work-item-instance-name-desc
    // Show the process instance name and description for the work item.
    ///////////////////////////////////
    function bpmWorkItemInstanceNameDesc() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-info-well bpm-on-clicked="bpmOnClicked && bpmOnClicked()" ng-init="bpmClass=' +
            "'h4'" +
            '" name="{{wi.instanceName}}" description="{{wi.instanceDescription}}" bpm-class="{{bpmClass}}"></bpm-info-well>',
            scope: {
                wi: '=',
                bpmClass : "@?",
                bpmOnClicked: '&?'
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmWorkItemState
    // bpm-work-item-state
    ///////////////////////////////////
    // Directive to display work item state with image
    function bpmWorkItemState() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-style="{color: myStyle[state]}" class="{{className[state]}}" aria-hidden="true"></span> <span ng-show="state">{{"workItemState."+state | translate}}</span>',
            scope: {
                "state":"@"
            },
            controller : function($scope) {
                $scope.className = {
                    0  : 'glyphicon glyphicon-remove',
                    3  : 'glyphicon glyphicon-play',
                    4  : 'glyphicon glyphicon-remove',
                    5  : 'glyphicon glyphicon-ok',
                    6  : 'glyphicon glyphicon-thumbs-down',
                    8  : 'glyphicon glyphicon-ok',
                    12 : 'glyphicon glyphicon-pause',
                    14 : 'glyphicon glyphicon-stop'
                };

                $scope.myStyle = {
                    0  : "black",
                    3  : "green",
                    4  : "black",
                    6  : "blue",
                    8  : "green",
                    12 : "orange",
                    14 : "red"
                };
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmSubTaskListCtrl
    ///////////////////////////////////
    bpmSubTaskListCtrl.$inject = ['BPMService','$scope'];
    function bpmSubTaskListCtrl(BPMService,$scope) {
        var vm = this;

        initialize();
        function initialize() {
             // Watch for the workItem to change in order to get the sub tasks.
             $scope.$watch('vm.workItem',
                 function(){
                    if (vm.workItem){
                        vm.workItem.subTaskList= BPMService.filterSubTaskList(vm.workItem);
                    }
                }
             );
        }
    }


    ///////////////////////////////////
    // bpmSubTaskList
    // bpm-sub-task-list
    ///////////////////////////////////
    // Directive to display a list of sub tasks for the given workitem
    function bpmSubTaskList() {
        var directive = {
            restrict: 'EA',
            template: '<ul class="list-group">' +
            '<li class="list-group-item" ng-repeat="subTask in vm.workItem.subTaskList | orderBy:' + "'state'" + '">' +
            '<h2>{{subTask.name}} <small ng-show="subTask.description">' +
            '<span style="color: blue;" popover-placement="bottom" uib-popover="{{subTask.description}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>' +
            '</small></h2><small><bpm-work-item-state state="{{subTask.state}}"></bpm-work-item-state></small>' +
            '<div class="row" ng-hide="subTask.state==0 || subTask.state==4">' +
            '<small class="col-sm-6">pour <span ng-repeat="assignee in subTask.assignees">{{assignee}}, </span></small>' +
            '<small ng-hide="subTask.priority<0" class="col-sm-6">{{' + "'priority.priority'" + ' | translate}} ' +
            '<bpm-priority-text priority="{{subTask.priority}}"></bpm-priority-text></small></div></li></ul>',
            controller: 'bpmSubTaskListCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                workItem: '='
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmSubTaskDescriptionDropdown
    // bpm-sub-task-description-dropdown
    ///////////////////////////////////
    // Directive to display a list of sub tasks for the given workitem
    function bpmSubTaskDescriptionDropdown() {
        var directive = {
            restrict: 'EA',
            template: '<div class="btn-group" uib-dropdown auto-close="disabled">' +
            '<button type="button" class="btn btn-default" uib-dropdown-toggle>{{"description" | translate}} <span class="caret"></span></button>' +
            '<ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">' +
            '<textarea class="form-control" ng-model="vm.task.description" rows="4" ng-attr-placeholder="{{' +
            "'description'" + ' | translate}}"></textarea></ul></div>',
            scope: {
                description: '='
            }
        };
        return directive;
    }


    bpmSubTaskModalCtrl.$inject = ['$scope','$uibModalInstance','BPMAdmin'];
    function bpmSubTaskModalCtrl($scope,$uibModalInstance,BPMAdmin) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER
        // task.name is the sub task name
        // task.assignees is the list of selected users.
        vm.task={};
        vm.selectedPriority = -1;
        // populate groupList with the list of groups fromthe directory.
        vm.groupList=[];

        // functions:
        vm.ok=ok;
        vm.cancel=cancel;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        // Fetch the groups from the server for the display.
        function fetchGroups() {
            BPMAdmin
                .getGroupList(configSettings.tenant)
                .success(
                    function (data, status) {
                        vm.groupList=data.groups;
                        console.log("Groups:",vm.groupList);
                    }
                )
                .error(
                    function (data, status) {
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        function ok() {
            // on close we return the task object containing the sub task name and assignees.
            vm.task.priority = vm.selectedPriority;
            $uibModalInstance.close(vm.task);
        }
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }


        initialize();
        function initialize() {
            fetchGroups();
            vm.task.name="";
            vm.task.assignees=[];
        }
    }


    ///////////////////////////////////
    // bpmWorkItemActionsDropdownCtrl
    ///////////////////////////////////
    bpmWorkItemActionsDropdownCtrl.$inject = ['BPM','$scope','$uibModal','$window'];
    function bpmWorkItemActionsDropdownCtrl(BPM,$scope,$uibModal,$window) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;

        // functions
        vm.showReassign= showReassign;
        vm.acceptWorkItem= acceptWorkItem;
        vm.declineWorkItem= declineWorkItem;
        vm.showDynamicProcess = showDynamicProcess;
        vm.showSubTask= showSubTask;
        vm.getPreviouslyCompletedProcessInstances= getPreviouslyCompletedProcessInstances;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function findActivityDefinition(wi, pi, pd) {
            var activityInstance = pi.nodes.find(function (a) { return a.idInst === wi.nodeInstanceId; });
            return pd.nodes.find(function (a) { return a.id === activityInstance.idDef; });
        }

        // Show the reassign modal dialog.
        function showReassign()
        {
            var activityRole = findActivityDefinition(vm.workItem.wi, vm.workItem.pi, vm.workItem.pd).role;

            var modalInstance = $uibModal.open(
                {
                    animation: true,
                    templateUrl: "bpm-angular/userSelectorModalTemplate.html",
                    controller: "ReassignWorkItemCtrl",
                    controllerAs: "vm",
                    resolve: {groupList: function() { return [activityRole]; } },
                    bindToController: true,
                    size: "lg"
                }
            );

            modalInstance
                .result
                .then(
                    function (users) {
                        vm.selectedUsers = users;
                        reassignWorkItem();
                    },
                    function () {
                        console.log('Modal dismissed at:', new Date());
                    }
                )
            ;
        }


        // Reassign the work item to the selected users.
        function reassignWorkItem(){
            vm.updating=true;
            var assignees= vm.selectedUsers.map(
                function(user){
                    return user.name;
                }
            );
            BPM
                .reassign(configSettings.tenant,configSettings.application,vm.workItem,assignees)
                .success(
                    function (data, status) {
                        vm.updating = false;
                        $scope.$emit('workItemReassignedEvent', vm.workItem);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        // Show Dynamic Process Modal Dialog
        function showDynamicProcess()
        {
            var modalInstance = $uibModal.open(
                {
                    templateUrl: "../template/startDynamicProcessTemplate.html",
                    controller: "StartDynamicProcessCtrl",
                    controllerAs: "vm",
                    bindToController: true,
                    size: "lg"
                }
            );

            modalInstance
                .result
                .then(
                    function (dynamicPlanName) {
                        vm.selectedDynamicPlanName = dynamicPlanName;
                        startDynamicProcess();
                    },
                    function () {
                        console.log('Modal dismissed at:', new Date());
                    }
                )
            ;
        }

        // Start Dynamic Process
        function startDynamicProcess(){
            vm.updating=true;
            BPM
                .startDynamicProcess(vm.workItem,vm.selectedDynamicPlanName)
                .success(
                    function (data, status) {
                        vm.updating = false;
                        vm.workItem = data;
                        $scope.$emit('bpmWorkItemStartDynamicProcessEvent', vm.workItem);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        function acceptWorkItem(){
            vm.updating=true;
            BPM
                .accept(vm.workItem)
                .success(
                    function (data, status) {
                        vm.workItem= data;
                        vm.updating=false;
                        $scope.$emit('bpmWorkItemAcceptedEvent', vm.workItem);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        function declineWorkItem(){
            vm.updating=true;
            BPM
                .decline(vm.workItem)
                .success(
                    function (data, status) {
                        vm.workItem= data;
                        vm.updating=false;
                        $scope.$emit('bpmWorkItemDeclinedEvent', vm.workItem);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        // Show the subTask modal dialog.
        function showSubTask()
        {
            var modalInstance = $uibModal.open(
                {
                    animation: true,
                    template: '<div class="modal-header"><h3 class="modal-title">{{"addSubTask"| translate}}</h3></div>' +
                    '<form style="margin: 10px" class="form-inline">' +
                    '<div class="form-group"><label for="inputTaskName">{{"bpm.task" | translate}}</label>' +
                    '<input ng-model="vm.task.name" type="text" class="form-control" id="inputTaskName" placeholder="{{' +
                    "'bpm.taskName'" +' | translate}}"></div>' +
                    '<div class="form-group"><bpm-due-date-selector due-date="vm.task.dueDate"></bpm-due-date-selector></div>' +
                    '<div class="form-group"><div class="input-group"><bpm-priority-select selected-priority="vm.selectedPriority"></bpm-priority-select></div></div>' +
                    '<div class="form-group"><bpm-sub-task-description-dropdown description="vm.task.description"></bpm-sub-task-description-dropdown></div>' +
                    '<bpm-user-selector selected-users="vm.task.assignees" group-list="vm.groupList"></bpm-user-selector>' +
                    '<div class="modal-footer"><div align="right">' +
                    '<button type="button" class="btn btn-primary" ng-click="vm.ok()">{{"ok" | translate}}</button>' +
                    '<button class="btn btn-warning" type="button" ng-click="vm.cancel()">{{"cancel" | translate}}</button></div></div></form>',
                    controller: "bpmSubTaskModalCtrl",
                    controllerAs: "vm",
                    bindToController: true,
                    size: "lg"
                }
            );

            modalInstance
                .result
                .then(
                    function (task) {
                        createSubTask(task);
                    },
                    function () {
                        console.log('Modal dismissed at:', new Date());
                    }
                )
            ;
        }

        function createSubTask(subTask){
            vm.updating=true;
            console.log("createSubTask");
            // We should probably fix the user selector so it sends back an array of strings instead of these objects.
            var assignees= subTask.assignees.map(
                function(user){
                    return user.name;
                }
            );
            subTask.assignees=assignees;
            subTask.due= subTask.dueDate ? subTask.dueDate.getTime() : null;
            BPM
                .createSubTask(vm.workItem,subTask)
                .success(
                    function (data, status) {
                        console.log("subTask",data);
                        vm.workItem= data;
                        vm.updating=false;
                        $scope.$emit('bpmWorkItemSubtaskEvent', vm.workItem);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        // Get all process instances with this same Plan name.
        // This is used so the user can see how work was done previously.
        function getPreviouslyCompletedProcessInstances(){
            vm.updating=true;
            var planName= vm.workItem.pd.name;
            vm.previouslyCompletedProcessInstances= [];
            var planSearchCriteria= {};

            // Specify search by Plan
            planSearchCriteria.filterBY=[];
            var planNameSearch={};
            planNameSearch.listField=BPM.pdListField.LISTFIELD_PLAN_NAME;
            planNameSearch.sqlOperator="=";
            planNameSearch.value=planName;
            planSearchCriteria.filterBY.push(planNameSearch);

            // We only want completed processes
            var completedProcessSearch={};
            completedProcessSearch.listField=BPM.piListField.STATE;
            completedProcessSearch.sqlOperator="=";
            completedProcessSearch.value=BPM.states._closed.toString();
            planSearchCriteria.filterBY.push(completedProcessSearch);

            // Sort by completion date
            planSearchCriteria.sortBy=[];
            var sortCriteria= {};
            // LISTFIELD_PROCESSINSTANCE_CLOSEDTIME
            sortCriteria.listField= BPM.piListField.CLOSEDTIME;
            // sort by descending time so we see most recent first
            sortCriteria.sortOrder= false;
            planSearchCriteria.sortBy.push(sortCriteria);


            var piFilter= "AllProcesses";
            BPM
                .searchProcessInstances(piFilter,planSearchCriteria)
                .success(
                    function (data, status) {
                        vm.previouslyCompletedProcessInstances=data.piList;
                        console.log("previouslyCompletedProcessInstances",vm.previouslyCompletedProcessInstances);
                        vm.updating=false;
                        $scope.$emit('bpmProcessInstanceListUpdatedEvent', vm.previouslyCompletedProcessInstances);
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

    }


    ///////////////////////////////////
    // bpmWorkItemActionsDropdown
    // bpm-work-item-actions-dropdown
    ///////////////////////////////////
    // Directive to display a drop with WorkItem Actions
    // previouslyCompletedProcessInstances gets popluated when user clicks on "Previously Completed"
    function bpmWorkItemActionsDropdown() {
        var directive = {
            restrict: 'EA',
            template: '<div class="btn-group" uib-dropdown is-open="status.isopen">' +
            '<button id="single-button" type="button" class="btn" uib-dropdown-toggle ng-disabled="disabled">Task Actions <span class="caret"></span></button>' +
            '<ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">' +
            '<li role="menuitem" ng-click="vm.acceptWorkItem()"><a>{{"bpm.accept" | translate}}<span style="color: green" class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></a></li>' +
            '<li role="menuitem" ng-click="vm.declineWorkItem()"><a>{{"bpm.decline" | translate}}<span style="color: red" class="glyphicon glyphicon-thumbs-down" aria-hidden="true">  </span></a></li>' +
            '<li role="menuitem" ng-click="vm.showReassign()"><a>{{"bpm.reassign" | translate}}<span style="color: blue" class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a></li>' +
            '<li class="divider"></li><li role="menuitem" ng-click="vm.showSubTask()"><a>{{"bpm.addSubTask" | translate}}</a></li>' +
//            '<li role="menuitem" ng-click="vm.showDynamicProcess()"><a>Dynamic Subprocess</a></li>' +
//            '<li class="divider"></li><li role="menuitem" ng-click="vm.getPreviouslyCompletedProcessInstances()"><a>Previously Completed</a></li>' +
            '</ul></div>',
            controller: 'bpmWorkItemActionsDropdownCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                workItem: '=',
                previouslyCompletedProcessInstances: '=?'
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // Process Definition List Directives
    ///////////////////////////////////

    angular
        .module('bpm-angular-ui')
        .controller('bpmProcessDefinitionListCtrl', bpmProcessDefinitionListCtrl)
        .directive('bpmProcessDefinitionListTableValue', bpmProcessDefinitionListTableValue)
        .directive('bpmProcessDefinitionListTable', bpmProcessDefinitionListTable)
        .directive('bpmProcessDefinitionListFeed', bpmProcessDefinitionListFeed)
        .directive('bpmProcessDefinitionListItem', bpmProcessDefinitionListItem)
        .directive('bpmProcessDefinitionListFilterSelect', bpmProcessDefinitionListFilterSelect)
        .directive('bpmProcessDefinitionListSortSelect', bpmProcessDefinitionListSortSelect)
        .directive('bpmProcessDefinitionListSortSelectReversible', bpmProcessDefinitionListSortSelectReversible)
    ;


    ///////////////////////////////////
    // bpmProcessDefinitionListCtrl
    ///////////////////////////////////
    bpmProcessDefinitionListCtrl.$inject = ['$scope','BPM','TimeFactory','$timeout'];
    function bpmProcessDefinitionListCtrl($scope,BPM,TimeFactory,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // functions
        vm.checkClicked = checkClicked;
        vm.pdClicked= pdClicked;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
        // User clicked on the checkbox for a pd so update it and the list of selected pd's
        function checkClicked(pd) {
            pd.isChecked = !pd.isChecked;
            vm.selectAllCheckboxApi.updateSelected();
        }

        function pdClicked(pd){
            vm.clickedPd= pd;
            if (vm.bpmOnClicked){
                $timeout(function(){
                    vm.bpmOnClicked();
                });
            }
        }

        initialize();
        function initialize() {
            vm.clickedPd= null;

            // Watch the columns so we can set a default
            $scope.$watch('vm.columns',
                function() {
                    if (!vm.columns || vm.columns==null || vm.columns==''){
                        vm.columnsArray=["id","name","assignee","instanceName","priority","choices"];
                    }
                    else{
                        vm.columnsArray=vm.columns.split(",");
                    }
                }
            );

            // When the list of pds changes the currently selected Definition is empty
            $scope.$watch('vm.pdArray',
                function() {
                    vm.clickedPd= null;
                }
            );
        }
    }

    ///////////////////////////////////
    // bpmProcessDefinitionListTableValue
    // DIRECTIVE
    // bpm-process-definition-list-table-value
    ///////////////////////////////////
    function bpmProcessDefinitionListTableValue() {
        return {
            restrict: 'EA',
            template: '<div ng-if="!field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}">' +
            '<span ng-if="field.name==' + "'application'" + '">{{pd.application}}</span>' +
            '<span ng-if="field.name==' + "'desc'" + '">{{pd.desc}}</span>' +
            '<span ng-if="field.name==' + "'id'" + '">{{pd.id}}</span>' +
            '<bpm-priority-text ng-if="field.name==' + "'priority'" + '" priority="{{pd.priority}}"></bpm-priority-text>' +
            '<bpm-process-definition-state ng-if="field.name==' + "'state'" + '" state="{{pd.state}}"></bpm-process-definition-state>' +
            '<span ng-if="field.name==' + "'version'" + '">{{pd.version}}</span>' +
            '</span>' +
            '<bpm-process-definition-name-desc bpm-on-clicked="bpmOnClicked && bpmOnClicked()" bpm-class="{{field.clickable?' +
            "'h4 btn-link'" + ':' + "'h4'" +
            '}}" ng-if="field.name==' + "'name'" + '" pd="pd"></bpm-process-definition-name-desc>' +
            '</div>' +
            '<div ng-if="field.uda">' +
            '<span ng-click="bpmOnClicked && bpmOnClicked()" ng-class="{' +"'btn btn-link'" + ':field.clickable}"><span ng-hide="field.translateTable">{{pd.uda[field.name]}}</span><span ng-show="field.translateTable">{{field.translateTable+"."+pd.uda[field.name] | translate}}</span></span>' +
            '</div>',
            scope: {
                field : "=",
                pd : "=",
                bpmOnClicked : "&?"
            }
        };
    }

    ///////////////////////////////////
    // bpmProcessDefinitionListTable
    // bpm-process-Definition-list-table
    // DIRECTIVE
    // events:
    ///////////////////////////////////
    // Directive to display Process Definition List
    function bpmProcessDefinitionListTable() {
        var directive = {
            restrict: 'E',
            template: '<bpm-item-table items="pdArray" item-type="pd" columns="columns" sort-by="sortBy" sort-order="sortOrder" filter-text="{{filterText}}" hide-checkbox="hideCheckbox" clicked-item="clickedPd" clicked-field="clickedField" selected-items="selectedPdArray" bpm-on-clicked="bpmOnClicked && bpmOnClicked()"></bpm-item-table> ',
            scope: {
                "pdArray": "=",
                "columns" : "=?",
                "sortBy" : "@?",
                "sortOrder" : "=?",
                "filterText" : "@?",
                "hideCheckbox" : "=?",
                "clickedPd": "=?",
                "clickedField": "=?",
                "selectedPdArray": "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessDefinitionListFeed
    // bpm-process-definition-list-feed
    ///////////////////////////////////
    // Directive to display feed of process definitions
    function bpmProcessDefinitionListFeed() {
        var directive = {
            restrict: 'E',
            template:   '<ul class="list-group">' +
            '<li ng-repeat="pd in vm.pdArray | orderBy:vm.sortBy:vm.sortOrder" class="list-group-item">' +
            '<bpm-process-definition-list-item pd="pd" bpm-on-clicked="vm.pdClicked(pd)"></bpm-process-definition-list-item>' +
            '</li>' +
            '</ul>',
            controller: 'bpmProcessDefinitionListCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "pdArray": "=",
                "sortBy" : "@?",
                "sortOrder" : "=?",
                "clickedPd": "=?",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessDefinitionListItem
    // bpm-process-definition-list-item
    ///////////////////////////////////
    // Directive to display an item in the Work Item List
    function bpmProcessDefinitionListItem() {
        var directive = {
            restrict: 'E',
            template: '<div class="row">' +
            '<div class="col-sm-5">' +
            '<bpm-process-definition-name-desc pd="pd" bpm-on-clicked="bpmOnClicked && bpmOnClicked()"></bpm-process-definition-name-desc>' +
            '</div>' +
            '<div class="col-sm-7"><p>{{"pdTable.id" | translate}}: {{pd.id}}</p>' +
            '{{"pdTable.state" | translate}}: <bpm-process-definition-state state="{{pd.state}}"></bpm-process-definition-state>' +
            '<p>{{"pdTable.version" | translate}}: {{pd.version}}</p>' +
            '</div></div>',
            scope: {
                "pd": "=",
                "bpmOnClicked" : "&?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessDefinitionListFilterSelect
    // DIRECTIVE
    // bpm-process-definition-list-filter-select
    //      Select box with Process Definition list filters
    // events:
    ///////////////////////////////////
    function bpmProcessDefinitionListFilterSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedFilter" name="pdFilterSelect" id="pdFilterSelectId" class="form-control">' +
            '<option value="AllPlans">{{"processDefinitionFilter.AllPlans" | translate}}</option>' +
            '<option value="MyPlans">{{"processDefinitionFilter.MyPlans" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedFilter": "="
            }
        };
    }


    ///////////////////////////////////
    // bpmProcessDefinitionListSortSelect
    // DIRECTIVE
    // bpm-process-Definition-list-sort-select
    //      Select box with standard Process Definition sort criteria
    // events:
    ///////////////////////////////////
    function bpmProcessDefinitionListSortSelect() {
        return {
            restrict: 'EA',
            template:   '<select ng-model="selectedSort" name="pdSortSelect" id="pdSortSelectId" class="form-control">' +
            '<option value="name">{{"name" | translate}}</option>' +
            '<option value="id">{{"bpm.plan" | translate}} {{"bpm.id" | translate}}</option>' +
            '<option value="state">{{"workItemState.state" | translate}}</option>' +
            '<option value="owner">{{"bpm.owner" | translate}}</option>' +
            '<option value="priority">{{"priority.priority" | translate}}</option>' +
            '<option value="version">{{"bpm.version" | translate}}</option>' +
            '</select>',
            scope: {
                "selectedSort": "="
            }
        };
    }

    ///////////////////////////////////
    // bpmProcessDefinitionListSortSelectReversible
    // DIRECTIVE
    // bpm-process-Definition-list-sort-select-reversible
    //      Select box with standard Process Definition sort criteria and a reverse button
    // events:
    ///////////////////////////////////
    function bpmProcessDefinitionListSortSelectReversible() {
        return {
            restrict: 'EA',
            template:   '<div class="input-group" ng-init="sortOrder= true">' +
            '<bpm-process-definition-list-sort-select selected-sort="selectedSort"></bpm-process-definition-list-sort-select>' +
            '<span class="input-group-addon" style="cursor: pointer">' +
            '<span ng-show="sortOrder" class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '<span ng-hide="sortOrder" class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true" ng-click="sortOrder=!sortOrder"></span>' +
            '</span></div>',
            scope: {
                "selectedSort": "=",
                "sortOrder": "="
            }
        };
    }

    ///////////////////////////////////
    // Process Definition Directives
    ///////////////////////////////////
    angular
        .module('bpm-angular-ui')
        .controller('bpmProcessDefinitionUdaListCtrl', bpmProcessDefinitionUdaListCtrl)
        .directive('bpmProcessDefinitionUdaList', bpmProcessDefinitionUdaList)
        .directive('bpmProcessDefinitionHeader', bpmProcessDefinitionHeader)
        .directive('bpmProcessDefinitionNameDesc', bpmProcessDefinitionNameDesc)
        .directive('bpmProcessDefinitionState', bpmProcessDefinitionState)
    ;

    ///////////////////////////////////
    // bpmProcessDefinitionUdaListCtrl
    ///////////////////////////////////
    bpmProcessDefinitionUdaListCtrl.$inject = ['$scope'];
    function bpmProcessDefinitionUdaListCtrl($scope) {
        var vm = this;
        // Need to set the values of the UDAs from the default PD values in the the pd

        vm.extAttArray = extAttArray;

        function setUdaValuesFromPD() {
            if (vm.processDefinition){
                vm.udaValues = {};
                vm.processDefinition.pd.dataItemRefs.forEach(
                    function(dataItemRef){
                        // Set the value based on the type of the UDA
                        vm.udaValues[dataItemRef.identifier] = dataItemRef.initValue;
                        if (dataItemRef.type == "INTEGER" || dataItemRef.type == "DATE" || dataItemRef.type == "BIGDECIMAL" || dataItemRef.type == "FLOAT" || dataItemRef.type == "LONG"){
                            vm.udaValues[dataItemRef.identifier] = Number(dataItemRef.initValue);
                        }
                        if (dataItemRef.type == "BOOLEAN"){
                            vm.udaValues[dataItemRef.identifier] = Boolean(dataItemRef.initValue);
                        }
                    }
                );
            }
        }

        // Get the extended attributes from the PD start node
        // The start node is the node with type == 0
        function getStartNodeDefinition() {
            return vm.processDefinition.pd.nodes.find(
                function(pdNode){
                    return (pdNode.type === 0)
                }
            );
        }

        function extAttArray() {
            if (vm.processDefinition){
                var startNodeDefinition= getStartNodeDefinition();
                return startNodeDefinition.ExtendedAttributes.ExtendedAttribute;
            }
        }

        initialize();
        function initialize() {
            // Watch for the processDefinition to be loaded in the control.
            $scope.$watch('vm.processDefinition',setUdaValuesFromPD);
        }
    }

    ///////////////////////////////////
    // bpmProcessDefinitionUdaList
    // bpm-process-definition-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Process Definition
    function bpmProcessDefinitionUdaList() {
        var directive = {
            restrict: 'E',
            template: '<bpm-generic-uda-list udas="vm.processDefinition.pd.dataItemRefs" uda-values="vm.udaValues" extended-att-array="vm.extAttArray()">' +
            '</bpm-generic-uda-list>',
            controllerAs: 'vm',
            bindToController: true,
            controller : bpmProcessDefinitionUdaListCtrl,
            scope: {
                "processDefinition": "<",
                "udaValues": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessDefinitionHeader
    // bpm-process-definition-header
    ///////////////////////////////////
    // Directive to display a Process Definition
    // This doesn't need a controller as it is display only.
    function bpmProcessDefinitionHeader() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-process-definition-name-desc pd="pd"></bpm-process-definition-name-desc>' +
            '<p>{{pd.owner}}</p>' +
            '<p><bpm-process-definition-state state="{{pd.state}}"></bpm-process-definition-state></p>',
            scope: {
                "pd": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessInstanceUdaList
    // bpm-process-instance-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Process Instance
    function bpmProcessInstanceUdaList() {
        var directive = {
            restrict: 'E',
            template: '<bpm-generic-uda-list udas="vm.processInstance.pd.dataItemRefs" uda-values="vm.processInstance.pi.uda" extended-att-array="vm.processInstance.pd.ExtendedAttributes.ExtendedAttribute">' +
            '</bpm-generic-uda-list>',
            controllerAs: 'vm',
            bindToController: true,
            controller : function () {
                var vm = this;
                vm.extAttArray = extAttArray;

                function getNodeInstanceByIdInst(idInst) {
                    return vm.workItem.pi.nodes.find(
                        function(piNode){
                            return (piNode.idInst == idInst)
                        }
                    );
                }

                function getNodeDefinitionById(id) {
                    return vm.workItem.pd.nodes.find(
                        function(pdNode){
                            return (pdNode.id == id)
                        }
                    );
                }

                function extAttArray() {
                    console.log("extAttArray");
                    if (vm.workItem){
                        var nodeInstance= getNodeInstanceByIdInst(vm.workItem.wi.nodeInstanceId);
                        var nodeDefinition= getNodeDefinitionById(nodeInstance.idDef);
                        return nodeDefinition.ExtendedAttributes.ExtendedAttribute;
                    }
                }
            },
            scope: {
                "processInstance": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessDefinitionNameDesc
    // bpm-process-definition-name-desc
    // Show the process definition name and description.
    ///////////////////////////////////
    function bpmProcessDefinitionNameDesc() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-info-well bpm-on-clicked="bpmOnClicked && bpmOnClicked()" ng-init="bpmClass=' +
            "'h1'" +
            '" name="{{pd.name}}" description="{{pd.desc}}" bpm-class="{{bpmClass}}"></bpm-info-well>',
            scope: {
                pd: '=',
                bpmClass : "@?",
                bpmOnClicked : "&?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessDefinitionState
    // bpm-process-definition-state
    ///////////////////////////////////
    // Directive to display Process Definition state with image
    function bpmProcessDefinitionState() {
        var directive = {
            restrict: 'EA',
            template: '<span ng-style="{color: style[state]}" class="{{className[state]}}" aria-hidden="true"></span> <span ng-show="state">{{"processDefinitionState."+state | translate}}</span>',
            scope: {
                "state":"@"
            },
            controller : function($scope) {
                // This section tells what icon to show for the state
                $scope.className = {
                    0  : "glyphicon glyphicon-info-sign",
                    1  : "glyphicon glyphicon-play",
                    2  : "glyphicon glyphicon-ban-circle",
                    3  : "glyphicon glyphicon-stop"
                };

                // This section tells what color the icon should be for the state
                $scope.style = {
                    0  : "blue",
                    1  : "green",
                    2  : "blue",
                    3  : "red"
                };
            }



        };
        return directive;
    }





    ///////////////////////////////////
    // UDA Directives
    ///////////////////////////////////

    angular
        .module('bpm-angular-ui')
        .directive('bpmWorkListUdas', bpmWorkListUdas)
        .directive('bpmOldUdaListItem', bpmOldUdaListItem)
        .directive('bpmUdaListItem', bpmUdaListItem)
        .controller('bpmUdaListCtrl', bpmUdaListCtrl)
        .directive('bpmUdaList', bpmUdaList)
        .controller('bpmGenericUdaListCtrl', bpmGenericUdaListCtrl)
        .directive('bpmGenericUdaList', bpmGenericUdaList)
        .directive('bpmOldInputUdaListItem', bpmOldInputUdaListItem)
        .controller('bpmOldDateUdaListItemCtrl', bpmOldDateUdaListItemCtrl)
        .directive('bpmOldDateUdaListItem', bpmOldDateUdaListItem)
        .controller('bpmInputUdaListItemCtrl', bpmInputUdaListItemCtrl)
        .directive('bpmInputUdaListItem', bpmInputUdaListItem)
        .controller('bpmDateUdaListItemCtrl', bpmDateUdaListItemCtrl)
        .directive('bpmDateUdaListItem', bpmDateUdaListItem)
    ;

    ///////////////////////////////////
    // bpmWorkListUdas
    // bpm-work-list-udas
    ///////////////////////////////////
    // Directive to display UDAs for a list of wi or pi objects
    // This should be used for displaying UDAs in Work List or Process List
    // For displaying UDAs for a Process Instance or Work Item see bpm-uda-list

    function bpmWorkListUdas() {
        var directive = {
            restrict: 'E',
            controller: function($scope) {
                $scope.$watch( "wiOrPi",
                    function(){
                        $scope.udas = [];
                        for (var uda in $scope.wiOrPi.uda) {
                            $scope.udas.push(uda);
                        }
                    }
                );
            },
            template: '<span ng-repeat="udaName in udas">{{udaName}}: <b>{{wiOrPi.uda[udaName]}}</b><br/></span> ',
            scope: {
                "wiOrPi": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmOldUdaListItem
    // bpm-old-uda-list-item
    ///////////////////////////////////
    // Directive to display a specific UDA
    // This should be used for displaying UDAs in a work item or process instance view.
    // udaDataItemRef: { name, type, identifier}
    // name: this is the name that will be displayed in the label
    // identifier: this specifies which UDA value will be displayed.
    // For displaying UDAs in a work list see work-item-display-work-list-udas
    function bpmOldUdaListItem() {
        var directive = {
            restrict: 'EA',
            template: '<bpm-old-input-uda-list-item input-type="text" ng-if="udaDataItemRef.type!='+"'BOOLEAN'" +'&& udaDataItemRef.type!='+"'DATE'"+' && udaDataItemRef.type!=' +
            "'INTEGER'"+' && udaDataItemRef.type!='+"'LONG'"+'" bpm-object="bpmObject" identifier="{{udaDataItemRef.identifier}}" label="{{udaDataItemRef.name}}"></bpm-old-input-uda-list-item>' +
            '<bpm-old-input-uda-list-item input-type="number" ng-if="udaDataItemRef.type=='+"'INTEGER'"+' || udaDataItemRef.type=='+"'LONG'"+'" bpm-object="bpmObject" identifier="{{udaDataItemRef.identifier}}" label="{{udaDataItemRef.name}}"></bpm-old-input-uda-list-item>' +
            '<div class="checkbox col-sm-offset-3 col-sm-9" ng-if="udaDataItemRef.type=='+"'BOOLEAN'"+'">' +
            '<label><input ng-model="bpmObject.pi.uda[udaDataItemRef.identifier]" type="checkbox">{{udaDataItemRef.name}}</label></div>' +
            '<bpm-old-date-uda-list-item ng-if="udaDataItemRef.type=='+"'DATE'"+'" bpm-object="bpmObject" uda-data-item-ref="udaDataItemRef"></bpm-old-date-uda-list-item>',
            scope: {
                "bpmObject": "=",
                "udaDataItemRef": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmUdaListItem
    // bpm-uda-list-item
    ///////////////////////////////////
    // Directive to display a specific UDA
    // This should be used for displaying UDAs in a work item or process instance view.
    // udaDataItemRef: { name, type, identifier}
    // name: this is the name that will be displayed in the label
    // identifier: this specifies which UDA value will be displayed.
    // For displaying UDAs in a work list see work-item-display-work-list-udas
    function bpmUdaListItem() {
        var directive = {
            restrict: 'E',
            template: '<bpm-input-uda-list-item bpm-readonly="bpmReadonly" input-type="text" ng-if="udaDataItemRef.type!='+"'BOOLEAN'" +'&& udaDataItemRef.type!='+"'DATE'"+' && udaDataItemRef.type!=' +
            "'INTEGER'"+' && udaDataItemRef.type!='+"'LONG'"+'" value="value" identifier="{{udaDataItemRef.identifier}}" label="{{udaDataItemRef.name}}"></bpm-input-uda-list-item>' +
            '<bpm-input-uda-list-item bpm-readonly="bpmReadonly" input-type="number" ng-if="udaDataItemRef.type=='+"'INTEGER'"+' || udaDataItemRef.type=='+"'LONG'"+'" value="value" identifier="{{udaDataItemRef.identifier}}" label="{{udaDataItemRef.name}}"></bpm-input-uda-list-item>' +
            '<div class="checkbox col-sm-offset-3 col-sm-9" ng-if="udaDataItemRef.type=='+"'BOOLEAN'"+'">' +
            '<label><input ng-model="value" type="checkbox" ng-disabled="bpmReadonly">{{udaDataItemRef.name}}</label></div>' +
            '<bpm-date-uda-list-item bpm-readonly="bpmReadonly" ng-if="udaDataItemRef.type=='+"'DATE'"+'" value="value" uda-data-item-ref="udaDataItemRef"></bpm-date-uda-list-item>',
            scope: {
                "value": "=",
                "udaDataItemRef": "=",
                "bpmReadonly" : "=?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmUdaListCtrl
    ///////////////////////////////////
    bpmUdaListCtrl.$inject = ['$scope'];
    function bpmUdaListCtrl($scope) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER

        // functions

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
        // if the object is a Process Definition this formats it as a Process Instance so it can be started.
        function formatAsPi() {
            if (vm.bpmObject && !vm.bpmObject.pi){
                var udas={};
                    // We have a PD
                // UDAs and their default values are in dataItemRefs
                vm.bpmObject.pd.dataItemRefs.forEach(
                    function(dataItemRef) {
                        console.log("dataItemRef",dataItemRef);
                        var initValue= dataItemRef.initValue;
                        if (dataItemRef.type=="INTEGER" || dataItemRef.type=="DATE" || dataItemRef.type=="FLOAT" || dataItemRef.type=="LONG" || dataItemRef.type=="BIGDECIMAL"){
                            initValue= Number(dataItemRef.initValue);
                        }
                        if (dataItemRef.type=="BOOLEAN"){
                            initValue= (dataItemRef.initValue=="true");
                        }
                        udas[dataItemRef.identifier]= initValue;
                    }
                );
                vm.bpmObject.pi={
                    uda: udas
                };

            }
        }

        initialize();
        function initialize() {
            console.log("WARNING: bpm-uda-list is deprecated. Please use bpm-work-item-uda-list or bpm-process-instance-uda-list or bpm-start-process-uda-list")
            // Watch for the bpm-object to be loaded in the control.
            $scope.$watch('vm.bpmObject',formatAsPi);
        }
    }


    ///////////////////////////////////
    // bpmUdaList
    // bpm-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Work Item or Process Instance or even a Process Definition
    // This should be used for displaying UDAs in a single Work Item or Process Instance view.
    // For displaying UDAs in a work list see work-item-display-work-list-udas
    // It determines a Process Definition as it will only have the PD section.
    function bpmUdaList() {
        var directive = {
            restrict: 'E',
            template: '<form class="form-horizontal">' +
            '<div class="container-fluid form-group" ng-repeat="uda in vm.bpmObject.pd.dataItemRefs" ng-hide="uda.name.startsWith('+
            "'__'"+') || uda.name.startsWith('+"'_'"+')">' +
            '<bpm-old-uda-list-item bpm-object="vm.bpmObject" uda-data-item-ref="uda"></bpm-old-uda-list-item>' +
            '</div></form>',
            controller : bpmUdaListCtrl,
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "bpmObject": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmGenericUdaListCtrl
    ///////////////////////////////////
//    bpmGenericUdaListCtrl.$inject = ['$scope'];
    function bpmGenericUdaListCtrl() {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER

        // functions
        vm.idInHideUda = idInHideUda;
        vm.idInProtectUda = idInProtectUda;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        // returns true if s is in the comma separated string commaString
        // this also trims the strings before comparing
        function isStringInCommaString(s, commaString){
            var stringArray= commaString.split(",");
            var theString= stringArray.find(
                function (element) {
                    var trimmedElement= element.trim();
                    return  trimmedElement== s;
                }
            );
            return Boolean(theString);
        }

        function isIdInExtAtt(id, extAttName){
            if (!vm.extendedAttArray){
                return false;
            }
            // Get the correct ExtendedAttribute from the set
            var extAttObject= vm.extendedAttArray.find(
                function (element) {
                    return (element.Name == extAttName);
                }
            );
            if (!extAttObject){
                return false;
            }
            return isStringInCommaString(id,extAttObject.Value);
        }

        function idInHideUda(id){
            return isIdInExtAtt(id,"HideUDA");
        }

        function idInProtectUda(id){
            return isIdInExtAtt(id,"ProtectUDA");
        }

        initialize();
        function initialize() {
        }
    }

    ///////////////////////////////////
    // bpmGenericUdaList
    // bpm-generic-uda-list
    ///////////////////////////////////
    // Directive to display UDAs for a Work Item or Process Instance or even a Process Definition
    // This should only be used as an internal component.
    // Parameters:
    //      dataItemRefs (input): the list of data item refs from pd.dataItemRefs
    //      extendedAttributes (input): Extended Attributes list to use for HideUDA and ProtectUDA
    //      udaValues (input/output): Object with id-value pairs of UDAs
    function bpmGenericUdaList() {
        var directive = {
            restrict: 'E',
            template: '<form class="form-horizontal container-fluid">' +
            '<span ng-repeat="uda in vm.udas" ng-hide="uda.name.startsWith('+
            "'__'"+') || uda.name.startsWith('+"'_'"+') || vm.idInHideUda(uda.identifier)">' +
            '<div class="form-group">' +
            '<bpm-uda-list-item bpm-readonly="vm.idInProtectUda(uda.identifier)" value="vm.udaValues[uda.identifier]" uda-data-item-ref="uda"></bpm-uda-list-item>' +
            '</div>' +
            '</span>' +
            '</form>',
            controllerAs: 'vm',
            bindToController: true,
            controller : bpmGenericUdaListCtrl,
            scope: {
                "extendedAttArray": "<?",
                "udas": "<",
                "udaValues": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmInputUdaListItemCtrl
    ///////////////////////////////////
    bpmInputUdaListItemCtrl.$inject = ['$scope'];
    function bpmInputUdaListItemCtrl($scope) {
        var vm = this;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function convertTextToNumber() {
            if (vm.inputType == "number"){
                vm.value= Number(vm.value);
            }
        }
        initialize();
        function initialize() {
            // Watch for the list of groups to be loaded in the control.
            $scope.$watch('vm.value',convertTextToNumber);
        }
    }

    ///////////////////////////////////
    // bpmInputUdaListItem
    // bpm-input-uda-list-item
    ///////////////////////////////////
    // Directive to display a UDA as an <input>
    // This should be used for displaying UDAs in a work item or process instance view.
    // Since this formats the UDA value as a string it should typically be used for STRING or number UDAs.
    // For displaying UDAs in a work list see work-item-display-work-list-udas
    // You can typically get value from here: bpmObject.pi.uda[identifier]
    function bpmInputUdaListItem() {
        var directive = {
            restrict: 'E',
            template:   '<div class="form-group">'+
            '<label class="col-sm-3 control-label">{{vm.label}}</label>'+
            '<div class="col-sm-9">' +
            '<input ng-hide="vm.bpmReadonly" type="{{vm.inputType}}" class="form-control" ng-model="vm.value">' +
            '<p class="form-control-static" ng-show="vm.bpmReadonly">{{vm.value}}</p>'+
            '</div></div>',
            controller: 'bpmInputUdaListItemCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "identifier": "@",
                "label": "@",
                "inputType": "@",
                "bpmReadonly" : "=?",
                "value": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmOldInputUdaListItem
    // bpm-old-input-uda-list-item
    ///////////////////////////////////
    // Directive to display a UDA as an <input>
    // This should be used for displaying UDAs in a work item or process instance view.
    // Since this formats the UDA value as a string it should typically be used for STRING or number UDAs.
    // For displaying UDAs in a work list see work-item-display-work-list-udas

    function bpmOldInputUdaListItem() {
        var directive = {
            restrict: 'EA',
            template:   '<div class="form-group">'+
                        '<label class="col-sm-3 control-label">{{label}}</label>'+
                        '<div class="col-sm-9"><input type="{{inputType}}" class="form-control" ng-model="bpmObject.pi.uda[identifier]"></div>'+
                        '</div>',
            scope: {
                "identifier": "@",
                "label": "@",
                "inputType": "@",
                "bpmObject": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmDateUdaListItemCtrl
    ///////////////////////////////////
    function bpmDateUdaListItemCtrl() {
        var vm = this;
        // functions
        vm.convertDateToLong=convertDateToLong;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function convertDateToLong() {
            vm.value= new Date(vm.value).getTime();
        }
    }

    ///////////////////////////////////
    // bpmDateUdaListItem
    // bpm-date-uda-list-item
    ///////////////////////////////////
    // Directive to display a UDA as a date
    // This should be used for displaying UDAs in a work item or Process Instance view.
    // Since this formats the UDA value as a date it should typically be used for DATE UDAs.
    // For displaying UDAs in a work list see work-item-display-work-list-udas
    // value comes from here: vm.bpmObject.pi.uda[vm.udaDataItemRef.identifier]
    function bpmDateUdaListItem() {
        var directive = {
            restrict: 'EA',
            template:   '<div class="form-group">' +
            '<label class="col-sm-3 control-label">{{vm.udaDataItemRef.name}}</label>' +
            '<div class="col-sm-9">' +
            '<p class="form-control-static" ng-show="vm.bpmReadonly">{{vm.value | date : ' +
            "'yyyy-MM-dd'" +
            '}}</p>'+
            '<div class="input-group" ng-hide="vm.bpmReadonly">' +
            '<input class="form-control" ng-change="vm.convertDateToLong()" type="text" placeholder="yyyy-mm-dd" ng-model="vm.value" uib-datepicker-popup="yyyy-MM-dd" is-open="fdate_opened" ng-required="false">' +
            '<span class="input-group-btn">' +
            '<button class="btn btn-default" ng-click="fdate_opened=true">' +
            '<span class="glyphicon glyphicon-calendar"></span>' +
            '</button></span>' +
            '</div></div></div>',
            controller: 'bpmDateUdaListItemCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "value": "=",
                "bpmReadonly" : "=?",
                "udaDataItemRef": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmOldDateUdaListItemCtrl
    ///////////////////////////////////
    function bpmOldDateUdaListItemCtrl() {
        var vm = this;
        // functions
        vm.convertDateToLong=convertDateToLong;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function convertDateToLong() {
            vm.bpmObject.pi.uda[vm.udaDataItemRef.identifier]= new Date(vm.bpmObject.pi.uda[vm.udaDataItemRef.identifier]).getTime();
        }
    }

    ///////////////////////////////////
    // bpmOldDateUdaListItem
    // bpm-old-date-uda-list-item
    ///////////////////////////////////
    // Directive to display a UDA as a date
    // This should be used for displaying UDAs in a work item or Process Instance view.
    // Since this formats the UDA value as a date it should typically be used for DATE UDAs.
    // For displaying UDAs in a work list see work-item-display-work-list-udas

    function bpmOldDateUdaListItem() {
        var directive = {
            restrict: 'EA',
            template:   '<div class="form-group">' +
            '<label class="col-sm-3 control-label">{{vm.udaDataItemRef.name}}</label>' +
            '<div class="col-sm-9"><div class="input-group">' +
            '<input class="form-control" ng-change="vm.convertDateToLong()" type="text" placeholder="yyyy-mm-dd" ng-model="vm.bpmObject.pi.uda[vm.udaDataItemRef.identifier]" uib-datepicker-popup="yyyy-MM-dd" is-open="fdate_opened" ng-required="false">' +
            '<span class="input-group-btn">' +
            '<button class="btn btn-default" ng-click="fdate_opened=true">' +
            '<span class="glyphicon glyphicon-calendar"></span>' +
            '</button></span></div></div></div>',
            controller: 'bpmOldDateUdaListItemCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "bpmObject": "=",
                "udaDataItemRef": "="
            }
        };
        return directive;
    }


    /////////////////////////////////////////
    //  Comment Directives
    /////////////////////////////////////////

    angular
        .module('bpm-angular')
        .controller('bpmProcessInstanceCommentCtrl', bpmProcessInstanceCommentCtrl)
        .directive('bpmAddComment', bpmAddComment)
        .directive('bpmComment', bpmComment)
        .directive('bpmCommentList', bpmCommentList)
    ;

    ///////////////////////////////////
    // bpmProcessInstanceCommentCtrl
    ///////////////////////////////////
    bpmProcessInstanceCommentCtrl.$inject = ['$scope','BPM'];
    function bpmProcessInstanceCommentCtrl($scope,BPM) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER
        //The pi part of the full process instance object
        vm.updating=false;

        // functions
        vm.addComment=addComment;
        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function addComment() {
            // Check first if the comment text is blank.
            if (!vm.comment || vm.comment.trim()=="") {
                return;
            }
            vm.updating=true;
            BPM
                .addComment(vm.pi,vm.comment)
                .success(
                    function (data, status) {
                        vm.updating=false;
                        // We can only overwrite the pi part, as someone might be using this with a workitem
                        vm.pi= data.pi;
                    }
                )
                .error(
                    function (data, status) {
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
            vm.comment= null;
        }
    }

    ///////////////////////////////////
    // bpmAddComment
    // bpm-add-comment
    ///////////////////////////////////
    // Directive to display a box where you can add a new Process Instance Comment
    // This has functionality to add a new comment
    function bpmAddComment() {
        var directive = {
            restrict: 'EA',
            template: '<h3 ng-show="vm.updating">{{"Updating" | translate}}</h3>' +
            '<div class="container-fluid row" ng-hide="vm.updating">' +
            '<textarea class="col-xs-10" form-control rows="2" ng-attr-placeholder="{{' +
            "'comments.addComment'"+' | translate}}" ng-model="vm.comment"></textarea>' +
            '<button ng-click="vm.addComment()" type="button" class="btn btn-primary" aria-label="{{' +
            "'comments.addComment'"+' | translate}}">' +
            '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></div>',
            controller: 'bpmProcessInstanceCommentCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "pi": "="
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmComment
    // bpm-comment
    ///////////////////////////////////
    // Directive to display an individual Process Instance Comment
    // NOTE: this is used by bpm-comment-list
    function bpmComment() {
        var directive = {
            restrict: 'EA',
            template: '<div class="media-left media-middle">' +
            '<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 48px; color: #006999"></span></div>' +
            '<div class="media-body">' +
            '<span class="panel-body">{{comment.message}}</span>' +
            '<small><span>{{"time.agoPrefix" | translate}}</span>' +
            '<span><bpm-time-as-words-from-now time-in-millis="{{comment.timeStamp}}"></bpm-time-as-words-from-now></span>' +
            '<span> </span><span>{{"time.agoSuffix" | translate}}</span><span> </span>' +
            '<span>{{"time.by" | translate}}</span> {{comment.userId}}</small></div>',
            scope: {
                "comment": "="
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmCommentList
    // bpm-comment-list
    ///////////////////////////////////
    // Directive to display list of Process Instance Comments
    // NOTE: this only displays the comments in the pi object and doesn't fetch them
    function bpmCommentList() {
        var directive = {
            restrict: 'EA',
            template: '<ul class="media-list">' +
            '<li class="media" ng-repeat="comment in pi.comments | orderBy:' +
            "'-timeStamp'"+'">' +
            '<bpm-comment comment="comment"></bpm-comment></li></ul>',
            scope: {
                "pi": "="
            }
        };
        return directive;
    }



    ////////////////////////////////////////
    //  User and Group Directives
    ////////////////////////////////////////

    angular
        .module('bpm-angular')
        .controller('bpmUserSelectorCtrl', bpmUserSelectorCtrl)
        .directive('bpmUserSelector', bpmUserSelector)
    ;

    ///////////////////////////////////
    // bpmUserSelectorCtrl
    ///////////////////////////////////
    bpmUserSelectorCtrl.$inject = ['$scope','BPMAdmin'];
    function bpmUserSelectorCtrl($scope,BPMAdmin) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        // we use this list so we can add a selectedField to each group
        vm.formattedGroupList= [];
        vm.userList= [];
        vm.selectedUsers= [];
        vm.selectedGroup= null;

        // functions
        vm.userIsSelected= userIsSelected;
        vm.groupClicked= groupClicked;
        vm.userClicked= userClicked;
        vm.selectedUserClicked= selectedUserClicked;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        // Returns true when the user is in vm.selectedUsers
        function userIsSelected(user)
        {
            var selectedUser=vm.selectedUsers.find(function(e) {return e.name == user.name;});
            return (selectedUser!==undefined && selectedUser.selected);
        }

        // When a group is clicked we display the group members in the User list.
        function groupClicked(group)
        {
            if (vm.selectedGroup!==null){
                vm.selectedGroup.selected= false;
            }
            vm.selectedGroup= group;
            group.selected=true;
            BPMAdmin
                .getGroupMembers(group.name)
                .success(
                    function (data, status) {
                        data.members.forEach(
                            function(element, index, list){
                                list[index].selected=vm.userIsSelected(list[index]);
                            }
                        );
                        vm.userList=data.members;
                    }
                )
                .error(
                    function (data, status) {
                        $scope.$emit('bpmErrorEvent', status);
                    }
                )
            ;
            $scope.$emit('GroupClickedEvent', group);
        }

        // When a user is clicked we add it to the list of selected users.
        function userClicked(user)
        {
            if (!user.selected){
                vm.selectedUsers.push(user);
                user.selected=true;
                $scope.$emit('UserClickedEvent', user);
            }
        }

        // When a selected user is clicked we remove it from the list of selected users.
        function selectedUserClicked(selectedUser)
        {
            var user=vm.userList.find(function(e){return e.name == selectedUser.name;});
            if (user !== undefined){
                user.selected=false;
            }
            var selectedUserIndex=vm.selectedUsers.findIndex(function (e){return e.name == selectedUser.name;});
            vm.selectedUsers.splice(selectedUserIndex,1);
            $scope.$emit('SelectedUserClickedEvent', selectedUser);

        }

        function formatGroupArray(){
            vm.formattedGroupList= vm.groupList.map(
                function(groupName){
                    return {"name" : groupName, "selected" : false};
                }
            );
        }

        initialize();
        function initialize() {
            // Watch for the list of groups to be loaded in the control.
            $scope.$watch('vm.groupList',formatGroupArray);
        }
    }

    ///////////////////////////////////
    // bpmUserSelector
    // bpm-user-selector
    // events:
    // User clicked: $scope.$emit('UserClickedEvent', user);
    ///////////////////////////////////
    // Directive to display list of users that can be selected
    function bpmUserSelector() {
        var directive = {
            restrict: 'EA',
            template: '<div class="row" style="overflow: hidden"><div class="col-sm-4"><div class="input-group">' +
            '<span class="input-group-addon" id="groupSearchId">{{"directory.groups" | translate}}</span>' +
            '<input type="text" ng-model="vm.groupSearch" class="form-control" placeholder="{{ ' + "'directory.filterGroups'" +' | translate }}" aria-describedby="groupSearchId"></div>' +
            '<ul class="list-group" style="max-height: 500px; overflow-y: auto">' +
            '<li ng-class="{active: group.selected == true}" ng-repeat="group in vm.formattedGroupList | filter:vm.groupSearch" class="list-group-item" ng-click="vm.groupClicked(group)">{{group.name}}<span class="pull-right glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>' +
            '</ul></div><div class="col-sm-4"><div class="input-group">' +
            '<span class="input-group-addon" id="userSearchId">{{"directory.users" | translate}}</span>' +
            '<input type="text" ng-model="vm.userSearch" class="form-control" placeholder="{{' +"'directory.filterUsers'" +' | translate}}" aria-describedby="userSearchId">' +
            '</div><ul class="list-group" style="max-height: 500px; overflow-y: auto">' +
            '<li ng-class="{disabled: user.selected == true}" ng-repeat="user in vm.userList | filter:vm.userSearch" class="list-group-item" ng-click="vm.userClicked(user)">{{user.name}}<span ng-show="user.selected == false" class="pull-right glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li></ul></div>' +
            '<div class="col-sm-4"><div class="input-group"><span class="input-group-addon" id="selectedSearchId">{{"selected" | translate}}</span>' +
            '<input type="text" ng-model="vm.selectedSearch" class="form-control" placeholder="{{' +
            "'directory.filterUsers'" + ' | translate}}" aria-describedby="selectedSearchId"></div>' +
            '<ul class="list-group" style="max-height: 500px; overflow-y: auto">' +
            '<li ng-repeat="selectedUser in vm.selectedUsers  | filter:vm.selectedSearch" class="list-group-item" ng-click="vm.selectedUserClicked(selectedUser)"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> {{ selectedUser.name}}</li></ul></div></div>',
            controller: 'bpmUserSelectorCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "selectedUsers": "=",
                "groupList": "="
            }
        };
        return directive;
    }



    ////////////////////////////////////////
    //  Time Directives
    ////////////////////////////////////////

    angular
        .module('bpm-angular')
        .directive('bpmTimeAsWords', bpmTimeAsWords)
        .directive('bpmTimeAsWordsFromNow', bpmTimeAsWordsFromNow)
        .directive('bpmDueDateSelector', bpmDueDateSelector)
        .controller('bpmDueDateProgressBarCtrl', bpmDueDateProgressBarCtrl)
        .directive('bpmDueDateProgressBar', bpmDueDateProgressBar)
    ;

    var bpmTimeAsWordsHtmlTemplate= '<span ng-show="jsonTime.days>=1">{{jsonTime.days}}</span> <span ng-show="jsonTime.days>1">{{"time.days" | translate}} </span><span ng-show="jsonTime.days===1">{{"time.day" | translate}} </span>' +
        '<span ng-show="jsonTime.days<=1">' +
        '<span ng-show="jsonTime.hours>=1">{{jsonTime.hours}}</span> <span ng-show="jsonTime.hours>1">{{"time.hours" | translate}} </span><span ng-show="jsonTime.hours===1">{{"time.hour" | translate}} </span></span>' +
        '<span ng-show="jsonTime.days===0 && jsonTime.hours<=1">' +
        '<span ng-show="jsonTime.minutes>=1">{{jsonTime.minutes}}</span> <span ng-show="jsonTime.minutes>1">{{"time.minutes" | translate}} </span><span ng-show="jsonTime.minutes===1">{{"time.minute" | translate}} </span></span>';



    ///////////////////////////////////
    // bpmTimeAsWords Directive
    // bpm-time-as-words
    // Directive for showing how long in words is the given time stamp.
    ///////////////////////////////////
    bpmTimeAsWords.$inject = ['TimeFactory'];
    function bpmTimeAsWords(TimeFactory) {
        var directive = {
            restrict: 'EA',
            template: bpmTimeAsWordsHtmlTemplate,
            scope: {
                timeInMillis: '@'
            },
            controller: ['$scope','$timeout', function ($scope,$timeout) {
                // We need to watch timeInMillis for when it has a real value and update
                $scope.$watch('timeInMillis',
                    function () {
                        // Verify that timeInMillis is a number, otherwise warn the developer.
                        if (isNaN($scope.timeInMillis)){
                            console.log("ERROR (bpm-time-as-words): time-in-millis should be a number, received:",$scope.timeInMillis);
                        }
                        else if ($scope.timeInMillis){
                            $scope.jsonTime= TimeFactory.convertTimeStampToJSON($scope.timeInMillis);
                        }
                    }
                );
            }]
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmTimeAsWordsFromNow Directive
    // bpm-time-as-words-from-now
    // Directive for showing how long ago or in the future something happened.
    ///////////////////////////////////
    bpmTimeAsWordsFromNow.$inject = ['TimeFactory'];
    function bpmTimeAsWordsFromNow(TimeFactory) {
        var directive = {
            restrict: 'EA',
            template: bpmTimeAsWordsHtmlTemplate,
            scope: {
                timeInMillis: '@'
            },
            link: function($scope, element, attrs) {
                // We need to watch timeInMillis for when it has a real value and update
                $scope.$watch('timeInMillis',
                    function () {
                        // Verify that timeInMillis is a number, otherwise warn the developer.
                        if (isNaN($scope.timeInMillis)){
                            console.log("ERROR (bpm-time-as-words-from-now): time-in-millis should be a number, received:",$scope.timeInMillis);
                        }
                        else if ($scope.timeInMillis){
                            $scope.jsonTime= TimeFactory.convertTimeStampToJSON(TimeFactory.timeDifference($scope.timeInMillis));
                        }
                    }
                );
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmDueDateSelector
    // bpm-due-date-selector
    // events:
    ///////////////////////////////////
    // Directive to display a drop down where user can select a due date.
    function bpmDueDateSelector() {
        var directive = {
            restrict: 'EA',
            template: '<div class="input-group">' +
            '<input class="form-control" type="text" placeholder="{{' +"'time.dueDate'" +
            ' | translate}}" ng-model="dueDate" uib-datepicker-popup="yyyy-MM-dd" is-open="tdate_opened" ng-required="false" />' +
            '<span class="input-group-btn"><button class="btn btn-default" ng-click="tdate_opened=!tdate_opened">' +
            '<span class="glyphicon glyphicon-calendar"></span></button></span></div>',
            scope: {
                "dueDate": "="
            }
        };
        return directive;
    }


    bpmDueDateProgressBarCtrl.$inject = ['$scope'];
    function bpmDueDateProgressBarCtrl($scope) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER
        vm.progressValue=null;

        function computeProgressValue()
        {
            // Verify that timeInMillis is a number, otherwise warn the developer.
            if (isNaN(vm.dueDateTimeStamp)||isNaN(vm.createdTimeStamp)){
                console.log("ERROR (bpm-due-date-progress-bar): due-date-time-stamp and created-time-stamp should be a number, received:",vm.dueDateTimeStamp,vm.createdTimeStamp);
            }

            var currentTime= Date.now();
            vm.progressValue=((currentTime-vm.createdTimeStamp)/(vm.dueDateTimeStamp-vm.createdTimeStamp))*100;
            vm.progressType="danger";
            // This takes care of the strange case where the creation date is after the due date.
            if (vm.progressValue<0){
                vm.progressValue=100;
            }
            // Set the color of the progress bar based on the percentage complete.
            if (vm.progressValue<50){
                vm.progressType="success";
            }
            else if (vm.progressValue<90){
                vm.progressType="warning";
            }
        }

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
        initialize();
        function initialize() {
            // We need to set up the watch as the WorkItem will be typically updated asynchronously.
            $scope.$watch('vm.dueDateTimeStamp',computeProgressValue);
        }
    }

    ///////////////////////////////////
    // bpmDueDateProgressBar
    // bpm-due-date-progress-bar
    ///////////////////////////////////
    // displays a progress bar based on the due date.
    function bpmDueDateProgressBar() {
        var directive = {
            restrict: 'EA',
            template: '<uib-progressbar value="vm.progressValue" ng-attr-type="{{vm.progressType}}" style="height: 5px"></uib-progressbar>',
            controller: 'bpmDueDateProgressBarCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "createdTimeStamp": "@",
                "dueDateTimeStamp": "@"
            }
        };
        return directive;
    }




    ///////////////////////////////////
    //     i18n
    //     Internationalization
    ///////////////////////////////////


    bpmAngularUiModule.config(['$translateProvider', function($translateProvider) {

        var bpm_fr={
            "piTable" :{
                "closedTime": "Date de completion",
                "creationTime": "Date de création",
                "dueDate": "Écheance",
                "description": "Description",
                "idDef": "Plan id",
                "idInst": "id",
                "initiator": "Initiator",
                "name": "Nom",
                "owners": "Owners",
                "parentProcess": "Parent",
                "priority": "Priorité",
                "sequence": "Sequence",
                "state": "État",
                "title": "Title",
                "type": "Type"
            },
            "wiTable" : {
                "application": "Application",
                "assignee": "Assignee",
                "choices": "Choices",
                "creationTime": "Date de création",
                "dueDate": "Écheance",
                "forms" : "Forms",
                "id": "id",
                "instanceDescription": "Description",
                "instanceId": "Process id",
                "instanceName": "Process",
                "instanceOwners": "Process Owners",
                "instanceTitle": "Process Title",
                "name": "Nom",
                "nodeInstanceId": "Activity id",
                "planDescription": "Plan Description",
                "planIdentifier": "Plan id",
                "planName": "Plan",
                "planTitle": "Plan Title",
                "priority": "Priorité",
                "state": "État",
                "subProcessId": ""
            },
            "pdTable" :{
                "application": "Application",
                "desc": "Description",
                "id": "id",
                "name": "Nom",
                "owner": "Owners",
                "priority": "Priorité",
                "state": "État",
                "version": "Version"
            },
            "workItemFilter" : {
                "MyCurrentWorkItems" : "Mes tâches courantes",
                "MyWorkItems"        : "Toutes mes tâches",
                "MyActiveWorkItems"  : "Mes tâches actives",
                "MyAcceptedWorkItems": "Mes tâches acceptées"
            },
            "processInstanceFilter" : {
                "AllActiveProcesses" : "All Active Processes",
                "MyProcesses"        : "My Processes",
                "AllProcesses"  : "All Processes",
                "MyActiveProcesses"  : "My Active Processes",
                "AllInactiveProcesses"  : "All Inactive Processes",
                "MyInactiveProcesses"  : "My Inactive Processes",
                "AllProcessesInErrorState": "All Processes In Error State"
            },
            "processDefinitionFilter" : {
                "AllPlans" : "All Plans",
                "MyPlans"  : "Mes Plans"
            },
            "workItemState": {
                "state": "État",
                "0" : "Pas débuté",
                "3" : "Actif",
                "4" : "Inactif",
                "5" : "Complété",
                "6" : "Décliné",
                "8" : "Accepté",
                "12": "En attente",
                "14": "Suspendu"
            },
            "processInstanceState" : {
                "1" : "Actif",
                "2" : "Fermé",
                "7" : "Actif",
                "8" : "Complété",
                "10" : "Erreur",
                "11" : "Interrompu",
                "12" : "Suspendu"
            },
            "nodeInstanceState" : {
                "state": "State",
                "0" : "Initial",
                "3" : "Running",
                "4" : "Waiting",
                "5" : "Complété",
                "6" : "Aborted",
                "10" : "Error",
                "11" : "Suspended"
            },
            "processDefinitionState" : {
                "0" : "Draft",
                "1" : "Published",
                "2" : "Private",
                "3" : "Obsolete"
            },
            "applicationState" : {
                "0" : "Initial",
                "1" : "Stopped",
                "2" : "Started",
                "3" : "Deleted"
            },
            "time" : {
                "creationDate"  : "Date de création",
                "closedTime"  : "Date de completion",
                "day"   	    : "jour",
                "days"   	    : "jours",
                "hour"   	    : "heure",
                "hours"   	    : "heures",
                "minute"   	    : "minute",
                "minutes"       : "minutes",
                "agoPrefix"     : "il y a",
                "agoSuffix"     : "",
                "inPrefix"      : "dans",
                "inSuffix"      : "",
                "dueInPrefix"   : "Dû dans",
                "dueInSuffix"   : "",
                "dueAgoPrefix"  : "Échu depuis",
                "dueAgoSuffix"  : "",
                "Due"           : "Échéance",
                "dueDate"       : "Échéance",
                "by"            : "par"
            },
            "comments":{
                "Comment"   	: "Commentaire",
                "Comments"   	: "Commentaires",
                "addComment"    : "Ajouter un commentaire"
            },
            "priority": {
                "priority": "Priorité",
                "undefined" : "Non définie",
                "low" : "Basse",
                "medium" : "Moyenne",
                "high" : "Haute"
            },
            "directory":{
                "group"     :   "Group",
                "groups"    :   "Groups",
                "filterGroups" : "Filter groups",
                "user"      : "User",
                "users"     : "Users",
                "filterUsers" : "Filter users"
            },
            "identification": {
                "identification": "Identification",
                "systemeSelectionner": "Sélectionner un système",
                "g1option": "G1 - Entreprise / Particulier en affaires",
                "g3option": "G3 - Particuliers",
                "g4option": "G4 - Représentants"
            },
            "placeHolder": { //TODO: A eliminer une fois le module de recherche dans g-search.js sera activé.
                "appNo": "No app",
                "codePostal1": "X9X",
                "codePostal2": "9X9",
                "division": "Division",
                "nas": "Numéro d’assurance sociale",
                "nasConjoin": "NAS conjoint",
                "neq": "Numéro d’entreprise du Québec",
                "noIntervenant": "Numéro d'intervenant",
                "nom": "Nom",
                "noRepresentant": "Numéro représentant professionnel",
                "noUsager": "Numéro usager",
                "prenom": "Prénom",
                "ted": "Numéro TED",
                "telephone": "(___)___-____"
            },
            "bpm":{
                "id"          : "id",
                "task"          : "Tâche",
                "taskName"          : "Tâches nom",
                "addSubTask"  : "Nouveau sous tâche",
                "subTask"  : "Sous tâche",
                "subTasks"  : "Sous tâches",
                "instance"   : "Cas",
                "instances"   : "Cas",
                "plan"   : "Plan",
                "plans"   : "Plans",
                "accept"    :   "Accepter",
                "decline"    :   "Refuser",
                "reassign"    :   "Réassigner",
                "initiator"    :   "Initiator",
                "owner"    :   "Owner",
                "version"    :   "Version"
            },
            "selected"      : "Selected",
            "name"          : "Nom",
            "description"   : "Description",
            "ok" : "OK",
            "cancel" : "Annuler"
        };

        var bpm_en= {
            "piTable" :{
                "closedTime": "Closed",
                "creationTime": "Created",
                "description": "Description",
                "idDef": "Plan id",
                "idInst": "id",
                "initiator": "Initiator",
                "name": "Name",
                "owners": "Owners",
                "parentProcess": "Parent",
                "priority": "Priority",
                "sequence": "Sequence",
                "state": "State",
                "title": "Title",
                "type": "Type"
            },
            "wiTable" : {
                "application": "Application",
                "assignee": "Assignee",
                "choices": "Choices",
                "creationTime": "Created",
                "dueDate": "Due",
                "forms" : "Forms",
                "id": "id",
                "instanceDescription": "Description",
                "instanceId": "Process id",
                "instanceName": "Process",
                "instanceOwners": "Process Owners",
                "instanceTitle": "Process Title",
                "name": "Name",
                "nodeInstanceId": "Activity id",
                "planDescription": "Plan Description",
                "planIdentifier": "Plan id",
                "planName": "Plan",
                "planTitle": "Plan Title",
                "priority": "Priority",
                "state": "State",
                "subProcessId": ""
            },
            "pdTable" :{
                "application": "Application",
                "desc": "Description",
                "id": "id",
                "name": "Name",
                "owner": "Owners",
                "priority": "Priority",
                "state": "State",
                "version": "Version"
            },
            "workItemFilter" : {
                "MyCurrentWorkItems" : "My Current Tasks",
                "MyWorkItems"        : "All My Tasks",
                "MyActiveWorkItems"  : "My Active Tasks",
                "MyAcceptedWorkItems": "My Accepted Work Items"
            },
            "processInstanceFilter" : {
                "AllActiveProcesses" : "All Active Processes",
                "MyProcesses"        : "My Processes",
                "AllProcesses"  : "All Processes",
                "MyActiveProcesses"  : "My Active Processes",
                "AllInactiveProcesses"  : "All Inactive Processes",
                "MyInactiveProcesses"  : "My Inactive Processes",
                "AllProcessesInErrorState": "All Processes In Error State"
            },
            "processDefinitionFilter" : {
                "AllPlans" : "All Plans",
                "MyPlans"  : "My Plans"
            },
            "workItemState": {
                "state": "State",
                "0" : "Not Started",
                "3" : "Active",
                "4" : "Inactive",
                "5" : "Completed",
                "6" : "Declined",
                "8" : "Accepted",
                "12": "Waiting",
                "14": "Supspended"
            },
            "processInstanceState" : {
                "state": "State",
                "1" : "Running",
                "2" : "Closed",
                "7" : "Running",
                "8" : "Closed",
                "10" : "Error",
                "11" : "Aborted",
                "12" : "Suspended"
            },
            "nodeInstanceState" : {
                "state": "State",
                "0" : "Initial",
                "3" : "Running",
                "4" : "Waiting",
                "5" : "Completed",
                "6" : "Aborted",
                "10" : "Error",
                "11" : "Suspended"
            },
            "processDefinitionState" : {
                "0" : "Draft",
                "1" : "Published",
                "2" : "Private",
                "3" : "Obsolete"
            },
            "applicationState" : {
                "0" : "Initial",
                "1" : "Stopped",
                "2" : "Started",
                "3" : "Deleted"
            },
            "time" : {
                "creationDate"  : "Creation time",
                "closedTime"    : "Completion time",
                "day"   	    : "day",
                "days"   	    : "days",
                "hour"   	    : "hour",
                "hours"   	    : "hours",
                "minute"   	    : "minute",
                "minutes"       : "minutes",
                "agoPrefix"   	: "",
                "agoSuffix"   	: "ago",
                "inPrefix"   	: "in",
                "inSuffix"   	: "",
                "dueInPrefix"   : "Due in",
                "dueInSuffix"   : "",
                "dueAgoPrefix"  : "Due",
                "dueAgoSuffix"  : "ago",
                "Due"           : "Due",
                "dueDate"       : "Due Date",
                "by"            : "by"
            },
            "comments":{
                "Comment"   	: "Comment",
                "Comments"   	: "Comments",
                "addComment"    : "Add comment"
            },
            "priority": {
                "priority": "Priority",
                "undefined" : "Undefined",
                "low" : "Low",
                "medium" : "Medium",
                "high" : "High"
            },
            "directory":{
                "group"     :   "Group",
                "groups"    :   "Groups",
                "filterGroups" : "Filter groups",
                "user"      : "User",
                "users"     : "Users",
                "filterUsers" : "Filter users"
            },
            "bpm":{
                "id"          : "id",
                "task"          : "Task",
                "taskName"      : "Task name",
                "addSubTask"    : "Add Sub-task",
                "subTask"  : "Sub-task",
                "subTasks"  : "Sub-tasks",
                "instance"   : "Process",
                "instanceName"   : "Process name",
                "instances"   : "Processes",
                "plan"   : "Plan",
                "plans"   : "Plans",
                "accept"    :   "Accept",
                "decline"    :   "Decline",
                "reassign"    :   "Reassign",
                "initiator"    :   "Initiator",
                "owner"    :   "Owner",
                "version"    :   "Version"
            },
            "selected"      : "Selected",
            "name"          : "Name",
            "description"   : "Description",
            "ok" : "OK",
            "cancel" : "Cancel"
        };

        $translateProvider
            .translations('en', bpm_en)
            .translations('fr', bpm_fr)
            .preferredLanguage('en')
            .fallbackLanguage('en')
            .useSanitizeValueStrategy('escape')
        ;
    }]);



    // Monkey patch String prototype to add startsWith as we use it in many places.
        if (!String.prototype.startsWith) {
            String.prototype.startsWith = function(searchString, position){
                position = position || 0;
                return this.substr(position, searchString.length) === searchString;
            };
        }
        // Monkey patch Array prototype to add find as we use it in many places.
        if (!Array.prototype.find) {
            Array.prototype.find = function(predicate) {
                'use strict';
                if (this == null) {
                    throw new TypeError('Array.prototype.find called on null or undefined');
                }
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }
                var list = Object(this);
                var length = list.length >>> 0;
                var thisArg = arguments[1];
                var value;

                for (var i = 0; i < length; i++) {
                    value = list[i];
                    if (predicate.call(thisArg, value, i, list)) {
                        return value;
                    }
                }
                return undefined;
            };
        }


})();
