/**
 * BPM Angular
 * Copyright Fujitsu 2016
 *
 * BPM Angular provides Angular services for fetching BPM objects from Agile Adapter.
 * Any User Interface code is in bpm-angular-ui.js
 *
 * BPM Angular directives all start with the prefix bpm-
 *
 *  BPMAdmin: provides methods at the Server, Tenant, and Application level.
 *  BPM
 *  BPMService: Utility functions for BPM
 *  TimeFactory : Methods for dealing with time.
 *  httpFactory: internal class wrapper for http GET, POST, PUT, and DELETE
 */


(function () {
    "use strict";

    var bpmAngularModule= angular.module('bpm-angular', []);


    ///////////////////////////////////
    // BPMAdmin
    ///////////////////////////////////

    // BPMAdmin is an AngularJS Factory that provides methods at the Server, Tenant, and Application level.
    angular
        .module('bpm-angular')
        .factory('BPMAdmin', BPMAdmin)
    ;

    BPMAdmin.$inject = ['httpFactory'];
    function BPMAdmin(httpFactory) {
        var factory = {
            initialize: initialize,
            // Fetch the list of tenants from the BPM Server
            getTenants : getTenants,
            getApplications : getApplications,
            getApplication : getApplication,
            appPublishPlans : appPublishPlans,
            encrypt : encrypt,
            getConnectorsConfiguration : getConnectorsConfiguration,
            setConnectorsConfiguration : setConnectorsConfiguration,
            appMakeReady : appMakeReady,
            appStart : appStart,
            appStop : appStop,
            // Directory Functions
            getGroupList: getGroupList,
            getGroupMembers: getGroupMembers,
            getUsersOfManager: getUsersOfManager,
            getManagersOfUser: getManagersOfUser
        };

        ////////////
        // IMPLEMENTATION
        return factory;

        /************************************************************************************
         * initialize
         * This is a the function for initializing the BPM Object
         * server should be a string like: http://interstagedemo:49950/aa/
         * server can also be "" and then it will use the local BPM server
         * This function adds the "api" to the URL.
         * It create a connection object like this
         {
             "server": "http://interstagedemo:49950/aa/",
                server can also be "" to use the localhost.
             "tenant": "Default",
             "application": "TestApp"
         },
         ************************************************************************************/
        function initialize(server)
        {
            // If the server is passed, make sure the server URL ends with a /
            var serverUrl= "";
            if (server){
                serverUrl= server.endsWith("/") ? server : server+"/";
            }
            serverUrl+="api";

            var bpmConnectionObject= {};
            bpmConnectionObject.server= serverUrl;

            factory.bpmConnection= bpmConnectionObject;
        }

        function getConnectionString()
        {
            // Verify that the Factory has been initialized
            if ((typeof factory.bpmConnection === "undefined")||(!factory.bpmConnection)){
                var errorMessage= "ERROR: BPMAdmin not initialized. BPMAdmin must be initialized before use. Use BPMAdmin.initialize()";
                console.log(errorMessage);
                throw errorMessage;
            }
            return factory.bpmConnection.server;
        }

        // api/Tenants
        function getTenants() {
            var url= getConnectionString()+"/Tenants";
            return (httpFactory.getRequest(url));
        }

        // api/t=tenant/Applications
        function getApplications(tenant) {
            var url = getConnectionString()+"/t="+tenant+"/Applications";
            return (httpFactory.getRequest(url));
        }

        // api/t=tenant/a=appName
        function getApplication(tenant, appName) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appName;
            return (httpFactory.getRequest(url));
        }

        function encrypt(plainText) {
            var url= getConnectionString()+"/Encrypt";
            var postData = {};
            postData.plaintext = plainText;
            return httpFactory.postJsonRequest(postData,url);
        }
        
        // api/t=tenant/a=appName/PublishPlans
        function appPublishPlans(tenant, appName) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appName+"/PublishPlans";
            return (httpFactory.getRequest(url));
        }
        
        // api/t=tenant/a=appId/ConnectorsConfig
        function getConnectorsConfiguration(tenant, appId) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appId + "/ConnectorsConfig";
            return (httpFactory.getRequest(url));
        }

        // POST api/t=tenant/a=appId/ConnectorsConfig
        function setConnectorsConfiguration(tenant, appId, newAppVars) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appId + "/ConnectorsConfig";
            return httpFactory.postJsonRequest(newAppVars,url);
        }

        // api/t=tenant/a=appName/MakeReady
        function appMakeReady(tenant, appName) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appName+"/MakeReady";
            return (httpFactory.getRequest(url));
        }

        // api/t=tenant/a=appName
        function appStart(tenant, appName) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appName;
            var postData= {};
            postData.state= 2;
            return httpFactory.postJsonRequest(postData,url);
        }

        // api/t=tenant/a=appName
        function appStop(tenant, appName) {
            var url = getConnectionString()+"/t="+tenant+"/a="+appName;
            var postData= {};
            postData.state= 1;
            return httpFactory.postJsonRequest(postData,url);
        }


        //    api/t={tenant}/Groups
        function getGroupList(tenant) {
            var url = getConnectionString()+"/t="+tenant+"/Groups";
            return httpFactory.getRequest(url);
        }

        //    api/t={tenant}/users/g={groupName}    returns members of the group
        function getGroupMembers(tenant, group) {
            var url= getConnectionString()+"/t="+tenant+'/users/g='+group;
            return httpFactory.getRequest(url);
        }

        //    api/t={tenant}/users/m={manager}      returns the team of a particular manager
        function getUsersOfManager(tenant, manager) {
            var url= getConnectionString()+"/t="+tenant+'/users/m='+manager;
            return httpFactory.getRequest(url);
        }

        //    api/t={tenant}/users/e={employee}     returns all the managers of the a particular worker.
        function getManagersOfUser(tenant, user) {
            var url= getConnectionString()+"/t="+tenant+'/users/e='+user;
            return httpFactory.getRequest(url);
        }

    }

    ///////////////////////////////////
    // BPM
    ///////////////////////////////////

    // BPM is an AngularJS Factory that provides methods for BPM objects like Work Items, Process Instances, and Process Definitions.
    angular
        .module('bpm-angular')
        .factory('BPM', BPM)
    ;

    BPM.$inject = ['httpFactory',"$q"];
    function BPM(httpFactory,$q) {
        var factory = {
            initialize: initialize,
            //USER and GROUP
            getGroupList: getGroupList,
            getUser: getUser,
            getGroupMembers: getGroupMembers,
            // WORK ITEM
            getWorkItemList: getWorkItemList,
            wrapWorkItemList: wrapWorkItemList,
            getWorkItem: getWorkItem,
            updateWorkItem: updateWorkItem,
            makeChoice: makeChoice,
            accept: accept,
            decline: decline,
            recall: recall,
            reassign: reassign,
            getDecisionTableList: getDecisionTableList,
            evaluateDecisionTables: evaluateDecisionTables,
            wiState : {
                "not started"  : 0,
                "active"  : 3,
                "inactive"  : 4,
                "completed"  : 5,
                "declined"  : 6,
                "accepted"  : 8,
                "waiting"  : 12,
                "suspended"  : 14,
                "voted"  : 16,
                "future"  : 25
            },
            wiListField : {
                "ACTIVITYINSTANCEID" : 20,
                "ASSIGNEE" : 15,
                "CREATEDTIME" : 17,
                "DUEDATE" : 34,
                "ID" : 14,
                "NAME" : 19,
                "PRIORITY" : 18,
                "STATE" : 16
            },
            startDynamicProcess: startDynamicProcess,
            createSubTask : createSubTask,
            // PROCESS INSTANCE
            getProcessInstanceList: getProcessInstanceList,
            wrapProcessInstanceList: wrapProcessInstanceList,
            getProcessInstance: getProcessInstance,
            getProcessInstanceHistory: getProcessInstanceHistory,
            addComment: addComment,
            addAttachment:addAttachment,
            deleteAttachment:deleteAttachment,
            suspend: suspend,
            resume: resume,
            abort: abort,
            operateActivities: operateActivities,
            setPriority:setPriority,
            freeTextSearch: freeTextSearch,
            piListField : {
                "COMMENT" : 49,
                "CLOSEDTIME" : 10,
                "CREATEDTIME" : 9,
                "DESCRIPTION" : 35,
                "DUEDATE" : 39,
                "ID" : 5,
                "INITIATOR" : 7,
                "NAME" : 11,
                "OWNER" : 31,
                "PARENTID" : 13,
                "PRIORITY" : 8,
                "STATE" : 6,
                "TITLE" : 12,
                "TYPE" : 41
            },
            piState : {
                "open"   : 0,
                "running"   : 1,
                "closed"    : 2,
                "_closed"    : 8,
                "resumed"   : 7,
                "error"   : 10,
                "aborted"   : 11,
                "suspended" : 12
            },
            // PROCESS DEFINITION
            getProcessDefinition : getProcessDefinition,
            getProcessDefinitionList : getProcessDefinitionList,
            getPublishedPdListByName : getPublishedPdListByName,
            getPublishedProcessDefinition : getPublishedProcessDefinition,
            createInstance : createInstance,
            createBatch : createBatch,
            pdListField : {
                LISTFIELD_PLAN_ID: 0,
                LISTFIELD_PLAN_IDENTIFIER: 3,
                LISTFIELD_PLAN_NAME: 2,
                LISTFIELD_PLAN_OWNER: 4,
                LISTFIELD_PLAN_STATE: 1
            },
            pdState : {
                STATE_DELETED : 4,
                STATE_DRAFT : 0,
                STATE_OBSOLETE : 3,
                STATE_PRIVATE : 2,
                STATE_PUBLISHED : 1
            }
        };

        ////////////
        // IMPLEMENTATION
        return factory;
        /************************************************************************************
         * initialize
         * This is a the function for initializing the BPM Object
         * server should be a string like: http://interstagedemo:49950/aa/
         * server can also be "" and then it will use the local BPM server
         * This function adds the "api" to the URL.
         * It create a connection object like this
         {
             "server": "http://interstagedemo:49950/aa/",
                server can also be "" to use the localhost.
             "tenant": "Default",
             "application": "TestApp"
         },
         ************************************************************************************/
        function initialize(server, tenant, application)
        {
            // If the server is passed, make sure the server URL ends with a /
            var serverUrl= "";
            if (server){
                serverUrl= server.endsWith("/") ? server : server+"/";
            }
            serverUrl+="api";

            var bpmConnectionObject= {};
            bpmConnectionObject.server= serverUrl;
            bpmConnectionObject.tenant= tenant;
            bpmConnectionObject.application= application;

            factory.bpmConnection= bpmConnectionObject;
        }
        
        function getServerConnectionString()
        {
            // Verify that the Factory has been initialized
            if ((typeof factory.bpmConnection === "undefined")||(!factory.bpmConnection)){
                var errorMessage= "ERROR: BPM not initialized. BPM must be initialized before use. Use BPM.initialize()";
                console.log(errorMessage);
                throw errorMessage;
            }
            return factory.bpmConnection.server;
        }
        
        function getTenantConnectionString()
        {
            // Verify that the Factory has been initialized
            if ((typeof factory.bpmConnection === "undefined")||(!factory.bpmConnection)){
                var errorMessage= "ERROR: BPM not initialized. BPM must be initialized before use. Use BPM.initialize()";
                console.log(errorMessage);
                throw errorMessage;
            }
            return factory.bpmConnection.server+"/t="+factory.bpmConnection.tenant;
        }
        
        

        function getConnectionString()
        {
            var app= (factory.bpmConnection.application=="AllApps") ? factory.bpmConnection.application : "a="+factory.bpmConnection.application;
            return getTenantConnectionString()+"/"+app;
        }
        
        function getGroupList() {
            var url = getTenantConnectionString() + "/Groups";
            return httpFactory.getRequest(url);
        }
        
        function getGroupMembers(group) {
            var url= getTenantConnectionString() + '/users/g=' + group;
            return httpFactory.getRequest(url);
        }
        
        function getUser(user) {
            var url= getTenantConnectionString() + '/users/u=' + user;
            return httpFactory.getRequest(url);
        }

        function getWorkItemList(wiFilter, searchFilter) {
            var url= getConnectionString()+"/"+wiFilter;
            if (!searchFilter){
                searchFilter={filterBy:[]};
            }

            return httpFactory.postJsonRequest(searchFilter,url);
        }
        
        function getDecisionTableList(appId) {
            var url= getTenantConnectionString() + "/a=" + appId + "/ObtainRules";
            return httpFactory.getRequest(url);
        }
        
        function evaluateDecisionTables(content, tenantId, appId) {
            var url = getServerConnectionString() + "/t=" + tenantId + "/a=" + appId + "/EvaluateRules";
            return httpFactory.postJsonRequest(content,url);
        }

        // Use this to convert a list of workList workItems to full workItems
        function wrapWorkItemList(workList) {
            return workList.map(
                function (workItem){
                    return {"wi" : workItem};
                }
            );
        }

        // /aa/api/t=Default/wi=2049
        function getWorkItem(workItemID) {
            var url= getConnectionString()+"/wi="+workItemID;
            return (httpFactory.getRequest(url));
        }

        // Save changes to the UDAs of the work item.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // /aa/api/t=Default/wi=2049
        function updateWorkItem(workItem) {
            var url= getConnectionString()+"/wi="+workItem.wi.id;
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            return httpFactory.postJsonRequest(postData,url);
        }

        // Make a choice on the work item.
        // this can also optionally update UDAs.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // /aa/api/t=Default/wi=2049/MakeChoice
        function makeChoice(workItem, choiceName) {
            if (typeof(workItem.wi) == 'undefined')
            {
                var errorMessage= "ERROR: workItem should be the full Work Item. For wi objects, just wrap the wi in an object like {wi: ...}"
                console.log(errorMessage);
                throw errorMessage;
            }
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/MakeChoice';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            postData.choice= choiceName;
            return httpFactory.postJsonRequest(postData,url);
        }

        // Accept the work item.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // /aa/api/t=Default/wi=2049/Accept
        function accept(workItem) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/Accept';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            return httpFactory.postJsonRequest(postData,url);
        }

        // Decline the work item.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // /aa/api/t=Default/wi=2049/Decline
        function decline(workItem) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/Decline';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            return httpFactory.postJsonRequest(postData,url);
        }

        // Recall the work item.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // /aa/api/t=Default/wi=2049/Recall
        function recall(workItem) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/Recall';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            return httpFactory.postJsonRequest(postData,url);
        }

        // Reassign the work item.
        // workItem: the work item. this must be formatted as the full work item like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the pi and pd elements can be null or undefined.
        // assignees: an array of userIDs
        // ['moe', 'larry', 'curly']
        // /aa/api/t=Default/wi=2049/Reassign
        function reassign(workItem,assignees) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/Reassign';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null))
            {
                postData.uda=workItem.pi.uda;
            }
            postData.assignees= assignees;
            return httpFactory.postJsonRequest(postData,url);
        }

        function startDynamicProcess(workItem,planName) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/StartSubProcess';
            var postData= {};
            if (typeof(workItem.pi) != 'undefined' && (workItem.pi!=null)) {
                postData.uda=workItem.pi.uda;
            }
            postData.planName= planName;
            return httpFactory.postJsonRequest(postData,url);
        }


        // workItem is parent workItem
        // task should be a JSON object like:
        /*
         {
         "name" : "MANDATORY: A task name",
         "description" : "OPTIONAL: A long description for the task.",
         "assignees": [] MANDATORY array of userIDs ['moe', 'larry', 'curly']
         "priority": x    OPTIONAL INTEGER
         "due": OPTIONAL timestamp
         }
         */
        function createSubTask(workItem,task) {
            var url= getConnectionString()+"/wi="+workItem.wi.id+'/StartDynamicTask';
            var postData= task;
            return httpFactory.postJsonRequest(postData,url);
        }


        // PROCESS INSTANCE METHODS
        // fetch list of ProcessInstance objects.
        // /aa/api/t=Default/a=app/AllProcesses
        // AllProcesses
        // MyProcesses
        // AllActiveProcesses
        // MyActiveProcesses
        // AllInactiveProcesses
        // MyInactiveProcesses
        // AllProcessesInErrorState
        // RETURN: A promise from HTTP
        //      {"piList": [{...}, ...]}
        function getProcessInstanceList(piFilter, searchFilter) {
            var url= getConnectionString()+"/"+piFilter;
            if (!searchFilter){
                searchFilter={filterBy:[]};
            }

            return httpFactory.postJsonRequest(searchFilter,url);
        }

        // Use this to convert a list of process List process instances to full process instances
        // An array of Process Instance objects
        // Typically this is the piList array from getProcessInstanceList
        // RETURN: an array of ProcessInstance objects.
        function wrapProcessInstanceList(piList) {
            return piList.map(
                function (processInstance){
                    return {"pi" : processInstance};
                }
            );
        }

        // /aa/api/t=Default/a=app/pi=2049
        function getProcessInstance(processInstanceId) {
            var url= getConnectionString()+"/pi="+processInstanceId;
            return (httpFactory.getRequest(url));
        }

        // /aa/api/t=Default/pi=2049/History
        function getProcessInstanceHistory(processInstanceID) {
            var url= getConnectionString()+"/pi="+processInstanceID+"/History";
            return (httpFactory.getRequest(url));
        }

        // Add a comment to the Process Instance
        // this can also optionally update UDAs.
        // pi: the pi portion of the BPM object.
        // this could be the pi part of a workitem
        // /aa/api/t=Default/pi=2049/AddComment
        function addComment(pi,comment) {
            var url= getConnectionString()+"/pi="+pi.idInst+"/AddComment";
            var processInstance={};
            processInstance.comment= comment;
            processInstance.uda= pi.uda;
            return httpFactory.postJsonRequest(processInstance,url);
        }

        // Add an attachment to the Process Instance
        // pi: the pi portion of the BPM object.
        // this could be the pi part of a workitem
        // /aa/api/t=Default/pi=2049/AddAttachment
        function addAttachment(pi,name,path) {
            var url= getConnectionString()+"/pi="+pi.idInst+"/AddAttachment";
            var processInstance={};
            processInstance.name= name;
            processInstance.path= path;
            return httpFactory.postJsonRequest(processInstance,url);
        }

        // Delete an attachment from the Process Instance
        // pi: the pi portion of the BPM object.
        // this could be the pi part of a workitem
        // /aa/api/t=Default/pi=2049/AddAttachment
        function deleteAttachment(pi,name) {
            var url= getConnectionString()+"/pi="+pi.idInst+"/RemoveAttachment";
            var processInstance={};
            processInstance.name= name;
            return httpFactory.postJsonRequest(processInstance,url);
        }

        // Suspend the Process Instance
        // this can also optionally update UDAs.
        // processInstance: the Process Instance. this must be formatted as the full process instance like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the wi and pd elements can be null or undefined.
        // /aa/api/t=Default/pi=2049/Suspend
        function suspend(processInstance) {
            var url= getConnectionString()+"/pi="+processInstance.pi.idInst+"/Suspend";
            return httpFactory.postJsonRequest(processInstance,url);
        }

        // Resume the Process Instance
        // this can also optionally update UDAs.
        // processInstance: the Process Instance. this must be formatted as the full process instance like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the wi and pd elements can be null or undefined.
        // /aa/api/t=Default/pi=2049/Resume
        function resume(processInstance) {
            var url= getConnectionString()+"/pi="+processInstance.pi.idInst+"/Resume";
            return httpFactory.postJsonRequest(processInstance,url);
        }

        // Abort the Process Instance
        // this can also optionally update UDAs.
        // processInstance: the Process Instance. this must be formatted as the full process instance like this:
        // { wi : {...}, pi : {...}, pd : {...}}
        // the wi and pd elements can be null or undefined.
        // /aa/api/t=Default/pi=2049/Abort
        function abort(processInstance) {
            var url= getConnectionString()+"/pi="+processInstance.pi.idInst+"/Abort";
            return httpFactory.postJsonRequest(processInstance,url);
        }

        function operateActivities(processInstance,optionalActivities,stopAllActivities) {
            var url= getConnectionString()+"/pi="+processInstance.pi.idInst+"/OperateActivities";
            processInstance.stopAllActivities = stopAllActivities;
            processInstance.activities = optionalActivities;
            return httpFactory.postJsonRequest(processInstance,url);
        }

        function setPriority(processInstance,priority) {
            var url= getConnectionString()+"/pi="+processInstance.pi.idInst;
            processInstance.priority = priority;
            return httpFactory.postJsonRequest(processInstance,url);
        }

        /**
         * Free text search through out the list of process instances.
         * @returns {list of process instance objects}
         */
        function freeTextSearch(searchText) {
            var url= getConnectionString()+"/SearchText";
            var postData= {};
            postData.searchText= searchText;
            return httpFactory.postJsonRequest(postData,url);
        }

        // PROCESS DEFINITION METHODS
        // fetch list of Plan objects.
        // /aa/api/t=Default/AllPlans
        // AllPlans
        // RETURN: A promise from HTTP

        function getProcessDefinitionList(pdFilter, searchFilter) {
            var url= getConnectionString()+"/"+pdFilter;
            if (!searchFilter){
                searchFilter={filterBy:[]};
            }
            return httpFactory.postJsonRequest(searchFilter,url);
        }

        function getProcessDefinition(planId) {
            var url= getConnectionString()+"/pd="+planId;
            return httpFactory.getRequest(url);
        }

        // This gets the list of pd objects
        function getPublishedPdListByName(name){
            var url= getConnectionString()+"/AllPlans";
            var searchFilter={filterBy:[]};
            // LISTFIELD_PLAN_NAME : 2
            // LISTFIELD_PLAN_STATE : 1
            // STATE_PUBLISHED : 1
            var nameFilter={
                listField : factory.pdListField.LISTFIELD_PLAN_NAME,
                sqlOperator : "=",
                value: name
            };
            var stateFilter={
                listField : factory.pdListField.LISTFIELD_PLAN_STATE,
                sqlOperator : "=",
                value: factory.pdState.STATE_PUBLISHED.toString()
            };
            searchFilter.filterBy.push(nameFilter);
            searchFilter.filterBy.push(stateFilter);
            return httpFactory.postJsonRequest(searchFilter,url);
        }

        // Return the Published Process Definition with the given name.
        // This method returns a promise. The result of the promise is either the Process Definition or null.
        function getPublishedProcessDefinition(name) {
            // The deferred is what we use to manage the multiple calls.
            var pdDeferred= $q.defer();

            getPublishedPdListByName(name)
                .success(
                    function (data, status) {
                        if (data.pdList.length > 0){
                            var pd = data.pdList[0];
                            getProcessDefinition(pd.id)
                                .success(
                                    function (data, status) {
                                        pdDeferred.resolve(data);
                                    }
                                )
                                .error(
                                    function (data, status) {
                                        pdDeferred.reject(data);
                                    }
                                )
                            ;
                        }
                        else{
                            // There is no published Process Definition
                            pdDeferred.resolve(null);
                        }
                    }
                )
                .error(
                    function (data, status) {
                        pdDeferred.reject(data);
                    }
                )
            ;

            // The the promise will be returned to the caller.
            return pdDeferred.promise;
        }

        function createInstance(planId,reqdata,attachments) {
            var url= getConnectionString()+"/pd="+planId+'/CreateInstance';
            var reqObj = {uda: reqdata};
            if(attachments) {
            	reqObj.attachments = attachments;
            }
            console.log("Create process instance",reqObj);
            return httpFactory.postJsonRequest(reqObj,url);
        }

        function createBatch(planId,reqdata) {
            var url= getConnectionString()+"/pd="+planId+'/CreateBatch';
            var fields = [];
            for(var i=0; i < reqdata.length; i++) {
                fields.push({uda: reqdata[i]});
            }
            return httpFactory.postJsonRequest({list: fields},url);
        }


    }


    ///////////////////////////////////
    // BPMService
    ///////////////////////////////////

    angular
        .module('bpm-angular')
        .service('BPMService',BPMService)
    ;


    // BPMService provides utility functions for BPM
    BPMService.$inject = ['httpFactory'];
    function BPMService(httpFactory) {
        this.getNodeInstanceById = getNodeInstanceById;
        this.getNodeById = getNodeById;
        this.filterSubTaskList = filterSubTaskList;
        this.aggregateWorkItemList = aggregateWorkItemList;

        this.nodeTypes= nodeTypes;

        var nodeTypes= {
            "ACTIVITY": 2,
            "AND": 4,
            "CHAINED_PROCESS": 9,
            "COMPOUND_ACTIVITY": 13,
            "CONDITION": 5,
            "DELAY": 8,
            "DYNAMIC_ACTIVITY": 14,
            "EVENT_ACTIVITY": 12,
            "EXIT": 1,
            "OR": 6,
            "REMOTE_SUB_PROCESS": 11,
            "START": 0,
            "SUB_PROCESS": 7,
            "VOTING_ACTIVITY": 10
        };



        /************************************************************************************
         * getNodeInstanceById
         * This takes the pi section of an object and returns the nodeInstace. For example:
         {
             "idDef": 8508,
             "idInst": 5518,
             "name": "Approuve la résolution",
             "state": 4
         },
         * You can then use idDef to get the node: getNodeById(pd, idDef) {
         *  NOTE: it is possible for idDef to be -1 which means there is no activity for this item
         *  This happens for example when the task was created as a dynamic sub task.
         ************************************************************************************/
        function getNodeInstanceById(pi, nodeInstanceId)
        {
            return pi.nodes.find(
                function(nodeInstance){
                    return (nodeInstance.idInst === nodeInstanceId);
                }
            );
        }

        /************************************************************************************
         * getNodeById
         * This takes the pd section of an object and returns the node definition. For example:
         {
             "ExtendedAttributes": { ... },
             "arrowsIn": [ ... ],
             "arrowsOut": [ ... ],
             "desc": "Node description",
             "id": 8516,
             "name": "Start",
             "size": {
               "height": 60,
               "width": 60
             },
             "type": 0,
             "upperLeft": {
               "x": 24,
               "y": 46
             }
           },
         ************************************************************************************/
        function getNodeById(pd, nodeIdDef)
        {
            return pd.nodes.find(
                function(node){
                    return (node.id === nodeIdDef);
                }
            );
        }

        /*****************************************************************
         filterSubTaskList generates a Sub Task List from workitem.pi.nodes
         by filtering out all nodes that are not: ACTIVITY, DYNAMIC_ACTIVITY, or VOTING_ACTIVITY
         Also we only want nodes where the parent is the current workitem
         workitem must include the pd portion so it can reference the type.
         filterSubTaskList is typically called when you display a workitem so you can see its sub tasks.
         *****************************************************************/
        function filterSubTaskList(workitem)
        {
            return workitem.pi.nodes.filter(
                function (nodeInstance){
                    // Check that this node is a child of the current workitem
                    if (workitem.wi.nodeInstanceId !== nodeInstance.parent){
                        return false;
                    }
                    // If the node does not have an idDef assume it is a dynamically added sub task
                    if (nodeInstance.idDef<0){
                        return true;
                    }
                    var nodeDef= getNodeById(workitem.pd, nodeInstance.idDef);
                    return ((nodeDef.type==nodeTypes.ACTIVITY) || (nodeDef.type==nodeTypes.DYNAMIC_ACTIVITY) || (nodeDef.type==nodeTypes.VOTING_ACTIVITY));
                }
            );
        }

        /*****************************************************************
         aggregateWorkItemList generates a list of Node Instances from a list of work items.
         It filters out any work items that are not of the following states:
         active, inactive, declined, accepted, voted
         aggregateWorkItemList is typically called when displaying a process instance so you can display who is currently responsible for moving the process forward
         *****************************************************************/
        function aggregateWorkItemList(workItems)
        {
            // first filter out the items that don't match the desired states.
            var currentWorkItems= workItems.filter(
                function (workItem){
                    return (
                        (workItem.state== 3) ||
                        (workItem.state== 4) ||
                        (workItem.state== 6) ||
                        (workItem.state== 8) ||
                        (workItem.state== 16)
                    )
                }
            );

            return currentWorkItems.reduce(
                function (nodeInstances,workItem){
                    // Check if the nodeInstance exists in the array we are building
                    var nodeInstance= nodeInstances.find(
                        function(ni){
                            return (ni.nodeInstanceId== workItem.nodeInstanceId);
                        }
                    );
                    // if we didn't find the node instance we add the workitem to represent the new node instance.
                    if (!nodeInstance){
                        nodeInstance=workItem;
                        nodeInstance.workItems=[];
                        nodeInstances.push(nodeInstance);
                    }
                    // This is the individual assignee and state that gets added to the list of workitems in the node instance.
                    var niWorkItem= {
                        id : workItem.id,
                        forms : workItem.forms,
                        assignee : workItem.assignee,
                        state : workItem.state
                    };
                    nodeInstance.workItems.push(niWorkItem);
                    return nodeInstances;
                },
                []
            );
        }




    }



    ///////////////////////////////////
    // Work Item Fetchers
    ///////////////////////////////////

    angular
        .module('bpm-angular')
        .controller('bpmWorkItemListFetcherCtrl', bpmWorkItemListFetcherCtrl)
        .directive('bpmWorkItemListFetcher', bpmWorkItemListFetcher)
        .controller('bpmWorkItemFetcherCtrl', bpmWorkItemFetcherCtrl)
        .directive('bpmWorkItemFetcher', bpmWorkItemFetcher)
    ;



    ///////////////////////////////////
    // bpmWorkItemListFetcherCtrl
    ///////////////////////////////////
    bpmWorkItemListFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmWorkItemListFetcherCtrl(BPM,$scope,$timeout) {
        function fetchWiList()
        {
            $scope.updating=true;
            BPM
                .getWorkItemList($scope.filter)
                .success(
                    function (data, status) {
                        $scope.wiArray=data.wiList;
                        $scope.updating=false;
                        if ($scope.bpmOnUpdate){
                            $timeout(function(){
                                $scope.bpmOnUpdate();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        $scope.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        initialize();
        function initialize() {
            $scope.updating=false;
            // Watch for the displayed filter to change
            $scope.$watch('filter',
                function() {
                    $scope.wiArray= [];
                    if ($scope.filter!=null && $scope.filter!==''){
                        fetchWiList();
                    }
                }
            );
            // Expose the methods for the directive api
            $scope.api = {
                "fetchWiList" : fetchWiList
            };
        }
    }

    ///////////////////////////////////
    // bpmWorkItemListFetcher
    // bpm-work-item-list-fetcher
    //    "filter": A string that specifies the filter to apply to the workitem list.
    //                      When the filter changes the bpmWorkItemListFetcher reloads the worklist from the server.
    //    "wiArray":        Array of small workitem (that is wi) objects
    //    "bpmOnUpdate":      Pass in a reference to a function that will be invoked every time the list is updated.
    //    "api":            Your controller can manually refresh the Work List by using the api.
    //                      To reload the Work List call: api.fetchWiList()

    ///////////////////////////////////
    // Directive to fetch WorkItem List from the server
    function bpmWorkItemListFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'bpmWorkItemListFetcherCtrl',
            scope: {
                "filter": "@",
                "wiArray": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmWorkItemFetcherCtrl
    ///////////////////////////////////
    bpmWorkItemFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmWorkItemFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;
        vm.workItem= null;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
        function fetchWorkItem() {
            // As soon as the ID changes the current workItem is no longer valid.
            vm.workItem= null;
            if (vm.workItemId){
                vm.updating=true;
                BPM
                    .getWorkItem(vm.workItemId)
                    .success(
                        function (data, status) {
                            vm.workItem= data;
                            vm.updating=false;
                            if (vm.bpmOnUpdate){
                                $timeout(function(){
                                    vm.bpmOnUpdate();
                                });
                            }
                        }
                    )
                    .error(
                        function (data, status) {
                            vm.updating=false;
                            $scope.$emit('bpmErrorEvent', data);
                        }
                    )
                ;
            }
        }

        initialize();
        function initialize() {
            // Watch for the id of the displayed WorkItem to change
            $scope.$watch('vm.workItemId',fetchWorkItem);

            // Expose the methods for the directive api
            vm.api = {
                "fetchWorkItem" : fetchWorkItem
            };
        }
    }

    ///////////////////////////////////
    // bpmWorkItemFetcher
    // bpm-work-item-fetcher
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmWorkItemFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'bpmWorkItemFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "workItemId": "@",
                "workItem": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // Process Instance
    ///////////////////////////////////
    angular
        .module('bpm-angular')
        .controller('ProcessInstanceListFetcherCtrl', ProcessInstanceListFetcherCtrl)
        .directive('bpmProcessInstanceListFetcher', bpmProcessInstanceListFetcher)
        .controller('ProcessInstanceFetcherCtrl', ProcessInstanceFetcherCtrl)
        .directive('bpmProcessInstanceFetcher', bpmProcessInstanceFetcher)
        .controller('bpmProcessInstanceHistoryFetcherCtrl', bpmProcessInstanceHistoryFetcherCtrl)
        .directive('bpmProcessInstanceHistoryFetcher', bpmProcessInstanceHistoryFetcher)
    ;

    ///////////////////////////////////
    // ProcessInstanceListFetcherCtrl
    ///////////////////////////////////
    ProcessInstanceListFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function ProcessInstanceListFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;

        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function fetchPiList() {
            vm.piArray= null;
            if (vm.filter!=null && vm.filter!==''){
                vm.updating=true;
                BPM
                    .getProcessInstanceList(vm.filter)
                    .success(
                        function (data, status) {
                            vm.piArray= data.piList;
                            vm.updating=false;
                            if (vm.bpmOnUpdate){
                                $timeout(function(){
                                    vm.bpmOnUpdate();
                                });
                            }
                        }
                    )
                    .error(
                        function (data, status) {
                            console.log("ERROR fetchProcessInstanceList",data);
                            vm.updating=false;
                            $scope.$emit('bpmErrorEvent', data);
                        }
                    )
                ;
            }
        }

        initialize();
        function initialize() {
            // Watch for the displayed filter to change
            $scope.$watch('vm.filter',fetchPiList);

            // Expose the methods for the directive api
            vm.api = {
                fetchPiList: fetchPiList
            };
        }
    }

    ///////////////////////////////////
    // bpmProcessInstanceListFetcher
    // bpm-process-instance-list-fetcher
    //    "filter": A string that specifies the filter to apply to the process instance list.
    //                          When the filter changes the bpmProcessInstanceListFetcher reloads the process instance list from the server.
    //    "piArray": Array of small pi objects
    //    "api":            Your controller can manually refresh the Process Instance by using the api.
    //                      To reload the Process Instance list call: api.fetchPiList()

    // events:              $scope.$emit('bpmProcessInstanceListUpdatedEvent', vm.processInstances);

    ///////////////////////////////////
    // Directive to fetch ProcessInstance List from the server
    function bpmProcessInstanceListFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'ProcessInstanceListFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "filter": "@",
                "piArray": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // ProcessInstanceFetcherCtrl
    ///////////////////////////////////
    ProcessInstanceFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function ProcessInstanceFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;
        vm.processInstance= null;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function fetchProcessInstance() {
            // As soon as the ID changes the current workItem is no longer valid.
            vm.processInstance= null;
            if (vm.processInstanceId){
                vm.updating=true;
                BPM
                    .getProcessInstance(vm.processInstanceId)
                    .success(
                        function (data, status) {
                            vm.processInstance= data;
                            vm.updating=false;
                            if (vm.bpmOnUpdate){
                                $timeout(function(){
                                    vm.bpmOnUpdate();
                                });
                            }
                        }
                    )
                    .error(
                        function (data, status) {
                            vm.updating=false;
                            $scope.$emit('bpmErrorEvent', data);
                        }
                    )
                ;
            }
        }

        initialize();
        function initialize() {
            // Watch for the id of the displayed Process Instance to change
            $scope.$watch('vm.processInstanceId',fetchProcessInstance);

            // Expose the methods for the directive api
            vm.api = {
                "fetchProcessInstance" : fetchProcessInstance
            };
        }
    }

    ///////////////////////////////////
    // bpmProcessInstanceFetcher
    // bpm-process-instance-fetcher
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmProcessInstanceFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'ProcessInstanceFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "processInstanceId": "@",
                "processInstance": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }


    ///////////////////////////////////
    // bpmProcessInstanceHistoryFetcherCtrl
    ///////////////////////////////////
    bpmProcessInstanceHistoryFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmProcessInstanceHistoryFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;
        vm.processInstanceId= null;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function fetchPiHistory() {
            // As soon as the ID changes the current workItem is no longer valid.
            vm.history= null;
            if (vm.processInstanceId){
                vm.updating=true;
                BPM
                    .getProcessInstanceHistory(vm.processInstanceId)
                    .success(
                        function (data, status) {
                            vm.history= data.history;
                            vm.updating=false;
                            if (vm.bpmOnUpdate){
                                $timeout(function(){
                                    vm.bpmOnUpdate();
                                });
                            }
                        }
                    )
                    .error(
                        function (data, status) {
                            vm.updating=false;
                            $scope.$emit('bpmErrorEvent', data);
                        }
                    )
                ;
            }
        }

        initialize();
        function initialize() {
            // Watch for the id of the displayed Process Instance to change
            $scope.$watch('vm.processInstanceId',fetchPiHistory);

            // Expose the methods for the directive api
            vm.api = {
                "fetchPiHistory" : fetchPiHistory
            };
        }
    }

    ///////////////////////////////////
    // bpmProcessInstanceHistoryFetcher
    // bpm-process-instance-fetcher
    ///////////////////////////////////
    // Directive to fetch the Process Instance History from the server
    // Returns an array of history events:
    function bpmProcessInstanceHistoryFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'bpmProcessInstanceHistoryFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "processInstanceId": "@",
                "history": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }



    ///////////////////////////////////
    // Process Definition
    ///////////////////////////////////

    angular
        .module('bpm-angular')
        .controller('bpmProcessDefinitionListFetcherCtrl', bpmProcessDefinitionListFetcherCtrl)
        .directive('bpmProcessDefinitionListFetcher', bpmProcessDefinitionListFetcher)
        .controller('bpmProcessDefinitionFetcherCtrl', bpmProcessDefinitionFetcherCtrl)
        .directive('bpmProcessDefinitionFetcher', bpmProcessDefinitionFetcher)
    ;

    ///////////////////////////////////
    // bpmProcessDefinitionListFetcherCtrl
    ///////////////////////////////////
    bpmProcessDefinitionListFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmProcessDefinitionListFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;
//        vm.api= {};

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE

        function fetchPdList()
        {
            vm.updating=true;
            BPM
                .getProcessDefinitionList(vm.filter)
                .success(
                    function (data, status) {
                        vm.pdArray = data.pdList;
                        vm.updating = false;
                        if (vm.bpmOnUpdate){
                            $timeout(function(){
                                vm.bpmOnUpdate();
                            });
                        }
                    }
                )
                .error(
                    function (data, status) {
                        console.log("ERROR fetchPdList",data);
                        vm.updating=false;
                        $scope.$emit('bpmErrorEvent', data);
                    }
                )
            ;
        }

        initialize();
        function initialize() {
            // Watch for the displayed filter to change
            $scope.$watch('vm.filter',
                function() {
                    vm.pdArray= null;
                    if (vm.filter!=null && vm.filter!==''){
                        fetchPdList();
                    }
                }
            );

            // Expose the methods for the directive api
            vm.api = {
                fetchPdList: fetchPdList
            };
        }
    }

    ///////////////////////////////////
    // bpmProcessDefinitionListFetcher
    // bpm-process-definition-list-fetcher
    //    "filter": A string that specifies the filter to apply to the process definition list.
    //                          When the filter changes the bpmProcessDefinitionListFetcher reloads the process definition list from the server.
    //    "pdArray": Array of small process definition (that is pd) objects
    //    "api":            Your controller can manually refresh the Process Instance by using the api.
    //                      To reload the Process Definition list call: api.fetchProcessDefinitionList()

    ///////////////////////////////////
    // Directive to fetch Process Definition List from the server
    function bpmProcessDefinitionListFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'bpmProcessDefinitionListFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "filter": "@",
                "pdArray": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }

    ///////////////////////////////////
    // bpmProcessDefinitionFetcherCtrl
    ///////////////////////////////////
    bpmProcessDefinitionFetcherCtrl.$inject = ['BPM','$scope','$timeout'];
    function bpmProcessDefinitionFetcherCtrl(BPM,$scope,$timeout) {
        var vm = this;
        ////////////////////////////////////////////////
        // HEADER
        vm.updating=false;
        vm.processDefinition= null;

        ////////////////////////////////////////////////
        // IMPLEMENTATION CODE
        function fetchProcessDefinition() {
            // As soon as the ID changes the current Process Definition is no longer valid.
            vm.processDefinition= null;
            if (vm.processDefinitionId){
                vm.updating=true;
                BPM
                    .getProcessDefinition(vm.processDefinitionId)
                    .success(
                        function (data, status) {
                            vm.processDefinition= data;
                            vm.updating=false;
                            if (vm.bpmOnUpdate){
                                $timeout(function(){
                                    vm.bpmOnUpdate();
                                });
                            }
                        }
                    )
                    .error(
                        function (data, status) {
                            vm.updating=false;
                            $scope.$emit('bpmErrorEvent', data);
                        }
                    )
                ;
            }
        }

        initialize();
        function initialize() {
            // Watch for the id of the displayed Process Definition to change
            $scope.$watch('vm.processDefinitionId',fetchProcessDefinition);

            // Expose the methods for the directive api
            vm.api = {
                fetchProcessDefinition: fetchProcessDefinition
            };
        }
    }

    ///////////////////////////////////
    // bpmProcessDefinitionFetcher
    // bpm-process-definition-fetcher
    ///////////////////////////////////
    // Directive to display a WorkItem
    function bpmProcessDefinitionFetcher() {
        var directive = {
            restrict: 'EA',
            controller: 'bpmProcessDefinitionFetcherCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                "processDefinitionId": "@",
                "processDefinition": "=",
                "bpmOnUpdate" : "&?",
                "api": "=?"
            }
        };
        return directive;
    }






    ///////////////////////////////////
    // TimeFactory
    ///////////////////////////////////

    // TimeFactory provides methods for dealing with time
    angular
        .module('bpm-angular')
        .factory('TimeFactory', TimeFactory)
        .filter('timewords', timewords)
        .filter('timewordsFromNow', timewordsFromNow)
    ;

    function TimeFactory() {
        var factory = {
            MINUTE_SECONDS: 60,
            HOUR_SECONDS: 3600,
            DAY_SECONDS: 86400,
            convertTimeStampToJSON: convertTimeStampToJSON,
            convertTimeStampToWords: convertTimeStampToWords,
            isFuture: isFuture,
            timeDifference: timeDifference
        };

        ////////////
        // IMPLEMENTATION
        return factory;

        // convert the amount of milliseconds to time in JSON object
        // {
        //      days : 1
        //      hours : 12
        //      minutes : 0
        function convertTimeStampToJSON(timeStampMillis) {
            var seconds= timeStampMillis/1000;
            var jsonTime= {};
            jsonTime.days= Math.floor(seconds/(factory.DAY_SECONDS));
            seconds= seconds-(jsonTime.days*factory.DAY_SECONDS);
            jsonTime.hours= Math.floor(seconds/factory.HOUR_SECONDS);
            seconds= seconds-(jsonTime.hours*factory.HOUR_SECONDS);
            jsonTime.minutes= Math.floor(seconds/factory.MINUTE_SECONDS);
            if (jsonTime.minutes===0){
                jsonTime.minutes=1;
            }
            return jsonTime;
        }

        // convert the amount of milliseconds to time in words
        function convertTimeStampToWords(timeStampMillis) {
            var totalSeconds= timeStampMillis/1000;
            var jsonTime=factory.convertTimeStampToJSON(timeStampMillis);

            var displayText= "";
            if (totalSeconds<factory.MINUTE_SECONDS){
                return ("1 minute");
            }

            if (jsonTime.days>1){
                // Plural days
                displayText= displayText.concat(jsonTime.days.toString(),' days ');
            }
            else
            {
                if(jsonTime.days===1){
                    // Use singular day
                    displayText= displayText.concat(jsonTime.days.toString(),' day ')
                }
                if (jsonTime.hours>1){
                    // Plural hours
                    displayText= displayText.concat(jsonTime.hours.toString(),' hours ')
                }
                else
                {
                    if(jsonTime.hours===1){
                        // Use singular hour
                        displayText= displayText.concat(jsonTime.hours.toString(),' hour ')
                    }
                    if (jsonTime.days===0)
                    {
                        // Only if days are zero do we show minutes.
                        if (jsonTime.minutes>1){
                            // Plural minutes
                            displayText= displayText.concat(jsonTime.minutes.toString(),' minutes ')
                        }
                        else
                        {
                            if(jsonTime.minutes===1){
                                // Use singular minute
                                displayText= displayText.concat(jsonTime.minutes.toString(),' minute ')
                            }
                        }

                    }
                }
            }
            return displayText;
        }


        // return true if timeStampMillis is in the future, false if in the past or current
        function isFuture(timeStampMillis) {
            return (Date.now()<timeStampMillis);
        }

        // return the difference in millis from now. return is always positive
        function timeDifference(timeStampMillis) {
            var now= (Date.now());
            var timeDiff= timeStampMillis - now;
            return (Math.abs(timeDiff));
        }

    }

    ///////////////////////////////////
    // timewords
    // Filter for showing how long in words is the given time stamp.
    ///////////////////////////////////
    timewords.$inject = ['TimeFactory'];
    function timewords(TimeFactory) {
        return function(input) {
            return TimeFactory.convertTimeStampToWords(input);
        }
    }

    ///////////////////////////////////
    // timewordsFromNow
    // Filter for showing how long ago or in the future something happened.
    ///////////////////////////////////
    timewordsFromNow.$inject = ['TimeFactory'];
    function timewordsFromNow(TimeFactory) {
        return function(input) {
            return TimeFactory.convertTimeStampToWords(TimeFactory.timeDifference(input));
        }
    }



    ///////////////////////////////////
    // httpFactory
    ///////////////////////////////////

    // httpFactory is a wrapper to make using the $http library easy for bpm-angular
    // This is for internal use only and typically it shouldn't be used by BPM developers.

    angular
        .module('bpm-angular')
        .factory('httpFactory', httpFactory)
    ;

    httpFactory.$inject = ['$http'];
    function httpFactory($http) {

        var factory = {
            getRequest: getRequest,
            postFormRequest: postFormRequest,
            postJsonRequest: postJsonRequest,
            putJsonRequest: putJsonRequest,
            deleteJsonRequest: deleteJsonRequest
        };

        ////////////
        // IMPLEMENTATION
        return factory;

        function getRequest(serviceURL)
        {
            var req = {
                withCredentials: true,
                method: 'GET',
                url: serviceURL,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            return $http(req);
        }

        // This posts the given parameterData JSON object as a JSON object in the request body
        function postJsonRequest(parameterData, serviceURL){
            var req = {
                method: 'POST',
                url: serviceURL,
                data: JSON.stringify(parameterData),
                withCredentials: true,
                headers: {
                    'Content-Type': 'text/plain; charset=utf-8'
                }
            };
            return $http(req);
        }

        // This PUTs the given parameterData JSON object as a JSON object in the request body
        function putJsonRequest(parameterData, serviceURL){
            var req = {
                method: 'PUT',
                url: serviceURL,
                data: JSON.stringify(parameterData),
                withCredentials: true,
                headers: {
                    'Content-Type': 'text/plain; charset=utf-8'
                }
            };
            return $http(req);
        }

        // This sends a DELETE request with the given parameterData JSON object as a JSON object in the request body
        function deleteJsonRequest(parameterData, serviceURL){
            var req = {
                method: 'DELETE',
                url: serviceURL,
                data: JSON.stringify(parameterData),
                withCredentials: true,
                headers: {
                    'Content-Type': 'text/plain; charset=utf-8'
                }
            };
            return $http(req);
        }

        // This posts the given parameterData JSON object as a form post
        // Each element in the JSON object will be an attribute posted.
        function postFormRequest(parameterData, serviceURL){
            var req = {
                withCredentials: true,
                method: 'POST',
                url: serviceURL,
                data: $.param(parameterData)
            };
            return sendRequest(req);
        }

        function sendRequest(req)
        {
            return $http(req)
                .success(function (data, status) {
                })
                .error(function (data, status) {
                    console.log("error:",status);
                })
                ;
        }
    }


})();

// Patches for Internet Explorer
if (!String.prototype.endsWith){
    String.prototype.endsWith = function(pattern) {
        var d = this.length - pattern.length;
        return d >= 0 && this.lastIndexOf(pattern) === d;
    };
}